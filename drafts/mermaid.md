```mermaid
gantt
    title Skills timeline
    dateFormat  YYYY-MMa-DD

    section Section
    Long task           :, 2014-01-01, 50d
    Another task     :, 2014-01-01, 70d

```
