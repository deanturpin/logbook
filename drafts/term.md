# Terminology

## Replacing legacy master/slave terminology
As a privileged white male it is difficult to understand but imagine if it jarred every time you heard "slave". Maybe think of an equivalent pair that would offend you.

Weirdly master/servant sounds terrible but master/slave has made it into common parlance.

Primary/secondary has been mooted but I'm not sure it has quite the same relationship.

How would you feel if all instances of master/slave were replaced with male/female?

What else conveys the same one-to-many master/subordinates relationship?

- King/peasants
- Kidnapper/hostages
- Leader/followers

I think it's better to think of a collaborative, working towards a common goal. Server/clients is kind of the wrong way around as the single server is fielding requests from multiple clients. But I do quite like controller/agent.

## Tier 1/Tier 2
I think this is interesting you can have multiple levels of ...,

Using main...

## References
- https://www.theserverside.com/opinion/Master-slave-terminology-alternatives-you-can-use-right-now
- https://www.zdnet.com/google-amp/article/github-to-replace-master-with-alternative-term-to-avoid-slavery-references/
- https://en.wiktionary.org/wiki/master#Synonyms
- https://en.wikipedia.org/wiki/Global_Language_Monitor
- https://en.wikipedia.org/wiki/Master/slave_(technology)

