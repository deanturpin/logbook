# Broker versus brokerless

Obtain the optimal performance/functionality/reliability ratio.

## ZeroMQ
### Broker
- Star / hub and spoke
- Pipeline

### No broker

### Broker as a directory service

### Distributed broker

### Distributed directory service

### References
- http://wiki.zeromq.org/whitepapers:brokerless

