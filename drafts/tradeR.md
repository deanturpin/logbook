# tradeR

Market data via the [CryptoCompare API](https://min-api.cryptocompare.com/) and prices fetched in parallel using `make`. See the [source](https://gitlab.com/deanturpin/tradeR).

TURBO token is a fictional currency created to illustrate how much a normal distribution of random numbers can look like a real currency. The red markers highlight when the latest price has dropped by a significant amount: a number of standard deviations. A "2% over a day" line is projected out from the final price to indicate if there's enough activity to achieve the target yield.

See [tradeR](https://deanturpin.gitlab.io/tradeR/).

