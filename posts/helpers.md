---
title: Coding without coding
subtitle: Tools that will improve your code so you don't have to
date: 2021-06-22
tags:
- cpp
- programming
---

# Coding without coding
Tools that will improve your code so you don't have to.

- iwyu: tidy includes to improve build times
- clang-format: format your code
- clang tidy

## See also (Jason Turner)
- reddit/r/cpp
- C++ ISO home
- leanpub.com (c++ section)
- cppinsights.io -- compiler's view of code
- wg21.link -- search c++ standard
- eel.is/c++draft -- up to the minute standard for the C++ programming language
- godbolt.org
- cppreference.com (also includes a C reference and offline archive)

## Blogs
- Fluent C++ (blog) -- include daily tutorial for teams
- C++ stories
- Sutter's Mill
- Random ASCII by Bruce Dawson
- Modernes C++
- The Old New Thing (Raymond Chen)

## YouTube
- Jason Turner
- The Cherno
- cppcon
- bisqwit
- OneLoneCoder
- Meeting Cpp (conference channel)
