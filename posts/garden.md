---
title: Gardening
subtitle: Pots and raised beds
date: 2020-06-17
tags:
- gardening
- plants
- botany
- fruit
---

# Gardening

## Growing in pots
- Mint
- Broad beans
- Basil
- Oregano
- Peppers (chilli and snack)
- Tomato
- Rosemary
- Blackcurrent

## Watering
If the top surface of the soil looks dark then it doesn't need watering. Otherwise stick your finger in the soil.

## Soil
- Soil and compost should be 50:50
- Fork every few days to aerate

### Best soil mixure
Equal parts

- Vermiculite
- Pete moss
- Mixture of composts

## Raised beds
### Good
- Herbs good, no need to crop rotate
- Rosemary, marjoram and lavender good for attracting pollinating insects
- Onions don't like drying out nor crowding from weeds
- Potatoes work in raised beds but easier in bags (tip the used bags into the raised beds for the following year.

### Bad
- Multi layer planters for strawberries
- Basil best in the sun (greenhouse)
- Mint family should not planted be in the ground (invasive)
- Tomato's ripen with sun and heat
- Large vine crops too rambling for raised bed

## No dig beds
Add mulch regularly, less for salad crops.

## Square foot gardening
More efficient use of space than rows.

## Pest control
- Nowhere to hide
- Natural predators

## Weed control
Compost is sterilised.

## Companion planting
Complementary crops.

- Corn / bean / squash
- Basil / tomato
- Onion / radish / carrot

## Further reading
- Crop rotation
- Interplanting
- Succession planting
- Trap crops - Nasturtium are loved by aphids
- Vertical gardening
- Succession planting
- Mulch
- Overwintering
- Winter crops
- Lasagne Gardnering
