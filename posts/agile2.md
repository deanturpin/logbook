# Agile tldr

Ten steps to happiness.

| Too long | Didn't read |
|-|-|
| Release automatically every sprint | Otherwise it will become somebody's full-time job |
| Do the bits that improve users' lives right now | And "users" could be the devops team, but somebody else needs to be using it as soon as it's committed |
| Stop doing the things that nobody needs or asked for | It may have seemed like a good idea once, but so do lots of things. Products need guidance and vision, of course, but don't be afraid of abandoning something that doesn't work as expected |
| Eat your own dog food | If you're not using your software to do a job, then you're not testing it properly |
| Dev, scrum master and product owner are different roles for >1 person | When you're under pressure fixing code the other roles will be neglected |
| Plan and estimate as a team | There's no I in Agile |
| Standup: what you did yesterday, what you're doing today, and highlight any blockers | Extended discussions must be between interested parties only and after the standup |
| Estimate in complexity points | Humans are great at comparing task _sizes_; time-based estimates only work if you've done exactly the same job before |
| 10% of your time should be refining the backlog | You don't have to have a dedicated group session for this |
| Do the hard bit first | It feels like progress picking those "low hanging fruit", but you've still got to chop that tree down |
| Don't add tickets for admin | You're tracking dev done not hours in the day |


<!--
The first rule of Agile: Agile meetings are for discussing work not how to do Agile
-->

## Complexity-based estimation

If you roughly break down a task, it's quite nice to order the sub-tasks in terms of complexity. And it helps direct you to which bits need breaking down further. Once they are broken down, they are more likely to be shared between the team: we are empowering the team to select tasks, not handing out jobs to individuals. Similarly, the total complexity points is the only metric that is reported; not who did the most.

> Q. What if I have 200 simple file edits to do?

> A. Then you have a boring afternoon ahead of you, or you write a script so you don't have to.

Breaking down tasks into smaller actions is highly encouraged as low-complexity jobs are unlikely to be blocked or prolonged by the unforeseen/unknown. E.g., a 13-point task with no detail has an increased likelihood of slipping into the next sprint. But when a task is incomplete, always re-estimate: we are measuring how good we are at _completing what we promised_; otherwise you just end up tracking the inexorable passage of time.
