# Blackout Friday

Hit it to the man by withdrawing cash and spending it locally: is it effective?

## Potential Effects

- Liquidity Pressure (in extreme cases): If a massive number of people withdrew cash at once, banks might face short-term liquidity strain. However, they usually have mechanisms in place to manage withdrawals.
- Psychological & Awareness Impact: A coordinated effort could raise awareness about financial systems and how much people rely on them.


## Limitations

- Money Stays in the System: If you withdraw cash but later deposit or spend it in a way that flows back into banks, the long-term impact is minimal.
- Bank Reserves Are Managed: Banks keep only a fraction of deposits as cash, but they can adjust reserves or borrow to compensate for large withdrawals.
- Scale Required: For real disruption, millions of people would need to participate simultaneously.

## Alternatives

- Switching to Credit Unions or Decentralized Finance (DeFi): Moving funds out of major banks and into alternative financial systems can have a longer-lasting impact.
- Targeted Spending or Boycotts: Avoiding transactions with certain corporations or banks can be more effective than just withdrawing cash.
- Supporting Ethical Banks: Moving funds to banks with better ethical practices influences the industry more directly.

