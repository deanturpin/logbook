# Automatic watches
## Brands that appreciate in value
- Rolex
- Patek Philippe
- Vacheron Constantin

## Watches that lose their RRP quickly
- Vacheron Constantin
- H Moser
- Tag Heuer

## The holy trinity
- Audemars Piguet
- Patek Philippe
- Vacheron Constantin

## Escapements
- [Lever](https://en.wikipedia.org/wiki/Lever_escapement) -- most modern watches
- [Anchor](https://en.wikipedia.org/wiki/Anchor_escapement) -- pendulum clocks
- [Coaxial](https://en.wikipedia.org/wiki/Coaxial_escapement) -- recent Omegas
- [Detent](https://www.timeandwatches.com/p/the-detent-escapement-from-marine.html) -- detent

## Spares and tools
- https://gleave.london/
- https://www.polywatch.de/en/
- https://www.hswalsh.com/

## Timing
|Beats per second | Vibrations per hour |
|---|---|
| 3	  | 10800|
| 4	  | 14400|
| 5	  | 18000|
| 5.5 | 19800 |
| 6	  | 21600|
| 7	  | 25200|
| 8	  | 28800|

## Trench watches
> The Trench watch (wristlet) was a type of watch that came into use by the military during World War I, as pocket watches were not practical in combat. It was a transitional design between pocket watches and wristwatches, incorporating features of both.

> The very first watch that somebody adapted to wear on a wrist is unknown. The first series of purpose-made men’s wristwatches was produced by Girard-Perregaux in 1880 for the German Navy During World War I numerous companies, including Omega, Longines, and others produced wristwatches for the military. These watches were of virtually identical style with an enamel dial, wide white numerals, and a luminescent radium hour hand. Often they did not bear the name of the manufacturer, though the movement, originally designed in the 1890s for ladies’ pendant watches, was marked "Swiss".

https://en.wikipedia.org/wiki/Trench_watch

## Under a grand
- [Swatch sistem 51](https://www.swatch.com/en-gb/body-amp-soul-yas100g/YAS100G.html)
- Hamilton Khaki
- Tissot Gentlemen
- Christopher Ward c65 trident vintage
- Tudor black bay 58 (keeps value)
- Breitling superocean
- Nomos Orion
- Baume and Mercier Clifton baumatic
- Grand Seiko spring drive

## Cheap but good
- Seiko SKX
- Vostok Amphibia
- Bulova
- Orient Bambino
- Lunar pilot

## Skeleton watches
- Sturling Anatol
- Bulova

## Misc
- Tissot PRX
- Seiko cocktail time
- Zelos nova
- Grand Seiko snowflake
- Sugess tourbillon
- Vincero watches

## Build your own
- https://shop.diywatch.club/
- https://jeekwatch.com/jk-31-mens-sale-watches-custom-logo/

## How to spot a fake
- https://www.truefacet.com/guide/spot-fake-hublot-watch/amp/

## Stores
- https://www.chrono24.co.uk
- https://www.watchfinder.co.uk
- https://teddybaldassarre.com
- https://prideandpinion.com
- https://delraywatch.com
- https://thetimetellershop.com/
- https://www.cousinsuk.com

## Tools
### Bergeon
- 30080-B - Orange - 0.50mm
- 30080-C - White - 0.60mm
- 30080-D - Yellow - 0.80mm
- 30080-E - Black - 1.00mm
- 30080-F - Red - 1.20mm
- 30080-G - Grey - 1.40mm
- 30080-H - Violet - 1.60mm
- 30080-J - Green - 2.00mm
- 30080-K - Blue - 2.50mm
- 30080-L - Brown - 3.00mm

## Timekeeping challenges
- https://en.wikipedia.org/wiki/Long_Now_Foundation
- https://en.wikipedia.org/wiki/Time_formatting_and_storage_bugs

## References
- [Swiss watch schematics](https://oldswisswatches.com/watch-parts-branded/omega-calibre-movement-and-spare-parts/omega-601/)
- [Ranfft calibre library](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&2&2uswk&Omega_601)
- [Types of pocket watch](https://www.pocketwatch.co.uk/types-of-pocket-watch)
- https://swisstimeservices.co.uk/glossary/
- https://deployant.com/watches-101-the-wheel-train-assembly-of-a-movement-at-the-lange-akademie/
- https://en.wikipedia.org/wiki/List_of_most_expensive_watches_sold_at_auction

