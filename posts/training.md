# Teach yourself things

## Subscription-based

- https://www.algoexpert.io/
- https://www.pluralsight.com/

## c++

- [Cpp quiz](https://cppquiz.org/)
- [Teach yourself C++ in 45 years](https://germs-dev.gitlab.io/cs/)

