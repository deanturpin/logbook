---
title: Learning circular breathing
subtitle: On the didgeridoo
date: 2020-09-13
tags:
- music
- didgeridoo
- breathing
- meditation
---

# Learning circular breathing
I've been playing for a few months so it’s time to give something back.

I gave myself a month to learn circular breathing. You need to immerse yourself
in the culture and practice: go into a YouTube flat spin. Commit to 20 minutes
a day, no excuses. And it’s difficult. The first few weeks you’ll be building
up the muscles you haven’t used before and it’s very hard to measure
progression. But it’s the repetition that’s important.

I thought circular breathing would click after a while and then I’d be able to
do it for an hour. I've learnt a few things but nothing so simple yet profound
as the didge: it is just a pipe after all.

You first learn to breathe in and then you realise that sometimes you have to
breathe out on your circular breath -- it's all about balancing the pressure
not just replacing air -- and then you find you want to smooth out the
transition which is a whole other beast.

The big thing for me which I haven’t seen anyone mention explicitly is that the
push is not a tight squeak like a trumpet but a slow raspberry sound: really
make your lips flap.
