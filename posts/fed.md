---
title: The federal preserve
subtitle: "Fermentation: the art of cooking without cooking"
date: 2019-11-01
tags:
- food
- recipe
- fermenting
- sauerkraut
---

# The federal preserve
## Salt
All these recipes use a percentage of salt to allay the progression of
pathogens in favour of the good bacteria. All measurements are by weight, so if
1.6% salt is required then 1000g of veg would need 16g of salt. But you should
always taste the mix during preparation. 2% salt is the standard but I go down
as far as 1.6% with sauerkraut and haven't experienced much mould.

## Cleaning
Fermentation vessels don't need to be sterilised but I like to at least run
everything through the dishwasher beforehand. Additionally I fill the
jars/crocks with boiling water prior to use and/or heat in a low oven for 20
minutes or so. I set it to just greater than boiling so the liquid can
evaporate but any plastic vessels will survive. I often repurpose regular 500ml
jars but it's nice to use a proper crock with an airlock.

## Basic principles of fermented cabbage
There's plenty of shop-bought kimchi in Brighton but very few in eco-friendly
packaging. So why not make your own?

- 100% red cabbage (loose, Infinity Foods)
- 25% wild garlic (wild, The Downs)
- 1.6% salt
- 2% red chili (loose, Taj)

Chop everything and mix in the salt. Crush with your hands to get the juices
flowing. Leave for ten minutes and repeat until there's a good amount of liquid
in the bottom of the bowl. If the cabbage is particularly tough to get juice
out of give it a bash with a pestle and leave it alone whilst the salt gets to
work. Repeat a couple of times until you have adequate juice: you need enough
liquid to submerge the cabbage in the fermenting vessel. You can buy weights to
keep the loose bits submerged but if you're using an airlock you needn't
bother.

Pack into a glass jar leaving about 20% expansion space and close the lid. Keep
out of the sunlight, it should start to bubble by day two. Taste it after a
week. Is it delicious? If so, eat on homemade bread with aioli.

The flavour profile evolves over the course of a few weeks. If you manage to
not eat it all you'll notice the taste drops off after a few weeks. If you
really like the taste on a certain day move it to the fridge to slow it down.

## Fermented salsa
- Two large tomatoes, coarsely chopped
- Half as much onion as toms, 5mm dice
- Two garlic cloves, finely chopped
- A few radishes, finely sliced
- One red chilli, finely sliced

The denser the vegetable the finer it needs cutting.

Put everything in a mixing bowl on a scale, add 1.6% of the weight of the veg
in salt. Mush it between your fingers until mixed and quite liquid. Taste and
then add to a jar, almost to the brim, pop the lid on and leave in a dark
cupboard for up to a week. You can put it in a fridge if you like but the
acidity should stop anything untoward occurring.

## Okraut
An experiment to see how okra behaves in a ferment. The addition of okra does
make it quite slippery so possibly not for everybody.

## Natto
Soak your dried soy beans overnight. I half fill a sealable container and fill
it up with water. Overnight the beans will have expanded to the top of the
container. Steam the beans in a pressure cooker for half an hour and then
inspect to see how tender they are. They should be almost falling apart so if
they still have some bite add more water to the pressure cooker and stick them
in for another half an hour.

Once you're happy with your beans line a baking tray with greaseproof with the
intention of folding it over into a sealed parcel. Add your beans whilst hot
(the heat doesn't matter in this case) and mix in either a pot of frozen natto
or a few tablespoons from your previous batch. Mix well.

Fold the greaseproof over into a parcel and do you best to seal it. Leave the
package in a warm place for 24 hours. I leave it beneath a downlighter in the
kitchen. Pop into a clean jar and keep in the fridge.

## Worcestershire sauce
- Barley Malt Vinegar
- Spirit Vinegar
- Molasses
- Sugar
- Salt
- Anchovies
- Tamarind Extract
- Onions
- Garlic
- Spice
- Flavourings (soy sauce, lemons, pickles and peppers)

## Bloody Mary
### Basic recipe
- vodka
- tomato juice
- Worcestershire sauce
- Tabasco sauce
- piri piri sauce
- beef consommé or bouillon
- horseradish
- celery
- olives
- salt
- black pepper
- cayenne pepper
- lemon juice
- celery salt

### Blanche House recipe
- 50ml Vodka or Gin
- 25ml fresh lemon juice
- 7 shakes of Worcestershire Sauce
- 2 generous dashes of Tabasco
- 5 turns of black pepper
- A large pinch of celery salt
- A teaspoon of creamed horseradish
- 150ml Tomato Juice
- Shake and strain into a slim Jim

### Turpin recipe
- Tomatoes 685
- Shallots 126
- Tamari tsp
- Molasses tsp
- Bullet peppers 29
- Tomato vine
- Celery 92
- Garlic 6
- Olives 19
- Peppercorns 16 (corns, ground)
- Salt 15 (1.6% weight of veg)
