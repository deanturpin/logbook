---
title: CG resources
subtitle: Resources and vocabulary
date: 2020-10-25
tags:
- 3d
- blender
- modelling
- cgi
---

# CG resources

## HDR
- https://hdrihaven.com/

## Textures
- https://texturebox.com/
- https://www.poliigon.com/

## Models
- https://sketchfab.com/3d-models

## 8K videos
- https://www.youtube.com/watch?v=1La4QzGeaaQ
- https://www.youtube.com/watch?v=7k2uKb9vCOI

## 4K videos
- ["Spring" (Blender)](https://www.youtube.com/watch?v=WhWc3b3KhnY)

