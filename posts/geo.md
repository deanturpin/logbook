---
title: Geospacial
date: 2020-12-05
tags:
- geo
- geospacial
---

# Geospacial

- "[GeoJSON](https://en.wikipedia.org/wiki/GeoJSON) is an open standard format
designed for representing simple geographical features, along with their
non-spatial attributes. It is based on the JSON format."
- Sequalize
- [GIS -- geographical information
standard](https://en.wikipedia.org/wiki/GIS_file_formats)
- "[GML -- geography markup
language](https://en.wikipedia.org/wiki/Geography_Markup_Language) is the XML
grammar defined by the Open Geospatial Consortium (OGC) to express geographical
features."
- "[PostGIS](https://en.wikipedia.org/wiki/PostGIS) is an open source software
program that adds support for geographic objects to the PostgreSQL
object-relational database."

