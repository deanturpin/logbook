# Taking photos of groups of people

People are drawn to walls when lining up for a photo. But while the "firing
line" seems a basic instinct, don't do it! Casting distracting shadows on the
walls with on-camera flash. And don't be bullied into taking a picture next to
the toilets.

Make a habit of looking beyond the subject. Move them if there's something
offensive. Even say why you're moving them: "Can I just move you this way a
little, please? There's a tree just behing your head," or, "the light is
much nicer over here." People respond to vague statements like "nice light"
-- even if it's inquantifiable. -- and most of the time they will want to
look their best.

In a live environment the human eye is particularly good at filtering out the
background cruft, and focussing attention on the matter in hand. Not so when
that scene is projected onto a flat screen, where a road sign can suddenly
spoil what would otherwise be a great picture. So take the time to look beyond
the subject, you may not get another chance at the same shot.  However, if you
do spot a pylon sticking out of the bridesmaid's head, just make sure you don't
do it again with the bride.

In places bereft of space the wide-aperture lens rules, blowing any street
detritus into soothing bokeh; but do make sure all the important bits (eyes)
are equidistant from you. And look for leading lines drawn by the blurred
background, for possible incorporation into the picture.

Moving the camera changes the subject only a little; but every step, crouch,
or tip-toe yields a completely new canvas. So move a little when framing:
offensive things in the background pop out when they're not stationary.

Sometimes you should be paying as much attention to the background as the
subject.

If there are two of you: get a second opinion. Your shooting buddy may be
more objective about the overall shot.

Look for good spots and problems before you even lift your camera. Indoors:
is there a TV on an otherwise good plain wall? Can it be covered with a
towel?

Eyes are also very good at white-balancing. When shooting in a room
illuminated by a large window, are there any other lights on in the room?
Can they be turned off to avoid multiple-source colour-correction problems?

