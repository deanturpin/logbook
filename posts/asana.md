# A curated list of yoga poses

```csv
Transliteration,Sanskrit,English,Description,URL,Type,Variation
Sukhasana| Yogasana | Svastikasana,सुखासन| स्वस्तिकासन,Auspicious| Lucky mark,,,Warm up,
Siddhasana | Muktasana | Siddha Yoni Asana,सिद्धासन| मुक्तासन,Liberated| Accomplished| The Adept's Pose,Seated cross-legged with knees down,,Warm up,
Shavasana,शवासन,Corpse,,,Warm up,Butterfly legs | buttterfly legs and arms | reverse corpse (lying on the front)
Pavan Muktasana | Apanasana,पवन मुक्तासन| अपानासन,Wind Release pose,Supine with knees to chest,,Warm up,
Ardh Pavan Muktasana | Apanasana,अर्ध पवन मुक्तासन| अर्ध अपानासन,Half Wind Release pose,Supine with one knee to chest,,Warm up,
Ananda Balasana,आनन्द बालासन,Happy Baby| stirred up,,,Warm up,Straight legs
Jathara Parivartanasana,ञटर परिवर्तनासन,Belly twist,Lie on back and windscreen wiper the legs over to one side,,Warm up,Cat tail: hold trailing foot
Makarasana,मकरासन,Crocodile,Supine with elbows down and supporting head in hands,,Warm up,
Bidalasana | Marjariasana,बिडालासन| मार्जरीआसन,Cat / Cow,On all fours arch back upwards then downwards,,Kneeling,Reverse hands | side bend look backwards on cow
Tadasana,ताडासन,Mountain,Standing tall with hands by sides,,Warm up,Side bend | back bend
Pranamasana,प्रणामासन,Prayer,Standing with hands in prayer,,Warm up,
Uttanasana,उत्तानासन,Standing Forward Bend,,,Fold,Prayer hands | revolving | hands clasped behind back | holding opposite elbows | holding calves
Parighasana,परिघासन,Gate,,,Kneeling,
Adho Mukha Svanasana,अधो मुखश्वानासन,Downward-Facing Dog,,,Shoulders,
Ardh Pincha Mayurasana,अर्ध मयूरासन,Dolphin,Down face dog with forearms down,,Shoulders,
Virabhadrasana I,वीरभद्रासन |,Warrior I,,,Legs,
Virabhadrasana II,वीरभद्रासन II,Warrior II,,,Legs,Reach backwards (like Trikonasana)
Anjaneyasana,अञ्जनेयासन,Crescent Moon/lunge,,,Legs,
Viparita Virabhadrasana,विपरीत वीरभद्रासन,Reversed Warrior | Sun warrior,,,Legs,
Dragon,अजगरासन,Dragon,Deep lunging warrior,,Legs,Hands on floor | arm behind back | turn chest upward
Trikonasana | Utthita Trikonasana,त्रिकोणासन | उत्थित त्रिकोणासन,Triangle,,,Fold,
Reversed Trikonasana,त्रिकोणासन,Reversed triangle,,,Fold,
Parshvottanasana,पार्श्वोत्तनासन,Short trikonasana with forward fold,,,Fold,Clasp hands behind back
Parshvakonasana,पार्श्वकोणासन,Side angle,,,Legs,
Utthita Parsvakonasana,उत्थित पार्श्वकोणासन,Extended Side Angle,,,Legs,
Virabhadrasana III,वीरभद्रासन III,Warrior III,,,Balance,
Ardh Chandrasana,अर्ध चन्द्रासन,Half moon,,,Balance,
Utkatasana,उत्कटासन,Awkward | Powerful | Chair,,,Squatting,Lift one foot from the floor
Ardh Utkatasana,अर्ध उत्कटासन,Awkward pose with hands clasped and palms forward,,,Squatting,Lift one foot from the floor
Revolved Utkatasana,उत्कटासन,Awkward pose with twist,,,Squatting,
Vrikshasana | Ek pad pranamasana,वृक्षासन,Tree | one foot prayer,,,Balance,
Garudasana,गरुडासन,Eagle,,,Balance,
Single leg Utkatasana,उत्कटासन,Single leg chair,Pistol squat,,Squatting,
Prasarita Padottanasana,प्रसारित पादोत्तानासन,Wide Stance Forward Bend,,,Fold,Clasp hands behind back
Natarajasana,नटराजासन,Lord of the Dance | Nataraja's Pose,,,Balance,
Utthita Hastapadangusthasana,उत्थित हस्त पादाङ्गुष्ठासन,Standing Big Toe Hold,,,Balance,Leg to the side | leg to the front
Trivikramasana | Svarga Dvijasana,त्रिविक्रमासन,Trivikrama's pose | Durva’s pose | Bird of Paradise pose,Standing splits,,Splits,
Kumbhakasana | Phalakasana,,Plank,Straight armed plank,,Plank,One hand | one hand one foot
Chaturanga Dandasana,चतुरङ्ग दण्डासन,Four-Limbed Staff,Low Plank,,Plank,
Ashtanga Namaskara | Chaturanga,अष्टाङ्ग नमस्कार,Eight-Limbed Salutation | Caterpillar,,,Plank,One leg
Vasishtasana,वसिष्ठासन,Vasishta's pose,Side plank,,Plank,Extend top leg
Bhairavasana,भैरवासन अण्कुशासन,Formidable,Side plank with front leg behind head,,Plank,
Balasana,बालासन,Child,,,Kneeling,Wide legs | arms back
Vajrasana,वज्रासन,Thunderbolt,Sitting on the heels,,Kneeling,
Broken toe pose,,Broken toe pose,Kneeling with bent toes,,Kneeling,
Bharadvajasana,भरद्वाजासन,Bharadvaja's twist,Kneeling to one side with upper body twist,,Kneeling,Half lotus
Virasana | Dhyana Virasana,वीरासन | ध्यान वीरासन,Hero | Hero's Meditation,,,Quads,
Laghu Vajrasana,लघु वज्रासन,Little Thunderbolt,,,Quads,
Supt Virasana,सुप्त वीरासन,Reclined hero,,,Quads,
Mandukasana,मन्दुकासन,Frog,Face down wide kneel,,Kneeling,
Malasana,मालासन,Garland,Wide knee squat,,Squatting,
Pashasana,पाशासन,Noose,Squat with bind,,Squatting,
Simhasana,सिंहासन,Lion,Wide kneel| reversed hands and tongue out,,Hips,
Ardh Padmasana,अर्ध पद्मासन,Half lotus | square pose,,,Lotus,
Gorakshasana,गोरक्षासन,Cowherd | Gorakhnath's pose,Seated with knees spread and soles of feet pressed together,,Lotus,
Gomukhasana,गोमुखासन,Cow face,,,Lotus,Shoelace – lean over legs
Parighasana,पद्मासन,Lotus,,,Lotus,
Lolasana,लोलासन,Pendant,Start kneeling and elevate legs with arms,,Arms,
Tulasana,तुलासन,Balance | Scales,Elevated lotus with arms outside legs,,Lotus,
Kukkutasana,कुक्कुटासन,Cockerel,Elevated lotus with arms inside legs,,Arms,
Garbha Pindasana,गर्भासन,Embryo in Womb,Lotus holding face with arms through legs,,Lotus,
Ardh Baddh Padmottanasana,अर्ध बद्ध,Half lotus forward fold,,,Lotus,
Ek pada galavasana,एक पद गलवनासन,Flying pigeon,,,Lotus,
Maksikanagasana,मक्सिकानागासन,Dragonfly | Hummingbird | Grasshopper,,,Arms,
Ardh Baddh Padma Prapadasana,अर्ध बद्ध पद्म प्रपदासन,Half lotus tiptoe,,,Lotus,
Supt Trivikramasana,सुप्त त्रिविक्रमासन,Trivikrama's pose,Supine front splits,,Splits,
Rajakapotasana,राज कपोतासन,King Pigeon,,,Splits,
Hanumanasana,हनुमनासन,Hanuman | Monkey,Front splits with arms pointing upwards,,Splits,
Samakonasana,समकोणासन,Side splits,,,Splits,
Baby Kakasana,ककासन,Baby crow,Crow with forearms on the floor and knees to triceps,,Arms,
Kakasana,ककासन,Crow (arms bent),,,Arms,
Bakasana,बकासन,Crane (straight arm crow),,,Arms,
Dvi Pad Koundinyasana,द्वि पद कौण्डिन्यसन,Side crow,,https://en.wikipedia.org/wiki/Koundinyasana,Arms,
Ek Pad Koundinyasana,एक पद कौण्डिन्यसन,Side crow with split legs,,https://en.wikipedia.org/wiki/Koundinyasana,Arms,One elbow down
Tittibhasana,टिट्टिभासन,Firefly,,,Arms,
Astavakrasana,अष्टावक्रासन,Aṣṭāvakra's pose | eight-angle pose,,,Arms,
Mayurasana,मयूरासन,Peacock,,,Arms,
Vrischikasana,वृश्चिकासन,Scorpion,,,Inversion,
Shirshasana | Kapalasana,शीर्षासन,Headstand | forearm balance,,,Inversion,Butterfly legs | rotated splits
Straight arm Shirshasana,शीर्षासन,Straight arm headstand,,,Inversion,Palms up
Pincha Mayurasana,पिञ्च मयूरासन,Feathered Peacock,,,Inversion,
Adho Mukha Vrikshasana,अधो मुख वृक्षासन,Handstand | Downward-Facing Tree,,,Inversion,
Viparita Karani | Uttanapadasana,विपरीतकरणि,Supported back bend,With shoulders down and legs upwards,,Backbend1,Legs up the wall
Bhujangasana,भुजङ्गासन,Cobra,,,Backbend1,
Bhujapidasana,भुजपीडासन,Arm-pressing posture | seal | sphinx,,,Backbend1,
Urdhv Mukh Shvanasana,ऊर्ध्व मुख श्वानासन,Upward-Facing Dog,,,Backbend1,
Dhanurasana,धनुरासन,Bow,Prone backbend holding ankles,,Backbend1,
Shalabhasana,शलभासन,Locust,Prone backbend with legs elevated and straight arms pushing forward,,Backbend1,Face down with hands flat
Sarpasana,सर्पासन,Snake,Prone backbend with legs elevated and straight arms pushing backwards,,Backbend1,
Janu shirshasana,जानु शीर्षासन,Head to knee,Seated forward fold with one leg bent,https://en.wikipedia.org/wiki/Janusirsasana,Fold,
Kraunchasana,क्रौञ्चासन,Heron,Seated with one foot back like hero and lift straight leg to head,,Fold,
Parivrtt Sury Yantrasana,परिवर्त्त सूर्य यन्त्रासन,Compass Pose,Seated with one leg bent and other straight behind shoulder,,Fold,
Ushtrasana,उष्ट्रासन,Camel,,,Backbend2,
Matsyasana,मत्स्यासन,Fish,Supine backbend with hands under glutes and back of head on floor,,Backbend2,
Urdhva Dhanurasana | Chakrasana,ऊर्ध्व धनुरासन | चक्रासन,Upwards-facing bow| wheel,,,Backbend2,
Ek pada urdhva dhanurasana,एक पद ऊर्ध्व धनुरासन | एक पद चक्रासन,One foot upwards-facing bow,,,Backbend2,Lift opposite hand
Viparita Dandasana,विपरीत दण्डासन,Inverted Staff,,,Backbend2,
Ek pada viparita dandasana,एक पद विपरीत दण्डासन,One foot inverted Staff,,,Backbend2,
Karnapidasana,कर्णपीडासन,Ear-pressing,,,Shoulders,
Sarvangasana,सालम्ब सर्वाङ्गासन,Shoulder Stand,,,Shoulders,Split legs
Niralamba Sarvangasana,विलम्ब सर्वाङ्गासन,Unsupported shoulder Stand,,,Shoulders,
Setu Bandh Sarvangasana,सेतु बन्ध सर्वाङ्गासन,Shoulder supported bridge,,,Shoulders,
Halasana,हलासन,Plough| snail,Shoulder stand with legs straight over head,,Shoulders,
Dandasana,दण्डासन,Staff,,,Seated,
Baddh Konasana | Bhadrasana,बद्ध कोणासन,Bound angle | Cobbler's pose,,,Seated,Lean to front and side
Akarna Dhanurasana,आकर्णधनुरासन,Shooting bow | Archer | Bow and arrow,,,Seated,
Navasana | Paripurna Navasana | Naukasana,नावासन | परिपूर्णनावासन | नौकासन,Boat | Full Boat,,,Seated,
Marichyasana,मरीच्यासना,Marichi's Pose,Seated one leg straight with upper body twist,,Squatting,
Matsyendrasana,मत्स्येन्द्रासन,Lord of the Fishes | Matsyendra's pose,Loose cow face with upper body twist,,Seated,Bind
Paschimottanasana,पश्चिमोत्तानासन,Seated Forward Bend,,,Fold,
Upavishta Konasana,उपविष्टकोणासन,Open Angle,Seated straddle forward fold,,Fold,
Kapotasana,कपोतासन,Pigeon,,,Hips,
Kurmasana,कूर्मासन,Tortoise,Straddle forward fold with arms under legs,,Hips,
Yoganidrasana | Pasini Mudra,योग निद्रासन,Yogic sleep | Noose Mudra,Both legs behind head,,Hips,
Anantasana,अनन्तासन,Ananta's pose | Vishnu's Couch pose,Straight leg lift lying on the side,,Hips,
Ardh Bhekasana,अर्ध भेकासन,Frog,Prone backbend holding top of one foot,,Quads,
Bhekasana,भेकासन,Frog,Prone backbend holding top of feet,,Quads,
Supt Padangusthasana,सुप्त पादाङ्गुष्ठासन,Reclining hand to big toe pose,Supine straight leg raise,,Fold,
```

