---
title: योग
subtitle: Learning Sanskrit through Yoga
date: 2019-10-07
tags:
- sanskrit
- yoga
- hindi
- language
---

# योग

## Forward
> I originally began learning ગુજરાતી (Gujarati) around 2001 as a means to better understand my family heritage. But it has since expanded into an appreciation of other Indic languages and even right-to-left writing systems such as Arabic and Urdu.
> Whilst ગુજરાતી (Gujarati) came first for me but it wasn't a huge leap to हिन्दी (Hindi) and संस्कृतम् (Sanskrit).
>
> In the first part of the book I will break down selected yoga poses as a "way in" to the language. Learning to read and write will add another dimension to your understanding and practice and there are fascinating parallels with English and Arabic too once you get into it. It will feel daunting but the rewards are high.
>
> The second part is a comprehensive list of asana that will hopefully be of use even if you don't feel confident with Sanskrit.
>
> Indic words are followed with the English translation in brackets and where appropriate the English transliteration in double quotes.
>
> E.g., सुप्त (supine) "supt"
>
> You should be able to type the transliterations directly into Google Translate.

---

## Part one | संस्कृतम् (Sanskrit)
You can get by using [English transliterations](https://en.wikipedia.org/wiki/Devanagari_transliteration) alone but it's never going to be as expressive as learning the script itself. संस्कृतम् (Sanskrit) words can be more elegant as there are language features that simply aren't available in Latin languages. Also some sounds don't exist. For instance व (V) is somewhere between English V and W, so what's the equivalent in the Latin alphabet?

हिन्दी (Hindi) is the closest living language to संस्कृतम् (Sanskrit) and one of several [Devnagari](https://en.wikipedia.org/wiki/Devanagari) languages found in central and northern India, contrasting with the [Dravidian](https://en.wikipedia.org/wiki/Dravidian_languages) languages of the south.

Just as native speakers of the same language have different pronunciation of same word, using transliterations makes that two fold. And often an English rendering doesn’t do the original script justice.

## आसन (pose)
This seems as good a place to start as any. आसन (asana) is transliterated as the five letter word "asana" in English yet it comprises only three Sanskrit letters: आ (A), स (S), न (N). In English vowels only come after consonants so you might be surprised to find that in संस्कृतम् (Sanskrit) vowels can appear before, after, above, below or indeed not at all!

### Schwa
When consonants have no vowel between them there is an implicit [schwa](https://en.wikipedia.org/wiki/Schwa) -- "uh" sound -- so to join two sounds together you must use a special diacritic character to indicate it as a consonant cluster or use the oftentimes complex compound characters (ligatures) which संस्कृतम् (Sanskrit) makes extensive use of compared to हिन्दी (Hindi). आसन (asana) has two implicit schwas: AS(A)N(A). And consider "lift" and "Lidl" as pronounced by an English spearker: why do they have a different number of syllables?

## Dental and retroflex consonants
English has just one invocation of the letter D where the tongue tip is somewhere between the teeth and the roof of the mouth. Indic languages have two: dental and retroflex. So दीन "dean" and डीन "dean" sound the same to an English speaker but in हिन्दी (Hindi) they are distinct words.

## Modifiers
Poses often have modifiers like सुप्त (supine) and अधो (downward) that affect the basic shape.

- सुप्त "supt" (supine)
- अधो "adho" (downward)
- अर्ध "ardha" (half)
- उत्थित "utthita" (extended/raised)
- विपरीत "viparit" (reverse/adverse)
- बद्ध "baddh" (bound)
- एक पद "ek pad" (one foot)

## Vowels
Let's introduce andother vowel.

Note the hook beneath the first character in सुप्त (supine). Individually we have स (S), उ (U), प (P) and अ (T). In this context the उ (U) has a simpler form and hangs effortlessly from the leading स (S).

## Pose name construction
Each pose has a name and is suffixed with आसन. E.g., "boat pose" consists of नाव "nav" (boat) and आसन "asana" (pose). However, when the two words come together the leading A in asana takes another form: नावासन "navasana" (boat pose) - the V is pronounced without the bottom teeth touching the top lip, halfway betwween a V and W in English. This is called डंडा "danda" (stick). If you've attended an Indian wedding you may be familiar with the derived word and stick dance named डंडिया "[dandiya](https://en.wikipedia.org/wiki/Dandiya_Raas)".

## Alphabets
Strictly speaking Indic languages use [abugidas](https://en.wikipedia.org/wiki/Abugida) but let's stick with alphabet. You can learn the alphabet in a couple of weeks but you have to use it to keep it in your head. Perhaps transliterate a daily newspaper headline or use a Hindi calendar.

Via [Omniglot](https://www.omniglot.com/writing/sanskrit.htm). Compare the top row of each.

### Hindi
![](https://www.omniglot.com/images/writing/hindi_cons.gif)

### Sanskrit
![](https://www.omniglot.com/images/writing/sanskrit_cons.gif)

### Exercise
Try typing your name into Google Translate and play around with the length of the vowels and dental/retroflex consonants using shift.

- दिन "din" - short E, retroflex D
- दीन "dIn" - long E, retroflex D
- डीन "DIn" - long E, dental D
- टुर्पिन "Turpin" - retroflex T
- तुर्पिन "turpin" - dental T

See a [list of Sanskrit and Persian roots in Hindi](https://en.wikipedia.org/wiki/List_of_Sanskrit_and_Persian_roots_in_Hindi).

## Translation tools
[Google Translate](https://translate.google.com/) is the go-to tool for translations. The phonetic alphabet keyboard is really useful but the handwriting recognition tool is surprisingly good. However, it doesn't offer Sanskrit.

I generally use the phonetic keyboard unless I need something weird that can't be expressed by the transliteration.

## Familiar words
Some words have made their way into the English consciousness.

### औं
"om"

This familiar chant is probably the most familiar word to Westerner ears.

### नमस्ते
"namaste"

Closely followed by नमस्ते "namaste" (welcome/hello/thankyou). Breaking it down we have न "n", म "m", स्ते "stay". Note an "n" on its own is not pronounced "na" like "nap" but more like children are taught the alphabet: "luh, muh, nuh".

---

## Part two | आसन "asana"
See the [full list of asana](https://gitlab.com/deanturpin/asana/blob/master/asana.csv) and today's [yoga sequence of the day](https://deanturpin.gitlab.io/yogr/).

### मुक्तासन, सिद्धासन
"Muktasana, Siddhasana, Siddha Yoni Asana"

English: Liberated, Accomplished, The Adept's Pose

Notes: Seated cross-legged with knees down

### लघु वज्रासन
"laghu vajrasana"

लघु (little/small) वज्र (thunderbolt) आसन (pose)

### ञटर परिवर्तनासन​
"jathara parivartanasana"

English: Belly twist

Notes: Lie on back and windscreen wiper the legs over to one side

Variation: Cat tail - hold trailing foot

### अधो मुख श्वानासन
"adho mukh shvan asana"

अधो (downward) मुख (mouth/face) श्वाना (dog) आसन (pose)

### ऊर्ध्व मुख श्वानासन
"urdva mukh shvan asana"

ऊर्ध्व (upward) मुख (mouth/face) श्वान (dog) आसन (pose)

### अर्ध पवन मुक्तासन
"ardha pavan mukt asana"

अर्ध (half) पवन (wind) मुक्त (release/put) आसन (pose)

### त्रिकोणासन
"trikon asana"

त्रिकोण (triangle) आसन (pose)

### राज कपोतासन
"raj kapot asana"

राज (king) कपोत (pigeon) आसन (pose)
