# C++ features

> Moved to [turpin.dev](https://turpin.dev/).

## References

- https://devdocs.io/cpp/23
- https://en.wikipedia.org/wiki/C%2B%2B23
- https://en.wikipedia.org/wiki/C%2B%2B20
- https://en.wikipedia.org/wiki/C%2B%2B17
- https://turpin.one/posts/fav.html

