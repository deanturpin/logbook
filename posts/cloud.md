---
title: Cloud computing
date: 2019-09-15
tags:
- revision
- cloud
- linux
- vm
- cli
---

# Cloud computing

- [Play With Docker](https://labs.play-with-docker.com/)
- [Google Cloud](https://console.cloud.google.com/)
- leaf.cloud (powered by green energy)
- linode.com
- cloudflare.com
- [Amazon Web Services](http://ec2.amazon.com)
- [Digital Ocean](https://cloud.digitalocean.com)
- Hybrid Cloud

## Cloud9 versus Google Cloud SSH
- Google's is slightly slicker, more tightly integrated with the instance
browser.
- The Google web SSH client is so close to the real thing that you quickly
forget what's going on.
- Cloud9 IDE is quite nice, Sublime-style editor. But slightly odd ctrl codes
remind you that you're not actually typing into a real terminal.
- Cloud computing lets you put a price on solving a complex problem such as
cracking a complex password.

## Exploit location
With cloud computing you can run your code where it gives you an advantage,
often communication latency.

## Backups
Your backup strategy should be proportion to the cost of recreating lost data.
Similarly encryption strength should be proportional to the sensitivity of the
data.
