# The cost of inconvenience

It’s interesting how long you can put up with something for. You make the judgement that it’s less important than the job you’re tasked with. And two years later you’re still doing some weird startup sequence. Or you type a password a few times because you can't be bothered sharing SSH keys.

Perhaps occasionally we should stop to review our processes?
