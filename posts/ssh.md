---
title: But does it ssh?
subtitle: Using an old tablet to login to a Google Cloud instance
date: 2019-06-27
tags:
- linux
- google cloud
- ssh
- tablet
- bash
---

# But does it ssh?

Making use of an old Amazon Fire tablet and a cheap Bluetooth keyboard to login
to an 8-core Linux instance. The web ssh client doesn't work properly on any
browser on my iPhone but inexplicably it's fine in an old Silk! I *think*
there's some JavaScript failing on my modern phone that just gets ignored on
the Amazon tablet.

<iframe width="700" height="525" src="https://www.youtube.com/embed/ZvpoW4Le1e4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
