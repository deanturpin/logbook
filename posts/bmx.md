# BMX

## Frame size
See https://bikesizechart.com/bmx.

| height | top tube (in) | crank length |
| --- | --- | --- |
| 183| 22|185|

## References
- https://www.sourcebmx.com/products/rant-party-on-v2-front-wheel?variant=31841465368687
- https://www.freenightbmx.com/
- https://www.skatehut.co.uk/bikes/bmx-parts?page=3
- https://www.sturmey-archer.com/en/products/rear-hubs
- https://sheldonbrown.com/coaster-brakes.html
