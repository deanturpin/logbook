# What I'd like to see on a job spec

## Keywords

Initially I scan for keywords that pique my interest:

- C++23
- Linux
- Networking
- git
- unit testing
- cloud
- Agile

## C++ history

Using the latest tech obviously stands out. For C++ the big shake up was C++11 -- so anything this modern is of interest -- but really we
need to be talking at least '17.

However, very few companies seem to use '20 in production but a good example is
[Maven Securities](https://www.mavensecurities.com/maven-on-c/). In fact, I do
wonder if being so cutting edge might put people off!

But if a company is this up front about keeping up-to-date then it looks good to me.

## Autonomy


In the details I like to see some hint of autonomy and trust. And you sometimes see something like "20% own project time", which is awesome.
Unstructured training -- "play" -- is really important for development, and
you'll often end up researching something that will benefit your day job
anyway.

## Sustainability

Some acknowledgement of environmental considerations also makes a company stand out. The objective of writing efficient code is usually
to maximise throughput/core usage, but an interesting additional consideration
is that we might write efficient code to minimise resource usage and power
consumption.

## Interview process

Interviewing for senior dev roles can really be a drag five interviews are common. I think an online coding test/exercise using
HackerRank or Godbolt makes a great 1<sup>st</sup> interview and is insightful
and time-saving for both parties.

## Flexible working

A mixture of office and home working is convenient and effective for employer and employee. But I think _some_ contact with your
colleagues is beneficial (rather than fully remote).

## Eat your own dog food

If a company uses their own product to do the same job as the end user then it can only result in a better product.
