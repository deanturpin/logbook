---
title: You can't always want what you get
subtitle: (Quotes)
date: 2019-04-20
tags:
- agile
---

# You can't always want what you get

- Nobody ever stopped for anybody but themselves; quitting must be as selfish as the act itself.
- I'm a simple man, cut from simple cloth. But in a really snazzy pattern.
- Bisque in the eye: is there a more middle class injury?
- I cannot be bought! However, my signing hand has had one hell of a day and does enjoy a nice Chianti.
- I think my work-life balance is fine. I just need to trick somebody into doing some of this damn work so I can go surfing.
- Economy of words: never forget that. Ever.
- "Now you mention it Fritzel did keep himself to himself." History's least successful neighbourhood watch scheme.
- We are all made of Spars.
