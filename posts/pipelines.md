#### C++

1. [![](https://gitlab.com/germs-dev/bt/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/bt/-/pipelines/) [Trading strategy backtester (stock/crypto)](https://cpp.run/)
1. [![](https://gitlab.com/germs-dev/concurrency-support-library/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/concurrency-support-library/-/pipelines/latest) [C++ concurrency support library](https://threads.cpp.run/)
1. [![](https://gitlab.com/germs-dev/dft/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/dft/-/pipelines/latest) [Discrete Fourier transforms in C++23](https://dft.germs.dev/) -- Fourier analysis without 3rd-party libraries
1. [![](https://gitlab.com/germs-dev/fix/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/fix/pipelines/latest) [Exploring the FIX protocol](https://fix.germs.dev)
1. [![](https://gitlab.com/germs-dev/hear/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/hear/pipelines/latest) [Command line hearing test](https://gitlab.com/germs-dev/hear/)
1. [![](https://gitlab.com/germs-dev/cpp/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/cpp/-/pipelines/latest) [C++ recipes](https://cpp.turpin.dev/)

#### AI

1. [![](https://gitlab.com/germs-dev/cs/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/cs/-/pipelines/) [Teach yourself C++ in 45 years](https://turpin.dev/) -- training material complemented by the OpenAI API
1. [![](https://gitlab.com/germs-dev/openai/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/openai/-/pipelines/latest) [OpenAI sandbox](https://openai.germs.dev/)
1. [![](https://gitlab.com/germs-dev/scripts/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/scripts/-/pipelines/latest) [Command line OpenAI wrapper](https://turpin.cloud/)

<!--
#### R

1. [![](https://gitlab.com/deanturpin/tradeR/badges/main/pipeline.svg)](https://gitlab.com/deanturpin/tradeR/pipelines/latest) [Currency plotting in R](https://deanturpin.gitlab.io/tradeR/)
-->

#### Web

1. [![](https://gitlab.com/germs-dev/deanturpin/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/deanturpin/-/pipelines/latest) [Online logbook](https://turpin.one/) -- using GitBook 
1. [![](https://gitlab.com/germs-dev/webmeup/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/webmeup/-/pipelines/latest) [Static website generator](https://turpin.cloud/#static-website-generator) -- a one line website generator (maybe two)
1. [![](https://gitlab.com/germs-dev/tracehost/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/tracehost/-/pipelines/latest) [Check my websites are up](https://germs.dev/) -- top level domain topology
1. [![](https://gitlab.com/germs-dev/pipelines/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/pipelines/-/pipelines/latest) [GitLab CI pipelines](https://deanturp.in/) -- this web page

#### Misc

1. [![](https://gitlab.com/germs-dev/pjsip/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/pjsip/-/pipelines/latest) [pjsip built using cmake's ExternalProject](https://gitlab.com/germs-dev/pjsip)
1. [![](https://gitlab.com/germs-dev/energy/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/energy/-/pipelines/latest) [Haskell energy usage calculations](https://energy.turpin.cloud/)
1. [![](https://gitlab.com/germs-dev/quotations/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/quotations/-/pipelines/latest) [Curated list of tech quotes](https://quotations.germs.dev/)
1. [![](https://gitlab.com/germs-dev/skills/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/skills/-/pipelines/latest) [Skills timeline generator in R](https://skills.turpin.dev/)
1. [![](https://gitlab.com/germs-dev/render/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/render/-/pipelines/latest) [Blender renders](https://render.germs.dev/)
1. [![](https://gitlab.com/germs-dev/pbwc/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/pbwc/-/pipelines/latest) [Watchmaking resources](https://watch.turpin.one/)
1. [![](https://gitlab.com/deanturpin/idrawhouses/badges/main/pipeline.svg)](https://gitlab.com/deanturpin/idrawhouses/-/pipelines/latest) [Building projects](https://build.sima.one/)
1. [![](https://gitlab.com/germs-dev/mews-one/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/mews-one/-/pipelines/latest) [Airbnb landing page](https://mews.one/)
1. [![](https://gitlab.com/germs-dev/brighton/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/brighton/-/pipelines/latest) [Brighton recommendations](https://sima.one/)

