# UK funeral expenses
The death will need to be validated by a doctor. This can be done quickly in one day, but the departed may have to be taken to hospital for a coroner's report.

## Before you have the death certificate
### Inform a funeral director
It's actually quite good to talk to somebody who knows the process.

A cremation will cost £3000+ but they won't invoice you until a week after the actual funeral. The deceased's bank may accept the invoice and deduct it from the funds even without probate.

If it happens around Christmas then the options are much more limited and it may take three weeks to finalise everything.

### Probate
If the sum of deceased's accounts exceeds a certain threshold -- probably £10,000 -- you will need to carry out legal proceedings (probate) to move the funds to the executor. A solicitor will happily do this for you but the charges will be much higher than doing it yourself. Each bank may have a diffeent probate threshold but if it's close then you can appeal to the bank (they are really considerate when handling this sort of thing). If the estate is worth less than £300,000 then it's probably not worth paying somebody to do it.

### Registering the death
This is time-critical and usually it must be registered within five days. "Usual" means a coroner wasn't involved. You'll need to collect the "certificate of cause of death" from the doctor and then make an appointment with a registrar. The registrar will give you multiple copies of the official death certificate and also a green slip that you'll need to take to the funeral director. The registrar will also ask how many copies of the death certificate you need. If an organisation requires an original death certificate they will probably send it back, so you're just maximising how many can be sent out at the same time.

## Once you have the death certificate
### Informing banks
Once you have a death certificate take it to the banks held by the deceased. Ask lists of any direct debits and standing orders. If the executor is not comfortable handling the finances consider setting up a joint account to facilitate the handover; all inherited monies can be directed here initially but you could also set up a parallel ISA and savings account.

### Pensions
The UK government offers a "tell us once" service in my counties.

### Signing the cremation certificate
This needs to be done at least 24-hours before the cremation itself. Otherwise it won't go ahead.

### Obituary
The funeral director might offer to put an obituary in the local press.

### Eulogy
Try to start thinking about this as soon as possible or the reverend/celebrant will have to fill in the gaps. It will help to go through old photos with family.

### Misc
You may get up to two weeks compassionate leave in the UK so hopefully this will help to make the most of your time. If you're not the executor, keep in mind that they will need to be with you to discuss any details.
