# Build gcc from source

The date in the version is set here.

```bash
$ cat gcc/DATESTAMP 
20240203
```

See [Docker](https://hub.docker.com/r/deanturpin/gcc) and the [Docker file](https://gitlab.com/deanturpin/gcc/-/blob/main/Dockerfile) for build instructions.

## Reporting bugs

- [Bugs](https://gcc.gnu.org/bugs/)
- [Contribute](https://gcc.gnu.org/contribute.html)
- [Mailing lists](https://gcc.gnu.org/lists.html)

