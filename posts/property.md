# Property

- Rental income – passive/residual income: make money whilst you sleep
- Capital growth – generational wealth: leverage
- Rental growth – protection and preservation of wealth, beating inflation

## Leverage in property
- Mortgage
- Net wealth: deposit x4 x 1.1

Property prices double every ten years.

- Don't time the market, time in the market is the key.
- Limiting beliefs.
- I will stop giving myself excuses
- You're the average of the five people you spend most time with

## Employee versus entrepreneur
- Reliant on people all the time
- Education, decision, action

"What you don't hate you will learn to tolerate."

- 70% main source of income
- 20% working on
- 10% what you're educating yourself

## Deal packaging
- Always deal package buy to lets, everybody wants them
- As a secondary measure, deal package the latest thing

### Sourcing fee
- Find an opportunity and introduce to investor.
- Become a magnet for investors
- LAPS sales magnifier
- Lead magnets: draw investors to you

## LAPS
### Leads

### Appointment
Needs analysis

### Proposal
- Where are you at right now
- Where do you want to get to
- What have you tried
- Here's how I can help

Put forward enough proposals and you get...

## Sales
Always reward action takers.

- Area assessment: knowing which areas to look
- Analysers: to assess deals

- Objection is not rejection: an opportunity to overcome their problem with your soltion
- I am a solutions provider
- Make it as simple as possible through systemisation: from now, everything you do ask: is it likely this task will come up again? If so, write a system

Cast iron contracts
- NDA, non circumvention
- Reservation forms
- Disclaimers
- Privacy policies
- T&Cs

## Cycle
- Investors
- Properties
- Income

30% or profits should go on marketing.

