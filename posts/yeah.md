---
title: Yeah but no but
subtitle: Categorising utterances
date: 2020-04-19
tags:
- linguistics
---

# Yeah but no but

| Negative | Uncertain | Positive | Exclamation |
| -------- | --------- | -------- | ----------- |
| | | Yeah | |
| Nah | | Yah | |
| Nope | | Yep, yup | Oops! |
| | | | Ruh-roh |
| | | | Pah! |
| | | | Pff! |
| | Huh | Uh-huh | Ha! |
| | Hmm, um | Mmm | A-ha! |
| | Er | | Ah! |
| | | OK, K | |
