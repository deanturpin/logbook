<!--
# Dean Turpin -- Senior Software Engineer
Senior software engineer with 20+ years of commercial experience. Motivated self-learner, advocate of modern C++, Linux, automation and knowledge sharing. Open to relocation.
[https://turpin.dev/](https://turpin.dev/)
-->

- __Languages__: C++23, Python, Bash, Go; OOP, Functional
- __Design__: Multithreading, Data Structures, Algorithms, low-latency
- __Build__: VS Code, QtCreator, Visual Studio, GNU Make, CMake, Ninja, Copilot
- __Source control__: Git, Subversion
- __CI/CD__: GitLab, GitHub, Docker, Google Cloud, Jenkins
- __API__: CryptoCompare, MarketStack, OpenAI, gSOAP, REST, FIX
- __UI__: Qt, JUCE, MFC
- __Unit test/profiling__: Google Benchmark, Google Test, gprof, regex
- __Networking__: Wireshark, libpcap, Asterisk, OSI (TCP/UDP)
- __CPU__: Intel, ARM, SPARC
- __OS__: Ubuntu, macOS, Alpine, Unix (Solaris), VxWorks
- __Database__: PostgreSQL, MariaDB
- __Documentation__: MkDocs, Doxygen, Mermaid, Confluence, LaTeX, pandoc
- __Audio/Video__: Logic, Audacity, SIP, PCM, XMPP, ONVIF
- __Productivity__: Agile, Jira, Slack, Mentor
- __Education__: Computer Science BSc. 2:2 from Manchester University

