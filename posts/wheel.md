---
title: Colour wheels
subtitle: Complementary colours
date: 2020-02-23
tags:
- theory
- colour
---

# Colour wheels

Check out Blender Guru's ["Understanding color"](https://youtu.be/Qj1FK8n7WgY)
to whet your appetite and install the excellent [Color Harmony
app](https://play.google.com/store/apps/details?id=pl.powsty.colorharmony)
(Android).

1. Monochromatic
1. Complementary
1. Achromatic
1. Analogous
1. Accented analogous
1. Triadic
1. Tetradic
1. Rectangle
1. Square
1. Polychromatic

## RGB colour model
Red, orange, yellow, chartreuse, green, spring, cyan, azure, blue, violet, magenta, and rose.

## References
- [Sessions color calculator](https://www.sessions.edu/color-calculator/)
- [Canva color wheel](https://www.canva.com/colors/color-wheel/)
- [wikipedia: Color scheme](https://en.wikipedia.org/wiki/Color_scheme)
- [RYB](https://en.wikipedia.org/wiki/RYB_color_model)
- [RGB](https://www.youtube.com/watch?v=uYbdx4I7STg)

