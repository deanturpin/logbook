---
title: Software dev in a large corp versus a small company
date: 2019-11-11
draft: true
---

# Software dev in a large corp versus a small company

| Feature | Small company | large corporation |
| --- | --- | --- |
| Employees | <100 | >60000 |
| Source control | git | Subversion |
| Development strategy | Feature branches, merge to master by the PO | Commit to trunk |
| CTO | Is a software developer | Who is the CTO? |
| Business development manager (demos) | Hands on every day | Used the system only when preparing for demos |
| Agile | Not strictly Agile but evolved into Kanban | Notionally Agile but reverting to micro management |
| Production | Looking after Jenkins is a job in its own right | Jenkins management absorbed by the software team |
| User documentation | Documentation is a full-time in-house role | Not full-time, manuals subcontracted |
| Time booking | None | Booked by the hour to a works order number |
| Working hours | Fixed hours | Flexible start time (2 hour window) |

## Flexitime
A flexible start time does make it easier for the individual. But at the cost of fewer core hours and maybe encourages the "lazy late starter" mentality.
