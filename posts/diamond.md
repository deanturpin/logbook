# Diamonds

Hardness is rated using the [Mohs scale](https://en.wikipedia.org/wiki/Mohs_scale_of_mineral_hardness).

| Hardness | Substance or mineral |
|---|---|
| 8-8.5 | [cubic zirconia](https://en.wikipedia.org/wiki/Cubic_zirconia)
| 9.25 | [moissanite](https://en.wikipedia.org/wiki/Moissanite)
| 10 | diamond, [carbonado](https://en.wikipedia.org/wiki/Carbonado) |

> Because of its low cost, durability, and close visual likeness to diamond, synthetic cubic zirconia has remained the most gemologically and economically important competitor for diamonds since commercial production began in 1976. Its main competitor as a synthetic gemstone is a more recently cultivated material, synthetic moissanite.

## Diamond quality factor
- https://www.gia.edu/diamond-quality-factor
- https://www.rox.co.uk/the5cs
- https://www.phillipstoner.com/diamonds-the-5-cs/

### Colour
### Clarity
### Cut
- Shallow cut -- poor brilliance
- Well cut -- superior brilliance
- Deep cut -- poor brilliance

> How well a diamond has been cut (Proportions, Polish and Symmetry) determines how well light can refract through the stone and in turn, how much it will sparkle. Cut can also refer to the shape of the diamond.

> As a general rule, the higher the cut grade, the brighter the diamond. Under fluorescent lighting, these diamonds (left to right) display high, moderate, and low brightness.
The term “cut” also can describe a fashioned diamond’s shape. Shapes other than the standard round brilliant are called fancy cuts. They’re sometimes called fancy shapes or fancies. Fancy shapes also have names of their own, based on their shapes. The best known are the marquise, princess, pear, oval, heart, and emerald cut.

### Carat weight
> Diamond weights are stated in metric carats, abbreviated “ct.” One metric carat is two-tenths (0.2) of a gram... The metric carat is divided into 100 points. A point is one hundredth of a carat. Diamonds are weighed to a thousandth (0.001) of a carat and then rounded to the nearest hundredth, or point. Fractions of a carat can mean price differences of hundreds—even thousands—of dollars, depending on diamond quality.

### Certificate
#### Kimberley Process Certification Scheme
> The Kimberley Process Certification Scheme (KPCS) is the process established in 2003 to prevent "conflict diamonds" from entering the mainstream rough diamond market by United Nations General Assembly Resolution 55/56 following recommendations in the Fowler Report. The process was set up "to ensure that diamond purchases were not financing violence by rebel movements and their allies seeking to undermine legitimate governments."

- https://en.wikipedia.org/wiki/Kimberley_Process_Certification_Scheme
- https://en.wikipedia.org/wiki/Blood_diamond

