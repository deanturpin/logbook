# How long is a nanosecond?

Find equivalent operations in seconds for your environment. See also [size](/post/size). Repeat this exercise for size rather than speed.

| Nanoseconds | Time | Seconds |
| --- | --- | --- |
| Clock cycle @1GHZ | 1 | |
| L1 CACHE hit | 2 | |
| L2 CACHE hit | 5 | |
| L3 CACHE hit | 20 | |

## References
- https://stackoverflow.com/questions/4087280/approximate-cost-to-access-various-caches-and-main-memory
- https://mechanical-sympathy.blogspot.com/2013/02/cpu-cache-flushing-fallacy.html
- https://stackoverflow.com/questions/22584961/cache-miss-penalty-on-branching
- [Grace Hopper's](https://www.youtube.com/watch?v=9eyFDBPk4Yw) description of a nanosecond: maximum limiting distance

