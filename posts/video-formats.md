---
title: Video formats
date: 2019-10-15
tags:
- video
- codec
- bitrate
---

# Video formats

See formats on [encoding.com](https://www.encoding.com/formats/).

- Video Encoding Formats
- Video Codecs
- DRM
- Adaptive Bitrate
- Supported Image Formats
- Closed Captions
- Audio Formats

## TGA
"TGA is still used extensively throughout the animation and video industry because its primary intended outputs are standard TV screens, not color printed pages."

https://en.wikipedia.org/wiki/Truevision_TGA

## YUV
"YUV is a color encoding system typically used as part of a color image pipeline. It encodes a color image or video taking human perception into account, allowing reduced bandwidth for chrominance components, thereby typically enabling transmission errors or compression artefacts to be more efficiently masked by the human perception than using a "direct" RGB-representation. Other color encodings have similar properties, and the main reason to implement or investigate properties of Y′UV would be for interfacing with analog or digital television or photographic equipment that conforms to certain Y′UV standards."

## A7S
"The Sony α7, α7R and α7S (the α is sometimes spelled out as Alpha) are three closely related full-frame mirrorless interchangeable-lens cameras."

https://en.wikipedia.org/wiki/Sony_%CE%B17

## DPX
"Digital Picture Exchange (DPX) is a common file format for digital intermediate and visual effects work and is a SMPTE standard (ST 268-1:2014). The file format is most commonly used to represent the density of each colour channel of a scanned negative film in an uncompressed "logarithmic" image where the gamma of the original camera negative is preserved as taken by a film scanner. For this reason, DPX is the worldwide-chosen format for still frames storage in most digital intermediate post-production facilities and film labs."

https://en.wikipedia.org/wiki/Digital_Picture_Exchange

## CIN
"The Cineon System was one of the first computer based digital film systems, created by Kodak in the early 1990s. It was an integrated suite of components consisting a Motion picture film scanner, a film recorder and workstation hardware with software (the Cineon Digital Film Workstation) for compositing, visual effects, image restoration and color management."

https://en.wikipedia.org/wiki/Cineon

## SGI
"Silicon Graphics Image (SGI) or the RGB file format is the native raster graphics file format for Silicon Graphics workstations.[3] The format was invented by Paul Haeberli.[3] It can be run-length encoded (RLE). Among others FFmpeg and ImageMagick support this format."

https://en.wikipedia.org/wiki/Silicon_Graphics_Image

## 4K video sources
- https://4kmedia.org/
- https://www.appsloveworld.com/sample-4k-videos/
- https://www.blackmagicdesign.com/uk/products/blackmagicpocketcinemacamera/gallery

## Software
- [VLC player](https://vlc.onl/download/)
- [Nuke](https://learn.foundry.com/nuke/content/learn_nuke.html)
