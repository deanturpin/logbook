# Acid tracks

## Drivers for TB-03

- [macOS 14](https://www.roland.com/global/support/by_product/tb-03/updates_drivers/1d6b70a7-bfc5-42f5-8cd3-787102b7a639/)


## Daft Punk - Da Funk

| 16th | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 |
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
| note   | G  | A# | G | F | D# | C# | C# | C# | D# | D# | F  | G  | G# | G  | C  | G  |
| octave | -1 | +1 |   | +1 |   | -1 | -1 |   | +1 | -1  | +1 | +1 | -1 | -1 | -1 |    |
| accent |   |     |   |    | + |  + |  + | + |    |     |    | +  |    |    |    |    |
| slide  |   |     | / |    | / |    |  / |   | /  |     | /  | /  |    | /  | /  |    |

See [YouTube](https://www.youtube.com/watch?v=W1cpim5EAqI).


```text
G down
A# up
G slide
F up
D# accent + slide
C# down + accent
C# down + accent + slide
C# accent
D# up + slide
D# down
F up + slide
G up + accent + slide
G# down
G down + slide
C down + slide
G slide
```

## Higher State of Consciousness

```text
1 G slide
2 G up
3 G slide
4 G slide

5 G
6 G
7 B
8 G

9 B
10 G slide accent
11 G 
12 B

13 G slide accent
14 G slide
15 G
16 B
```

## Examples

- [Acid jam](https://www.youtube.com/watch?v=QbEfx4e4czs)
- [Voodoo Ray](https://www.youtube.com/watch?v=ehTvbQvtTZ4)
- [Higher State of Consciousness](https://www.youtube.com/watch?v=xTbn2Xu2YEU)