# Quants

> In quant trading, we rely on mathematical models, commonly called alphas,
> which are used to predict a component or a direction of future behaviour of
> financial securities of interest. Alphas are an important element in
> producing a forecast in price or returns, and one of the key components in
> making informed trading decisions to secure a profit. Being a key source of
> competitive advantage, the accuracy of estimating the magnitude of alphas in
> a particular direction is of utmost importance for systematic traders.

> One interesting phenomenon, henceforth referred to as alpha decay, is the
> loss in predictive power of an alpha model over time. Alpha decay presents a
> serious challenge for systematic traders as it leads to poorly-informed
> trading decisions which can have a substantial financial cost.

https://www.mavensecurities.com/alpha-decay-what-does-it-look-like-and-what-does-it-mean-for-systematic-traders/

