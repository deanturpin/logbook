# Current tech stack

[Refreshed {TODAY}]

C++26,
Ubuntu 25.04,
macOS 15.3.2,
g++ 15,
clang++ 19,
clang-format,
GitLab CI,
Qt 5.15.2,
Wirehshark 4.4.1,
bash 5.2,
vim,
pjsip 2.14,
VS Code,
CMake 3.30.3
Google Cloud,
GoogleTest/Benchmark,
gprof,
Linux kernel 6.11.0,
Darwin kernel 23.4,
Copilot,
Python 3.13.0,
Docker 27.2.1,
Logic Pro 11.1.2,
rekordbox 7.0.7,
Asterisk 22.2.0,

