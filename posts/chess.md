---
title: Chess
subtitle: Tips and tricks
date: 2021-02-01
tags:
- chess
- game
- board game
---

# Chess

## Aims
- Develop towards the centre
- Castle early and castle often

## State
- When both sides have similarly active pieces it's known as an even or balanced position
- Relative/absolute pins

## Notation
| Symbol | Meaning|
| --- | --- |
| + | Check |
| ++ | Double check |
| # | Checkmate |
| 0-0 | Castles kingside |
| 0-0-0 | Castles queenside |
| ! | Good move |
| ? | Bad move |
| !! | Brilliant move |
| ?? | Serious mistake|

## Weakest pawns
- F2 and F7
- Only the kings are defenders

## Piece weighting
- 1 pawn
- 3 knight
- 3 bishop
- 5 rook
- 9 queen

## Pawn patrol
- Pawn islands
- Connected pawns
- Pawn structure

## Phases of the game
### Opening

All your opening moves should support these goals.

- Establish control of the centre: the most important part of the board
- Get your pieces onto active squares towards the centre
- Get your king to safety by castling

### Middle game
TBD

### End game
- Promote pawns and capture the opponent's king
- Pawn races

## Tactics
1. Fork
1. Discovered attack
1. Pin
1. Skewer
1. Deflection

## References
- https://en.wikipedia.org/wiki/Rules_of_chess
- [Oldest recorded game (modern rules)](https://en.wikipedia.org/wiki/Scachs_d%27amor)
