# Yogr

<iframe style="width: 100%; height: 2000px; border:none;" src="https://deanturpin.gitlab.io/yogr/"></iframe>

## References
- Wikipedia's [list of asana](https://en.wikipedia.org/wiki/List_of_asanas) (whence inspiration originated)
- The [Travis CI nightly](https://travis-ci.org/deanturpin/yogr) that generates this page
- Some cool old illustrations in [Joga Pradīpikā](https://en.wikipedia.org/wiki/Joga_Prad%C4%ABpik%C4%81)
