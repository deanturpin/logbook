---
title: Mobile phone repair tips
date: 2021-12-27
tags:
- mobile
- phone
- repair
---

# Mobile phone repair tips

Tips by SR, renowned horologist and raconteur.

- Website. 'Ifixit'. Very good. https://store.ifixit.co.uk/
- Tools. Good set of small torx drivers.
- A tip. When taking phones apart stick the screws into a slab of blutak in the
  appropriate pattern.
- Get decent spudgers. You'll find that when you buy screens, they will often
  come with piles of tools and suckers for pulling the screens off a phone.
- Tip. To soften screen adhesive, use a hairdryer with the flat nozel on. Use
  medium heat and aim along the edges
- Spudgers is a flat implement like a guitar pick to prize a screen off. Have
  loads and as you work round the screen, leave them underneath so the screen
can't drop back down
- For a broken screen stick wide sellotape or similar on the whole of the
  screen. Protects you from shards and allows the puller to adhere to the
screen
- Removing batteries. iPhones: pull the release tape carefully to release the
  battery from the case. Always discharge the phone before you start.
- [On ESD] It's a throwback to unprotected CMOS. Just keep yourself and the
  phone at the same potential. It's called holding it.
- Use a wood tray to work on with a sheet of A3 paper so you can see the bits
  easier. Tray makes sure bits don't fall on the floor.
- Get yourself a head magnifier glasses to help you see what you are doing.
- And most important... Good direct lighting.
- It doesn't hurt to take a photo at various stages so you know where bits came
  from. After a while though, you'll know your way round.
- If you don't want to use a magnifier, you can stack reading glasses or get
  3.5 dioptre glasses. You can work with your nose in the works without it
getting blurry
- Remove the shield to expose the battery. First make sure all connectors are
  sound. Then probe the battery to check its charge. Leave it for 10mins then
reconnect the battery and try charging again or plug into the computer again.
Run Samsung Kyes or whatever it is called now. Of course if you had a spare
battery with a bit of charge you could pop that in and try booting up.
- SHINE MAGNETIC PICK UP TOOL: This is the one I have. Works well and comes
  with a set of three strength lenses. They are plastic though, so can get
scratched
- When putting screws back pop blue tack on the end of the driver. The screw
  will stick to this hence making it easier to get in the right place and screw
it in.
- Also get a couple of small storage boxes for bits.
- And get a set of good tweezers, and get a telescopic magnet for picking up
  dropped bits on the floor.

# Tools
- SHINE MAGNETIC PICK UP TOOL 10 LBS TELESCOPIC HIGH VISIBLE EXTENDING MAGNET CLIP ORANGE https://www.amazon.co.uk/dp/B01MCZK74A/ref=cm_sw_r_apan_glt_fabc_4DRQC7K1KC5FTBMQS3H9?_encoding=UTF8&psc=1
- Carson Unisex's CP-60 Pro Series MagniVisor Deluxe Headband LED Lighted Magnifier with 4 Different Lenses, Black, (1.5X, 2X, 2.5X, 3X) https://www.amazon.co.uk/dp/B007CDJKM2/ref=cm_sw_r_apan_glt_fabc_5ABT6BEWA2B5ZBCT1TGS

