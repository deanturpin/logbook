---
title: Dog child
subtitle: I wonder if having a child is like owning a dog.
date: 2019-06-18
tags:
- story
- literature
---

# Dog/child

At some oftentimes unexpected juncture you’re handed a helpless bundle of joy.
And into him you pour all your waking hours, love and encouragement. And you
develop a unique bond that can never be broken. Until he grows much larger and
stronger than you, and financially independent and then removes his own lead
and moves into a new kennel with another dog who he’d rather spend his time
with.

He calls you occasionally and then when you can no longer look after yourself
he puts you in a home with the other old parents that you pay for out of your
savings. And then you die and he gets all your stuff.

