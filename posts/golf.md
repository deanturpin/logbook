---
title: Minimum clubs to get around a golf course
date: 2020-04-20
tags:
- golf
- sport
---

# Minimum clubs to get around a golf course

> 3 wood, 3, 5, 7, 8, sand wedge and putter should do. Most golf clubs hire
> sets out. Good luck and go to the range before you play.
-- Steve
