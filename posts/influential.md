# Influential technology

Technologies that have had a positive, lasting impact on me as a developer.

- __2025__: ghostty
- __2024__: gcc/clang from source, C++ modules, `constexpr` cmath, Dev Containers, Codespaces, ChatGPT voice, -Wnrvo
- __2023__: Google Benchmark, Haskell, ChatGPT, AVX, Copilot, OpenSCAD
- __2022__: SIP, GitBook, Compiler Explorer (on prem), GitLab (on prem), GitLab service desk, git worktrees, FIX, `constexpr` (for UB), GoogleTest, VS Code web IDE
- __2021__: `std::format`, TMP, C++20 ranges and views (lazy evaluation)
- __2020__: Blender, Media servers, ZeroMQ, Tracy profiler, Raspberry Pi
- __2019__: Cloud computing, R/notebooks, GitLab, Hugo
- __2018__: Docker
- __2017__: Cryptocurrency, blockchain
- __2016__: C++1z
- __2015__: git, GitHub

