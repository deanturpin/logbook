# Tuning Linux
See [Tuning for Linux platforms](https://docs.oracle.com/cd/E19159-01/819-3681/abeji/index.html).

## Machine info
```bash
lscpu
# Cache line size
getconf LEVEL1_DCACHE_LINESIZE
```

