---
title: Cyber-security resources
date: 2020-05-10
tags:
- revision
- cyber
- security
- hacking
---

# Cyber-security resources
## Search tools
- Use different search engines
- Banner grab
- httrack
- inspy
- metagoofil
- intitle:"index of"
- [DNS poison/spoof](https://en.wikipedia.org/wiki/DNS_spoofing)

## Infosec websites
- https://www.hackthissite.org/pages/index/index.php -- website hacking training
- https://www.shodan.io/ - the IoT search engine
- https://searchdns.netcraft.com - what's that site running?
- https://www.exploit-db.com/searchsploit
- https://pipl.com/
- https://haveibeenpwned.com/
- https://wigle.net/
- https://www.peekyou.com/
- https://www.spokeo.com/
- https://radaris.com/
- https://piknu.com/

## Considerations
- Language vulnerabilities
- Common cyber attacks
- Tor
- Crypto attacks - frequency analysis
- Data encryption standard
- AES advanced encryption standard
- Substitution permutation network
- Kali Linux
- Vulnerability research with reverse engineering, penetration testing and ethical hacking
- Low level Linux programming and/or comprehensive knowledge in operating system security and associated network/platform design, hardening and deployment.
- Cyber-security and certification such as such as CISSP, SANS GIAC, Security+, Network+, Linux+, MCSE, CCNA or SSCP.
- Capture and decomposition through to integration, acceptance and customer sign off and associated toolsets.

## References
- https://en.wikipedia.org/wiki/Great_Firewall
- https://en.wikipedia.org/wiki/WeChat
