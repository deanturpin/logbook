# EV in Brighton

We have a home charger. The street chargers _sometimes_ include free parking but you have to check you don't need a permit too (the signs are ambiguous); and there are some inside car parks where you have to pay for both!

Around town most of the street chargers required the [Blink app](https://play.google.com/store/apps/details?id=com.ov.electricblue). There are a handful of [rapid chargers](https://electricbrighton.com/charging/rapid), but if you're coming down from London you will pass a couple of superchargers at Pease Pottage (Gatwick).

[Zap Map](https://play.google.com/store/apps/details?id=com.zapmap.zapmap) is the standard app but there's also [Electric Brighton](https://electricbrighton.com/).

| Description  | Power | Current | mph  | Voltage |
|---           |---    |---      |---   |---      |
| Lamppost     | 3.7kW | 16A     | 14   | 240V    |
| Home         | 7.4kW | 32A     | 28   | 240V    |
| Supercharger | 250kW | 520A    | 648  | 480V    |

See [Tesla Supercharger](https://en.wikipedia.org/wiki/Tesla_Supercharger) and [evbox.com](https://evbox.com/uk-en/electric-cars/tesla/tesla-model-y).

## Blink chargers

Outline of the process below. However, it is a bit error prone and a couple of times I've just given up, but the free parking whilst charging is great. They do get ICEd quite often -- perhaps because they're relatively discrete -- which doesn't happen on the very clearly-marked green EV bays in town.

- Register online and add a payment card
- Install the app
- Uses the standard Type II connector
- Find a charger and "check in" to the correct charger (check the ID above the socket)
- Wait for it to churn with a blank screen for a bit...
- You will then get charged £50 (so you need at least that much in the bank)
- When you disconnect the car you will be refunded the difference
- Works out about 61p per kWh

## PodPoint

Much slicker than Blink but of course you need another app. There's a couple at the Goldstone retail park and the Amex stadium.

## Connectors

- [CCS2 "Combo 2"](https://en.wikipedia.org/wiki/Combined_Charging_System) (Combined Charging Connector) -- what a design this is!
- IEC Type II -- the normal one you'll see everywhere
- [CHAdeMO](https://en.wikipedia.org/wiki/CHAdeMO) -- weird one that you need an adapter for
