---
title: Blockchain
date: 2019-09-15
tags:
- revision
- blockchain
- crypto
---

# Blockchain
The blockchain is an irrefutable/immutable database. Rather than using a single point of truth like the traditional centralised model, in the blockchain there are multiple copies of the transactions and the protocol ensure that they are synchronised and makes it difficult to exploit.
