# Hallmarks

## The standard mark
Describes the fineness of the material, which is purity measured in parts per 1000.

| Purity | Gold | Silver |
| --- | --- | --- |
| 375 | X | |
| 585 | X | |
| 750 | X | |
| 800 | | X |
| 916 | X | |
| 925 | | X |
| 958 | | X |
| 990 | X | |
| 999 | X | X |

## References
- https://en.wikipedia.org/wiki/Hallmark
