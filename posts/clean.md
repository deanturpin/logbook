# Cleaning

I have managed to exist for five decades without getting too excited about cleaning things, and then I bought a new car and I've gone all in!

## Shoes

[Jason Markk](https://www.jasonmarkk.com/) products for foaming cleaner, brush and repellent. Use a microfibre cloth to dry, such as VERSATOWEL™ Ultra Plush 400GSM Microfibre.

## Watches and jewellery

You cannot go wrong with an [ultrasonic cleaner](https://www.amazon.co.uk/DK-SONIC-Household-Ultrasonic-Eyeglasses/dp/B08S6V52MV/).

## Car

### Tools

- Meguiars RG203 5 Gallon Yellow Bucket x 2
- Meguiars X3003 Professional Grit Guard x 2
- Halfords lance
- Kärcher K2 Compact
- Waterproof gloves

### Cloths

- E.T™ Ultra Absorbent Washable Exterior Wash Mitt/Pad
- RESERVOIR™ Ultra Absorbent 900GSM Microfibre Drying Towel - Grey
- VERSATOWEL™ Ultra Plush 400GSM Microfibre Cloths - Orange (for inside)
- VERSATOWEL™ Ultra Plush 400GSM Microfibre Cloths - Blue (for outside)
- The Rag Company Waffle Weave glass towels
- P&S Rags to Riches Microfiber Detergent

### Wheels

- Meguiars Ultimate Wheel Cleaner
- Wheel brush

### Paint

- Infinity Wax Citrus Pre-wash Concentrate
- Infinity Wax Powerfoam Snowfoam
- Infinity Wax Pure Shampoo

### Interior

- Car Gods interior detailer

---

### Spot cleaning

For quick cleaning of light dirt, road detritus and bird scat. Just removing the worst of it before it becomes caked on prior to the proper weekend wash.

See [YouTube](https://www.youtube.com/watch?v=5Y_8Z6KkOgQ).

Also see [Jeremiah Jones](https://www.youtube.com/watch?v=G_9_0ElVChU).

- Premixed ONR in a spray bottle at 1:128
- Spray the offending area with force to try to remove without contact
- Leave to dwell for a couple of minutes
- Drag a fresh microfibre towel across the area... no rubbing!

## My previous process (before ceramic)

- Rinse
- Snow foam
- Rinse
- Spray on ONR mix
- Wipe down with microfibre (single bucket and lots of cloths)
- Dry with the wrong towel (bought a wax buff not a drying towel)

### Two buckets method

Standard two bucket method with grit guards.

### One bucket method

One bucket with shampoo and lots of cloths.

### Brands

- <https://www.infinitywax.com/>

### Cloths

- https://www.cleanandshiny.co.uk/collections/the-rag-company

### Vacuum

- [Ridgid](https://www.amazon.co.uk/RIDGID-VAC3000-Portable-3-Gallon-Horsepower/)
- [Speedex](https://www.speedexwireless.com/products/speedex-wireless-vacuum-cleaner)
- [Fanttik](https://www.amazon.co.uk/Fanttik-V8-Mate-Cordless-Ultra-Lightweight/dp/B0B9447P5P)

### To investigate

- filtered water
- drying aid
- autoglym atc for door jams
- infinity wax 10% off with code EPIC
- [Traffic film remover](https://www.jennychem.com/collections/traffic-film-removers/products/tfr-special-non-caustic-wax)
- https://fb.watch/nJrU1kytTY/
- [Wheel dips](https://www.dipyourcar.com/collections/wheel-kits-by-color/products/shadow-black-hyperdip-wheel-kit?variant=5630996356&fbclid=IwAR0DZK-yhcBSsW94hoNdrYYRAo3Xi7-6gzpEJ7ZL3L86ukv7lbsGmXCHa9Y)
- [Sam's detailing towel](https://www.samsdetailing.co.uk/products/drying-towel?fbclid=IwAR2CQ86OernaGAjsXlkomcS16ggyHdrwgBlv4IIy5ZX1TeJxv462dQY76ro_aem_Aaeazxkf5BmqUkzHggHV7PSiB_g9GRrLvPv_sJivaL6472ipLLDjAUeYNhzxcxyxDokM3KWZMhglPNZG9diwbkRy&utm_source=facebook&utm_medium=paid&campaign_id=23857344821250629&ad_id=23857344821300629)
