# Courses and books (and things to watch)

## _Book_ books (made of actual paper)

- [Bad Science](https://www.amazon.co.uk/Bad-Science-Ben-Goldacre/dp/000728487X) -- Ben Goldacre
- [The Art of Writing Efficient Programs](https://www.amazon.co.uk/Art-Writing-Efficient-Programs-optimizations/dp/1800208111/) -- Fedor G. Pikus
- First Steps in Music Theory (grades 1 to 5) -- Eric Taylor
- [The Climate Book -- Greta Thunberg](https://www.amazon.co.uk/gp/product/0241547474/) 
- [Building Low Latency Applications with C++: Develop a complete low latency trading ecosystem from scratch using modern C++ -- Sourav Ghosh](https://www.amazon.co.uk/gp/product/1837639353)
- C++ Concurrency in Action -- Anthony Williams 
- Learn You a Haskell for Great Good!: A Beginner's Guide: A Beginner's Guide to Haskell
- Practical Watch Repairing -- Donald de Carlo
- C++ Templates: The Complete Guide -- David Vandevoorde
- C++ High Performance: Master the art of optimizing the functioning of your C++ code, 2nd Edition -- Bjorn Andrist, Viktor Sehr, Ben Garney
- Learn Chess Tactics -- John Nunn
- Amo, Amas, Amat ... and All That -- Harry Mount
- Seriously Curious: 109 facts and figures to turn your world upside down Paperback -- Tom Standage

## Leanpub books

Get free updates so these are potentially bang up-to-date.
- [A Complete Guide to Standard C++ Algorithms](https://leanpub.com/cpp-algorithms-guide)
- [C++ Best Practices](https://leanpub.com/cppbestpractices)
- [Ace the Trading Systems Developer Interview (C++ Edition)](https://leanpub.com/tradingsystemsdeveloperinterviews)

## YouTubes that have changed my life

- [Sorting Algorithms: Speed Is Found In The Minds of People - Andrei Alexandrescu - CppCon 2019](https://www.youtube.com/watch?v=FJJTYQYB1JQ)

## Audiobooks

- The Art of Unit Testing: With Examples in C#
- Pimsleur Norwegian Level 1 (lessons 1-5)
- The Economics Book: big ideas simply explained -- DK
- The Selfish Gene -- Richard Dawkins
- Viruses, Plagues, and History: Past, Present, and Future -- Michael B. A. Oldstone
- Have You Eaten Grandma? -- Gyles Brandreth
- Raised Bed Gardening -- Jason Johns
- Sapiens -- Yuval Noah Harari
- Understanding Complexity -- Scott E. Page, The Great Courses
- How Colors Affect You: What Science Reveals -- William Lidwell, The Great Courses
- Vietnamese Phase 1, Unit 06-10: Learn to Speak and Understand Vietnamese with Pimsleur Language Programs -- Pimsleur
- Vietnamese Phase 1, Unit 01-05: Learn to Speak and Understand Vietnamese with Pimsleur Language Programs -- Pimsleur
- Mentors: How to Help and Be Helped -- Russell Brand
- Activate Your Vagus Nerve -- Dr. Navaz Habib
- Moneyland: Why Thieves and Crooks Now Rule the World and How to Take It Back -- Oliver Bullough
- How to Be Right... in a world gone wrong -- James O'Brien
- Whoops! Why Everyone Owes Everyone and No One Can Pay -- John Lanchester
- Naked Money - A Revealing Look at What It Is and Why It Matters -- Charles Wheelan
- Economics: The User's Guide: (A Pelican Book) -- Ha-Joon Chang
- Naked Statistics: Stripping the Dread from the Data -- Charles Wheelan
- The Science of Yoga: The Risks and Rewards -- William J Broad
- Why We Sleep: The New Science of Sleep and Dreams -- Matthew Walker
- Mythos -- Stephen Fry
- The Subtle Art of Not Giving a F\*ck: A Counterintuitive Approach to Living a Good Life -- Mark Manson
- I, Partridge: We Need to Talk About Alan -- Alan Partridge
- Flash Boys -- Michael Lewis
- Forex: Strategies: Best Forex Trading Strategies for High Profit and Reduced Risk (Volume 2) -- Matthew Maybury,
- Cryptocurrency: 3 in 1: Blockchain, Bitcoin, Ethereum -- Mark Smith
- The Big Short: Inside the Doomsday Machine -- Michael Lewis
- The Humans -- Matt Haig

## Pluralsight

- Presenting to the Boss(es)
- Lean In: Navigating Gender Bias at Work
- Play by Play: Solving Workplace Grumpiness
- Agile Estimation
- Agile for One
- Data Science with R
- Beginning Data Visualization with R
- Exploratory Data Analysis with R
- Using Advanced Data Structures in Modern Applications
- Algorithms and Data Structures - Part 2
- Algorithms and Data Structures - Part 1
- Digital Audio Fundamentals
- An Introduction to Algorithmics
- Ethical Hacking: Cryptography
- Go Fundamentals
- SOLID Principles of Object Oriented Design
- C++ Unit Testing Fundamentals Using Catch
- C++ Advanced Topics
- Introduction to the Boost C++ Libraries
- AWS Developer: Getting Started
- Ethical Hacking: Vulnerability Analysis
- Hardware for CompTIA A+ (220-901)
- Understanding and Applying Financial Risk Modeling Techniques
- Introduction to Browser Security Headers
- Ethical Hacking: Hacking Mobile Platforms
- Ethical Hacking: Hacking Wireless Networks
- Ethical Hacking: Cloud Computing
- Getting Started with Analyzing Network Traffic Using Wireshark
- Troubleshooting with Wireshark: Analyzing and Decrypting TLS Traffic in Wireshark
- SSCP®: Network and Communications Security
- Ethical Hacking: System Hacking
- Ethical Hacking: Sniffing
- Ethical Hacking: Scanning Networks
- Ethical Hacking: Enumeration
- Ethical Hacking: Social Engineering
- Ethical Hacking: Reconnaissance/Footprinting
- Ethical Hacking: Hacking the Internet of Things (IoT)
- SSCP®: Cryptography
- Cryptography Fundamentals for Developers and Security Professionals
- Docker Deep Dive
- Getting Started with Docker
- Product Owner Fundamentals - Product Ownership from the Trenches
- Scrum Master Fundamentals - Growing Yourself and Your Team
- Big Scrum
- Product Owner Fundamentals - Plotting the Product Owner's Career Path
- Scrum Master Fundamentals - Becoming a Great Scrum Master
- Agile Team Practices with Scrum
- Beautiful C++: STL Algorithms
- Practical C++14 and C++17 Features
- C++ Fundamentals - Part 2

