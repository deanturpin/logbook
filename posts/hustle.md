# Software developer side hustles to keep your skills fresh

## COVID-19

Analyse Covid-19 and save the World. See [Kaggle's covid
challenges](https://www.kaggle.com/covid19) and fetch daily data from [Pomber
on GitHub](https://github.com/pomber/covid19) (e.g., [these Bokeh
graphs](https://www.schleising.net/)).

"Make good use of all that commuting money you're not spending," said
[HarryB](https://90percentofeverything.com/about/index.html).

Run your projects in a [Google Cloud instance](https://cloud.google.com/) or
spend it on a
[VPN](https://www.expressrefer.com/refer-friend?referrer_id=51231154&utm_campaign=referrals&utm_medium=copy_link&utm_source=referral_dashboard).

## Finance

The finance industry offers a great source of data to wrangle and also the
promise of making some of it (money I mean). See [Crypto
Compare](https://www.cryptocompare.com/).

## ascii2cpu

Write a tool to stress your cores and make a pattern. Perhaps a video?

## Algorithms
Write an algorithm visualisation tool. (And implement the algorithms too.)

## Learn a 3D modelling tool

It's just really useful for all sorts: building a house, moving office, 3D printing...

You can get stuff done with [SketchUp](https://www.sketchup.com/) but if you want to go in then learn [Blender](https://www.blender.org/). It's a really steep learning curve but one day you'll have to build something and it will be invaluable. See a [short film made with Blender](https://www.youtube.com/watch?v=DVXEYksoE6c).

Alternatively script it quickly with [OpenSCAD](https://openscad.org/) and get back to the programming.

## It's complicated...

Listen to ["Understanding Complexity" by Scott E.  Page](https://www.audible.co.uk/pd/Understanding-Complexity-Audiobook/1629976849) to whet your appetite. See various [complexity-based
projects](/post/complexity).

## Create your own scripting language
Writing a compiler is serious but you can write a script parser in C++ that
does _something_ very quickly. Gets you thinking about some sticky edge cases
very quickly.

Evaluate and return a result for these expressions:
- 1 + 2
- 1 + -2
- 3 / 5
- 1,000 x 5

## Big data
It's not going away and it's only getting bigger. See a [consolidated list of sources](post/big).

## Write an open letter to your younger developer yourself
"If I'd known then all the things I know now..." -- see
[mine](https://turpin.one/posts/letter.html).

## Audio
- Tone generator (WAV processing is great to get back to basics)
- Write your own DFT ([Discrete Fourier Transform](https://en.wikipedia.org/wiki/Discrete_Fourier_transform))
- Generative music maker
- Speech analysis

## GPUs
How do you run code on your GPU?

- https://www.geeksforgeeks.org/running-python-script-on-gpu
- https://developer.nvidia.com/how-to-cuda-c-cpp

## Advert of code
A coding problem to solve [every day of December](https://adventofcode.com/).

## Document everything you know
It's a mammoth undertaking and you'll never finish but you [have to start](https://turpin.dev/).

## Teach
Even if it's showing somebody how to use Excel (which can be impenetrable if you're not from a techie industry).

## Zip it
A multi-threaded zip tool.

## Godbolt CLI
Like [Gobolt compiler explore](https://godbolt.org/) but available on the command line. Or perhaps a vi plugin?

## See also
- [Codementor](https://www.codementor.io/@npostolovski/40-side-project-ideas-for-software-engineers-g8xckyxef)
- [Skills that will earn you $125K](https://www.businessinsider.com/highest-paying-programming-languages-stack-overflow-developer-survey-2020-5)

