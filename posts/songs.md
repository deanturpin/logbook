# Dean Turpin songs

## Infectious diseases

```
Dbsus2 C#m Dbsus2 C#m
B A Am E
```

## Over shoes

```
D Dm A F
Bm E A E
```

## Isolation station

```
Verse
C Em Am G
C Em Am G

Chorus
F G Em E7 Am
G# G# G13 G13
```

## Trousers

```
Bb6 Em Bb6 Em
```

## Lemon drizzle

```
C C C C
G6 G6 G6 G6
Am Am
E E E E
```

## Hopes

```
# Verse
C C C C
Am7 Am7 Am7 A7m
Em Em F F
Dm Dm

C C C C
Am7 Am7 Am7 A7m
Em Em F F
Dm Dm Dm Dm

# Chorus
E7 E7 Am Am
Dm Dm G G
```

## Once more with skanking

```
G F C C
G F C C7
E E7 Am Am
G# G# G G
```

## Riverine

```
Cmaj7 A7sus2 Cmaj7 A7sus2
Em Em G G
```

## Hallo 5/4

```
Am Dm C Baug E
```

## Goats

```
Verse
E E E F#m9
E E E F#m9(11)
E E E F#m9
E E E F#m9

Chorus
G7 G7 C C
B7 B7 B7 B7
```

## Driving licence

Asus2 is Asus2#11.
```
E E E F#m9
G6 G6 Cmaj7 Cmaj7
B7 B7
```

## Untitled

```
Eadd9 Am6 Baug Em(add9)
```

## Pag

```
Verse
Am Am C C F F E E
C C Am Am G G G F
Am Am C C F F E E
C C Am Am G F E E E7 E7

Chorus
Dm Dm Am Am E E Am Am
Fmaj7 Fmaj7 C C E E E E
```

## Goodnight

Goodnight good night good night

I'm here to hold you tight

We'll dream out the day

And sleep out the night

GN GN GN

```
A E A A
A E A A
D Dm E F#m F#m
A E A A
```

## You know, you know?

```
Verse
A Amaj7 A7 D
Dm A E A

Chorus
C#m C#m A A
C#m7 C#m7 E E

Interlude
F#m F#m F#m F#m
Bm Bm Bm Bm
D D Dm Dm
A A E E
```

## Pink shorts and red wine

```
Dmaj7 A E A
Dmaj7 A F#m E
Dmaj7 A E
```

## Origin Unbeknownst

```
Chorus
Fmaj7 Fmaj7 Em Em
Fmaj7 Fmaj7 G Baug
Fmaj7 Fmaj7 Em Em
Fmaj7 Fmaj7 Baug E7 E7
```

## Slow song

Picked.
```
Em C Am Baug
```

## A choice of fabrics
```
E D C B
```

## Careful now (down with this sort of thing)

```
E7 Fmaj7 F#m7 G6
G#dim A7 D7 G
```

## Ahoy! Ahoy! I'm captain of the ship... (working title)

|string|Cadd9|G13|
|---|---|---|
|E| |0|
|B|3|3|
|G|0|0|
|D|2|3|
|A|3| |
|E| |3|

```
Verse
Cadd9 G13 Cadd9 G13
Dm C Dm G7
```

## Wintersun

```
Verse
C B Am Am
F G C C
C E7 Am Am7
F F G7 G7

Interlude
Am
```

## Made in Black

|string|Bm|C#|
|---|---|---|
|E| | |
|B|1|6|
|G|4|6|
|D|4|6|
|A|2|4|
|E| | |

```
Verse/chorus
Bm C# F Em G
D A F F7
```

## Gayatri Mantra (version)

See [Wikipedia](https://en.wikipedia.org/wiki/Gayatri_Mantra).

```
D D D A A A
Em Em Em A A A
```

## Zivjeli

A Croatian drinking song.

```
Cm Cm G Cm
Cm Cm G Cm
Fm Cm G Cm
Fm Cm D G G7
```

## References

- [Songs with interesting major/minor key changes](/posts/songs2/)
- [Oolimo guitar chord analyser](https://www.oolimo.com/)
