---
title: The art of tea
date: 2019-11-02
tags:
- tea
- recipes
---

# The art of tea

# Hibiscus tea "Lilin"
- 65g hibiscus
- 10g dried lemons
- Lemongrass
- Chamomile
- Vanilla
- Rose petals

# Hibiscus tea 2
- 65g hibiscus
- 10g dried lemons
- Verbena

# Potential ingredients
- Orange
- Hibiscus
- Lime
- Coconut
- Turmeric
- Rose
- Chamomile
- Barberry
- Lemon
- Kefir lime
- Cacao nibs
- Apple

# Potential combos
- Caramel
- Ginger cake
- Oat, rose, choc - no!
- Almond, milk, raspberry
- Coconut, turmeric, barberry, lemon
