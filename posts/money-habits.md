# Money habits

1. Pay yourself first
1. Don't get confortable with bad debt, save 10% before you pay the bills
1. You need a 3-6 months stockpile (buffer)
1. Know your income and expenses
1. Reigh in expensive hobbies (spending on skills, however, cannot be take away from you)
1. Don't focus on saving only (saving has a cap, making money does not)
1. Don't pay taxes you don't need to (ISAs etc)
1. Don't wait too long to invest (don't keep additional money -- above your buffer -- in a bank account)
1. Care about your finances!

See [ACCOUNTANT EXPLAINS: Money Habits Keeping You Poor](https://www.youtube.com/watch?v=Q0uXGQu55GM).

