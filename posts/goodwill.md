# Good Will Coin

So, how about a "good deed" token? You can't buy your way in like those Bitcoin slags, the only way to earn credit is by doing a good deed. Once you've done a good deed you can then pay somebody else for a task. Maybe you could even gift tokens?

- A good deed should take no longer than an hour
- Earn tokens by doing a good deed: you cannot buy your way in
- IOU coin
- Token awarded by community or completing contract between two parties

## Examples of a good deed

Just to get a feel for the scale of the task.

- Take somebody to the supermarket 
- Give somebody a lift into town 
- Hang a picture
- Mow the lawn

