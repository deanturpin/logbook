# C++ experience and side-projects

Commercially I develop using C++17, so I feel it is important to explore the latest C++ features in my personal projects. I track the latest Ubuntu Docker image in my CI pipes and I publish a nightly Docker build of gcc HEAD built from source.

Projects cover subjects such as PCAP processing, audio analysis, trading strategy backtesting and OpenAI. I believe a solid foundation in ubiquitous languages such as Bash and Python is essential in CI and cloud computing; I have an interest in a more functional approach offered by compile-time programming; and I consider profiling and benchmarking an integral and continuous part of the development process for low-latency applications.

[https://turpin.dev/](https://turpin.dev/)

