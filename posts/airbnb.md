# Airbnb changeover

## Doors
- Open all doors to air the house

## Laundry
- Strip down all beds: duvets, sheets, mattress protectors, pillow protectors
- Gather all bath/hand towels (including downstairs toilet), tea towels, bath mat
- Hang bedspreads over the doors
- Mattress protectors don't go in the dryer
- Bubble wash on anything mucky

## Kitchen
- Check all cupboards and fridge
- Check glasses in the cupboard are actually clean

## Towels
- Fold towels, two per guest

## Bathroom
- Taps
- Windows
- Window ledge
- Radiator
- Floor tiles

## Kitchen
- Clean fridge
- Wipe cupboard doors
- Check dishwasher, put on if dirty, otherwise put the dishes away

## Bedroom 1
- Clean patio glass
- Wipe wardrobe mirrors

## Bins
- Empty bins and add bin liner
- Wipe inside of bin

## Replenish
- Toilet roll
- Hand soaps in two bathrooms
- Washing up liquid in kitchen
- Refill soap

## Kitchen
- Wipe cutlery
- Clean top of stove
- Check microwave

## Patio
- Clean tables
- Sweep

## Lounge
- Clean chairs and bench
- Make up table
- Take out side tables and wipe

## Cleaning
- Hoovering, all floors, under beds, tops of skirting
- Clean all bathroom glass and handrail/balustrade with microfibre cloth
- Mop all floors

## Driveway
- Sweep
- Remove weeds

## Guest snacks
- Beer (4-pack)
- Prosecco or white wine (1 bottle)
- Crisps (2 packs)
- Chocolate biscuits (2 packs)
- Organic milk (1 pint)
- Orange juice (1 litre)

## Off peak deep clean
- Dish washer filter
- Grout bleach
