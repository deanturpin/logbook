# Links

See my [Wikipedia contributions](http://tools.wmflabs.org/guc/?user=Dean+Turbo)
and techie tidbits (plus cat pics) on [Twitter](https://twitter.com/deanturbo).

# xy
ASCII line plotter, takes a CSV on the command line (or stdin) and emits a graph -- [repo](https://gitlab.com/deanturpin/xy)/[example website](https://covid.germs.dev)

# tracehost
Network mapper using bash and GraphViz -- [repo](https://gitlab.com/deanturpin/tracehost)/[website](https://deanturpin.gitlab.io/tracehost)

# Have A Nice Day Trader
Trading strategy backtester -- [repo](https://gitlab.com/deanturpin/handt)/[website](https://deanturpin.gitlab.io/handt)

# tradeR
Currency analysis in R -- [repo](https://gitlab.com/deanturpin/tradeR)/[website](https://deanturpin.gitlab.io/tradeR)

# BIGO
Exploring Big 'O' programatically -- [website](https://gitlab.com/deanturpin/bigo)

# Spectrum
Command line audio analysis -- [repo](https://gitlab.com/deanturpin/spectrum-analyser)

# DFT
Discrete Fourier transforms -- [repo](https://gitlab.com/deanturpin/dft)

