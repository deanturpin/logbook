# Complex systems

## Complexity projects
- Game of Life
- Social distancing
- Crowd simulation
- Building evacuation
- SIR (epidemiology)

## A complex system has
- __A__daptation and learning
- __C__onnectedness
- __I__nterdependence
- __D__iversity

## Wolfram behaviours
- Stables
- Periodic
- Chaotic
- Complex (high info content)

## Misc
- Exploration versus exploitation
- Highest peak
- On a dancing landscape you can never stop exploring
- Black swan

## Emergence
- Complexity is an emergent property
- Stasis encourages exploration and vice versa
- Slime mold breaking symmetry
- Bottom-up and top-down emergence
- Power law distribution
- Long tail
- Weekly emerging, strongly emerging (never figure out)
- Preferential attachment model

## Agent-based models
- Fires in crowded buildings
- Epidemics
- Netlogo
- Good science and agent based models must simplify (abstract)

## Feedback and externality
Feedback is affecting the same action.

- Positive feedback yields tipping points
- Negative feedback yields stability
- Feedback and externality produce path dependence

## The tail wags the dog
- With complex systems the mean doesn't matter: it's the tails
- Segregation is emergent
- Tipping is one way only, segregation is stable

## Social randomness: coin flipping
Humans underestimate how long runs of the same side can occur randomly. If a
person is playing the part of the coin and selects three heads, the fourth flip
won't be a 50/50 chance of heads or tails -- whereas the result of a truly
random process would not be affected by the previous flips.

- Coin flip return times
- Don't allow system to build themselves without adding extra checks to limit large events
- Central limit theorem

