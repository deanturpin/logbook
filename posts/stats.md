# What does it all mean?

## Means
- Mean: the average value of a data set
- Median: the middle of a data set, the number that splits a data set in two;
requires sorting; median of an even number of elements is the mean of the
middle two elements
- Mode: value in a data set that occurs most often; if a data set has unique numbers there is no mode; if there are equal numbers of some elements there are multiple modes

## Random variables
- Discrete: finite numbers of values
- Continuous: infinite number of values

## Probability density functions
```
P(|Y - 2| < .1) is equivalent to P(1.9 < Y < 2.1)
```

- Probability is the area under the PDF
- Integral of `f(x)d` from zero to infinity is 1

