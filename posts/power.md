# Power

And batteries and chargers and things.

## Nothing Phone (2)

- Li-Ion 4700mAh, non-removable
- 45W wired, PD3.0, PPS, QC4, 100% in 55 min (advertised)
- 15W wireless, 100% in 130 min (advertised)
- 5W reverse wireless

### Charging

Measured using the app CPU X.

- Discharging: 4312mV, -200mA
- Charging: 4182mV +8738mA

## Sharge 140W PD 3.1 (wall charger)

### Supported Protocols

- PD 3.1/3.0
- QC 2.0/3.0/4+
- FCP
- SCP
- Apple 2.4
- AFCPE
- PPS

### Output

- USB-C1: 5V/9V/12V/15V⎓3A, 20V/28V⎓5A (140W MAX)
- USB-C2: 5V/9V/12V/15V⎓3A, 20V⎓5A (100W MAX)
- USB-A: 5V⎓4.5A, 4.5V⎓5A, 9V⎓3A, 12V⎓2.5A, 20V⎓1.5A (30W MAX)
- USB-C1+USB-C2: 100W+35W
- USB-C1+USB-A: 100W+30W
- USB-C2+USB-A: 5V⎓4A（20W MAX)
- USB-C1+USB-C2+USB-A: 100W+20W (5V⎓4A)

## Apple MacBook Pro (wall charger)

- 87W USB-C Power Adapter
- 20.2V⎓4.3A (USB PD) or 9V⎓3A (USB PD) or 5.2V⎓2.4A

## Samsung Galaxy S21 Ultra (wall charger)

- "Super Fast Charging"
- TRAVEL ADAPTER MODEL: EP-TA800 INPUT: 100-240V ~ 50-60Hz 0.7A OUTPUT: (PDO) 5.0V⎓3.0A or 9.0V⎓2.77A (PPS) 3.3-5.9V⎓3.0A or 3.3-11.0V⎓2.25A

## Sharge Phantom C to C Cable

- 480Mbps
- PD 240W, Max 5A

## Sharge Shargeek 100 (powerbank)

- 25,600mAh large capacity.
- 100W in/out fast charging. Fully recharged in 90mins.
- USB-C1: Max. 100W (PD 3.0)
- USB-C2: Max. 30W (PD 3.0 & QC 4+)
- USB-A: Max. 18W (QC 3.0)
- DC: Max. 75W

## Sharge Capsule Gravity (powerbank)

- 5,000 mAh
- USB-C × 1
- PD 2.0/3.0
- Input: 5V⎓2A, 9V⎓2A, 18W MAX
- Output: 5V⎓2.4A, 9V⎓2.2A, 20W MAX

## Sharge Power Bank Solar Panel

- Max 12W output
- Transformation Efficiency (EFF) ≥22 %

## HP ZBook Firefly 15.6 inch G8 Mobile Workstation PC (laptop)

- Power HP Smart 65 W External AC Power Adapter; HP Smart 45 W External AC Power Adapter
- Battery type HP Long Life 3-cell, 56 Wh Li-ion polymer
- Measured at 49 Wh under load

## Halfords 5 in 1 Jump Starter

- 12,000mAh capacity rechargeable lithium battery
- Suitable for 12V petrol vehicles up to 6L or diesel vehicles up to 3L
- Quick-charge USB-A output for charging mobile devices
- 12V DC Output
- USB Socket: 5V, 2.4A USB
- 12V DC output with an included cigarette lighter adapter gives 10A output for additional powering options

## Converting between Watts and Amps

> Volts × Amps = Watts

- 12V × 25.6Ah = 307.2Wh
- 4182mV × +8738mA = 36.5W
- 4312mV × -200mA = -0.9W

## Linux tools

```text
$ acpi
Battery 0: Discharging, 99%, 01:45:58 remaining
```

## References

- https://manhattanproducts.eu/pages/usb-c-pd-charging-everything-you-need-to-know
- https://www.androidauthority.com/usb-power-delivery-806266/

