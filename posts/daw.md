# DAW

## Reaper

- Download: https://www.reaper.fm
- Reaper stash samples: https://stash.reaper.fm

## Logic Pro

macOS only.

## Others

- https://www.tracktion.com/products/waveform-free

