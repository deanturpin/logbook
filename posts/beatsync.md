# Beat sync

So there's a lot of a fuss about the BEAT SYNC button on modern DJ decks. Obviously I'm biased towards tech, but you have to use the tools! In fact civilisation itself is measured on its ability to manufacture devices to make our lives easier. It's like buying a hammer and hitting the nail with your fist. In the dark. The end result is the same but the journey is arduous.

On records you were obviously in the dark, but there still remains this stigma about syncing with a button: waveform displays and tempo counter are fine; but making two numbers the same remains an artisanal practice.

## Comparison with modern photography

Could beat sync be comparable to autofocus on cameras? People still do insist on focussing manually but they have to accept that most of their photos wiill be blurred.

