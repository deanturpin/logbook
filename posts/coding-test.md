# Interview coding tests

1. Listen: usually all the information in the question is useful
1. Example: make examples larger, avoid special cases
1. Brute force: better to have brute force than nothing at all; don't necessarily code it, maybe just state the runtime
1. Optimise
1. Walk through your algorithm
1. Code
1. Test: use small test cases, then edge cases
1. Analyse and optimise: check for loops, maths, walk through the code, not the algorithm

## Whiteboard
- Use straight lines
- Start top left
- Leave space to insert lines of code
- Modularise up front (use a function that you haven't yet written)

## B.U.D.
- Bottlenecks
- Unnecessary work
- Duplicated work

## Space / time tradeoffs
- Consider saving time by using extra space (often hash tables)

