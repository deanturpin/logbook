# Preparing for a C++ interview

The [Amazon tech interview
topics](https://www.amazon.jobs/en-gb/landing_pages/p-software-development-topics)
is a great guide to whet your appetite. Know your language standards,
algorithms and data structures. Be comfortable calculating and discussing the
complexity of your solutions (Big O notation).

> What's the difference between symmetric and asymmetric encryption?

Get some side projects on GitLab to talk about at interviews and run/deploy
them as a daily cron job. Be aware that recruiters may remove any contact
details in your CV -- including URLs with your coding projects -- so be sure to
obfuscate them. Your website might be easy to find but very few recruiters have
time to research you.

> Tell me something you did beyond all expectations.

Practise your coding tests. It's good to get in a regular rhythm of doing these
tests -- ideally daily -- well in advance of any interview.
[HackerRank](https://www.hackerrank.com/) is a popular platform and
[Leetcode](https://leetcode.com/) has (subscription-only) company-specific
tests for the big players like Amazon and Facebook. It's good to experience
both as the IDEs are slightly different. Also see this [curated list of
interview
questions](https://github.com/MaximAbramchuck/awesome-interview-questions/blob/master/README.md#c).

> What's the worst bug you've ever fixed?

Do the skill assessments on [Pluralsight](https://www.pluralsight.com/) to
highlight your weaknesses and do the recommended courses. The more senior the
role you're going for the more you'll be expected to know design/algorithm
patterns and talk about the software life cycle.

Increasingly you must be aware of a larger tech stack.

> Imagine you're a browser requesting a web-page, describe all the technologies
> you meet along the way.

Register with a cloud provider, spin up some instances of Windows and Linux VMs
and get comfortable connecting to them remotely (via ssh, remote desktop).

## Study topics
- Modern C++ (post-11)
- Object-oriented programming
- Algorithms and Big 'O' notation
- Data structures
- Design patterns
- Software development life cycle
- Imagine you're a browser...
- TCP/IP, networks
- Clouds and containers
- Operating systems
- Parallelism, multi-threading
- CPU caches
- Cryptography
- Agile
- Databases
- Cyber security / penetration testing
- Cryptocurrency

## References
- [C++ datastructures and algorithms revision](https://github.com/sachuverma/DataStructures-Algorithms)

