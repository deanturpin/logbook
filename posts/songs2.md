---
title: Songs of note
date: 2020-05-06
tags:
- music
- guitar
---

# Songs of note

# A Forest -- The Cure
```
Verse
Am C F Dm

Interlude
B C F# F
```

# Lullaby -- The Cure
```
Verse
C#m7 C#m7 C#m7 C#m7
Amaj7 Amaj7 Amaj7 Amaj7

Interlude
F#m F#m F#m F#m
Amaj7 Amaj7 Amaj7 Amaj7
```

# Road To Nowhere -- Talking Heads
```
E E E E
C#m C#m C#m C#m
A A A A
B B E E
```

# Drive -- Aimee Mann
```
Verse
C Cmaj7 C C
C Cmaj7 C C

Chorus
Am D Am D
C Cmaj7 C C

Bridge
C Am C Am
Em F C G
```

# She's Always A Woman -- Billy Joel
```
Verse
A D A D D
D G D G G

Chorus
A D A F# F#
Bm Bm/A G A

Interlude
D G D
```

# Mystify Me -- INXS
```
Verse
Dm Am G F
Dm Am G F

Chorus
Am C G G
Am C G G
```

# Your Song -- Elton John
Capo on 1st fret.

```
Interlude
D G A G

Verse
D G A F#m
Bm Bm/A Bm/G# G
D A F# Bm
D Em G A A

D G A F#m
Bm Bm/A Bm/G# G
D A F# Bm
D Em G D D

Pre-chorus
A Bm Em G
A Bm Em G

Chorus
Bm Bm/A Bm/G# G
D Em G A

Interlude
D G A G

Verse
D G A F#m
Bm Bm/A Bm/G# G
D A F# Bm
D Em G A A

D G A F#m
Bm Bm/A Bm/G# G
D A F# Bm
D Em G D D

Pre-chorus
A Bm Em G
A Bm Em G

Chorus
Bm Bm/A Bm/G# G
D Em G A

Chorus 2 (ends on D)
Bm Bm/A Bm/G# G
D Em G D

Outro
D G A G D
```

# Outdoor Type -- Lemonheads
With capo on fret 2.

```
Intro
C C/B Am Am
C C/B Am Am

Verse
C C/B Am Am
C C/B Am Am
F G C C/B Am
F G F

Intro
C C/B Am Am
C C/B Am Am

Verse
C C/B Am Am
C C/B Am Am
F G C C/B Am
F G F

Interlude
Em F F G
Em F F G
F G C C/B Am
F F F G
```

# Cosmic Dancer -- T. Rex
```
Verse
G Em G Em
F C F C

Chorus
Am Am D D
```

# Push & Pull -- Nikka Costa
```
C Am Em G
```

# Songbird -- Oasis
```
G G G F#m
Em Em Em F#m
```

# Enjoy the silence -- Depeche Mode
Note the D# amd D#m in the intro and verse.

```
Intro
Cm D# Cm D#

Verse
Cm D#m G# G#

Chorus
Fm G# Cm A#
Fm G# Cm B
```

# Where the wild roses grow - Nick Cave and Kylie Minogue
```
Chorus
Gm Gm Cm Gm Gm Gm A# A# D D
Gm Gm Cm Gm Gm Gm Gm F Gm Gm

Verse
Gm Gm A# A# Cm Cm D D
Gm Gm A# A# Cm Cm D D
Gm Gm A# A# Cm Cm D D
Gm Gm A# A# Cm Cm D D D7 D7
```

# Memorial -- Devendra Banhart
```
Verse
A Bm D A

Chorus
E E F#m F#m
```

# Toothpaste kisses -- The Maccabees
```
Verse
A C#m7 A C#m7
D7 D7 G F#

Chorus
Bm Bm E E
```

# Your love is weird -- Beck

|string|E|E7|A|Fm7|
|---|---|---|---|---|
|E|0| | | |
|B|0|3|2|2|
|G|1|1|2|2|
|D|2|0|2|2|
|A|2|2|0|4|
|E|0|0| | |

With capo on fret 2.
```
E E E E7
A Fm A Fm
```

# Hurt -- Nine Inch Nails (Johnny Cash version)
```
Verse
Am Am C D

Chorus
G Am F C
```

# Please Please Please -- The Smiths
Capo on fret 2. Let the high E string ring throughout.

```
Verse
C G C G
Em F G G

Chorus
F G C B F
F G Am Am7
F G C C7
```

# Stronger -- Sugababes
Note the interesting Am to A change and then back again. You can also play around with Am7 and F7. The interlude is a Dm but with a descending root and an extra bar just for fun.

Repeat the first block twice, then the interlude, followed by two more choruses and finish on the A.

```
Intro
Am

Verse
F Am F Am
F Am F Am

Bridge
Dm F E E7

Chorus
F Dm A A
F Dm A A
```

```
Interlude
Dm C# C B
Dm C# C E E7
```

# Don't Think Twice -- Bob Dylan
Essential finger picking classic. Capo on 4th fret. See [Gareth Evans's lesson](https://www.youtube.com/watch?v=W2RdIH8A3yc).

```
Intro
C G Am Am
F G C C

Verse
C G Am Am
F F C G
C G Am Am
D7 D7 G G7

Chorus
C G C7 C7
F F D7 D7
C G Am F
C G C C
```

# Hallelujah -- Jeff Buckley
Not such a weird progession but it wanders around enough to need to write it down. Repeat four times.

```
Intro
C Am C Am

Verse
C Am C Am
F G C G
C F G Am F
G E7 Am Am

Chorus
F F Am Am
F F C G
```

# Freedom -- George Michael
The song revolves around C but the bridge takes it to Cm! With a descending bassline before ultimately resolving back to C.

```
Chorus
C A# F C
C A# F C

Verse
G F C C
G F C C
G F C C
G F C C

Bridge
Cm B A# A
Cm B A# G7
```

## References
- [Dean Turpin songs](https://turpin.dev/post/songs/)
- [Oolimo guitar chord analyser](https://www.oolimo.com/guitarchords/analyze)
