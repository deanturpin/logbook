# Logic Pro

Selecting a laptop for running Logic.

## The brief

Long time user with a 15 year gap, looking to get back into it in 2024. I worked a lot with versions 6, 7, Express... thinking about a MacBook Pro but not sure how much to spend. I always seemed to be fighting the CPU in the past, but does modern Logic make use of the more capable graphics cards on the top tier laptops?

## tl;dr... MacBook Air!

After much procrastination I have ended up with this configuration.

[![](https://mermaid.ink/img/pako:eNqFVf9u2jAQfpWTpUmdVBC_yi9Nm0rptKqlWgtrpQmpMsmReA125Di0WeE99j57sZ1NEwLrGH8E-77v7LvvzvYL85SPrM8CzeMQrm6nEui3-b57B1c8NireTBfcmyn1-DJlI-4NaASnQk_ZegeFSqUCZlY_mrJJmEof9UxFBupT9v4tXmOP13iT94N7j0T8gtyPQyXRGd5kxuoJ9ZGNMBjzOULTsYp07oVGH3xcCg-Twpr4Dx7XPrnVJwMYD8HOiuV9kcQRzwhtXeaTAgzzkBKb8QAz1MNM8oXwYDiBeqdTg69abUMVHtFuf_8ansP1pH5aAIlRmgdI4PmzQS15lJtKvr6wmQlPq0ulA3jEbKbKkQapMNxmfx6hZzTFsLEUBJ76QlH97qzUrU473lZPSUH7JeQ8NpZVWHYFvCbhwCgI-RI3xqWQWfTAFzG53tkx0JicXDkcWAA-uqrBXkEiTBLwlJQUtFAy2VfW9shAJQg3Z9Cp1dza1cpqEKVolDLhKu-APJU0sUKO7P__yLmGxL_cynnQRXKr0vXpGGbUhWmc8-_FXOxRfeX69tt4UDlzk7e68a_MXZXgA-m3ikMujVqsbOPsg3lx98wfV6OL4cXKtUsZs9C2qPtAqY93F6xSZrJU9NW24HkmdNpdsZ1Lbmo4k8tZbsXYGF_P0K41b_etPJNQyCCBC-CRpvAyUE8yPy1ZhOWYYS6iqB-JIDSBRvwHrXGIZvU6jHsH4ddLyFEoExlgGd7IuQMWed6kmJTKT5YKnHFJietUAi5RZ8ZKASbUKg1C4E61TyX6fcgN8JlK6QtF44LthKLFy_zPaUTH8fWicvG4_Dy67VON7JgtUC-48OlheLFuU2ZCXNBp6tPQxzlPIzNlU7kmKk-NGmfSY32jUzxmaexzg0PB6UlZ5MaYS9Z_Yc-s3-icVHvterPW7NRrvWbn5JhlrF_vNasn3ZNWrdlqNZrdRnd9zH4qRf61arvd6NFF2qn1Wu1ur9vKtzj3bS-z_pxHCW2BbjraPGfuVXPbfner2CjWfwBWiDPs?type=png)](https://mermaid.live/edit#pako:eNqFVf9u2jAQfpWTpUmdVBC_yi9Nm0rptKqlWgtrpQmpMsmReA125Di0WeE99j57sZ1NEwLrGH8E-77v7LvvzvYL85SPrM8CzeMQrm6nEui3-b57B1c8NireTBfcmyn1-DJlI-4NaASnQk_ZegeFSqUCZlY_mrJJmEof9UxFBupT9v4tXmOP13iT94N7j0T8gtyPQyXRGd5kxuoJ9ZGNMBjzOULTsYp07oVGH3xcCg-Twpr4Dx7XPrnVJwMYD8HOiuV9kcQRzwhtXeaTAgzzkBKb8QAz1MNM8oXwYDiBeqdTg69abUMVHtFuf_8ansP1pH5aAIlRmgdI4PmzQS15lJtKvr6wmQlPq0ulA3jEbKbKkQapMNxmfx6hZzTFsLEUBJ76QlH97qzUrU473lZPSUH7JeQ8NpZVWHYFvCbhwCgI-RI3xqWQWfTAFzG53tkx0JicXDkcWAA-uqrBXkEiTBLwlJQUtFAy2VfW9shAJQg3Z9Cp1dza1cpqEKVolDLhKu-APJU0sUKO7P__yLmGxL_cynnQRXKr0vXpGGbUhWmc8-_FXOxRfeX69tt4UDlzk7e68a_MXZXgA-m3ikMujVqsbOPsg3lx98wfV6OL4cXKtUsZs9C2qPtAqY93F6xSZrJU9NW24HkmdNpdsZ1Lbmo4k8tZbsXYGF_P0K41b_etPJNQyCCBC-CRpvAyUE8yPy1ZhOWYYS6iqB-JIDSBRvwHrXGIZvU6jHsH4ddLyFEoExlgGd7IuQMWed6kmJTKT5YKnHFJietUAi5RZ8ZKASbUKg1C4E61TyX6fcgN8JlK6QtF44LthKLFy_zPaUTH8fWicvG4_Dy67VON7JgtUC-48OlheLFuU2ZCXNBp6tPQxzlPIzNlU7kmKk-NGmfSY32jUzxmaexzg0PB6UlZ5MaYS9Z_Yc-s3-icVHvterPW7NRrvWbn5JhlrF_vNasn3ZNWrdlqNZrdRnd9zH4qRf61arvd6NFF2qn1Wu1ur9vKtzj3bS-z_pxHCW2BbjraPGfuVXPbfner2CjWfwBWiDPs)

## Disk use

Logic itself is a whopping 1.15GB but still, I've only used half of the Air's 512GB.

## But how did we get there!?

Various responses from Facebook.

> I am currently working on a song with about 100+ channels, loaded with 3'rd party plugins and the processing is not even at 50%
Macbook Pro M2, 32 gb ram.

> Logic itself doesn't make much use of the GPUs. There is a company which hopes to change that (GPU AUDIO) - but at the moment, Logic is 99% CPU/RAM dependent. (once a file/sound is loaded from storage)

> I’d disagree on some older machines. But with intel machines and multiple displays it made a huge difference, especially if you were scaling resolutions. I had a maxed out mini at one point that need an eGPU just to have Logic play nicely with two 27” screens. Only one and all the issues went away. It was well documented by other users when I researched it trying to troubleshoot. The apple silicon doesn’t seem to have any of the same issues, and they all have far more powerful GPU built in than any intel machines dreamed of.

## Articles and groups

Various articles below, but [this video](https://www.youtube.com/watch?v=i8NnPmK2MZo) was the clincher that 

- [Apple Insider comparison of Pro and Air](https://appleinsider.com/inside/15-inch-macbook-air/vs/m3-15-inch-macbook-air-vs-m3-14-inch-macbook-pro----comparison)
- [Apple Logic Pro X Users Group](https://www.facebook.com/groups/543628065696081)

## Apple store

I had a chat to an actual user at the Apple Store in Brighton.

We agreed that the £1500 Air was the same spec as the £2000 Pro (bar HD size), but the Pro has a bit more connectivity (extra USB-C and HDMI) and a "low impedence" headphone socket for my posh cans (which I normally use with a USB DAC, but I might just go dirty Bluetooth on my travels anyway). The Pro also has fans but he said he'd never heard them spin whilst using Logic: hoist with his own petard, perhaps?

You can see some preconfigured models here:

- [MacBook Air](https://www.apple.com/uk/shop/buy-mac/macbook-air/13-inch-m3)
- [MacBook Pro](https://www.apple.com/uk/shop/buy-mac/macbook-pro/14-inch)

However, I didn't realise for quite a while that you can add a 1TB disk to a MacBook Air for £200 (as none of the example configurations had a 1TB HD): I assumed it didn't fit in the case! So you _can_ get the Air to the same spec as the Pro -- in terms of HD and RAM -- for £400 less. The Pro, of course, has a better screen.

- Configure your [Air](https://www.apple.com/uk/shop/buy-mac/macbook-air/13-inch-midnight-apple-m3-chip-with-8-core-cpu-and-10-core-gpu-16gb-memory-512gb)
- Configure your [Pro](https://www.apple.com/uk/shop/buy-mac/macbook-pro/14-inch-space-grey-apple-m1-pro-chip-with-8-core-cpu-and-14-core-gpu-16gb-memory-1tb)

## Specs

| Model | CPU | GPU | RAM | Storage | Display | Thunderbolt ports | Price | Headphone | HDMI | SDXC |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Air | 8 | 10 | 16 | 512GB | 13.6" | 2 | £1499 | &#10003; |||
| Pro | 8 | 10 | 16 | 1TB | 14" XDR | 2 | £2099 | &#10003; | &#10003; | &#10003; |
| Pro | 11 | 14 | 18 | 512GB | 14" XDR | 3 | £2099 | &#10003; | &#10003; | &#10003; |
| Pro | 12 | 18 | 18 | 1TB | 14" XDR | 3 | £2499 | &#10003; | &#10003; | &#10003; |
| Pro | 12 | 18 | 18 | 512GB | 16" XDR | 3 | £2599 | &#10003; | &#10003; | &#10003; |
| Pro | 14 | 30 | 36 | 1TB | 14" XDR | 3 | £3299 | &#10003; | &#10003; | &#10003; |
| Pro | 14 | 30 | 36 | 1TB | 16" XDR | 3 | £3599 | &#10003; | &#10003; | &#10003; |

## Final configuration

| Device | Description | RRP |
|-|-|-|
| MacBook Air M3 16GB RAM 512GB HD | Laptop | 1499 |
| Volt | Audio interface | 349 |
| BeyerDynamic DT 1770 | Closed-back headphones | 300 |
| UA MIDI keyboard | USB-C keyboard | 89 |
| Rode Nt1A | Condenser mic | |
| Stagg mic cable | | |
| Mic stand | | |

## Performance

So RAM appears to be the bottleneck, not HD size. If you don't start much on login you can get the quiescent RAM usage down to 30% of 16GB. Logic barely even tickles the CPU.

## Migrating old projects

Logic 6 didn't bother putting file headers or even extensions on bounces so I had to convert all unknown files into something useful. Format was guessed by importing the raw audio data into Audacity with various options until the waveform looked sensible.

Script to convert all files without extensions in the current directory into WAVs.

```bash
for file in *; do
    [[ $file =~ .*\..* ]] || ffmpeg -f s24be -ar 44100 -ac 1 -i "$file" "$file.wav"
done
```

## Project file sizes

00:05:30 duration song rip

| rate (KHz) | rip | stems | bounce |
|-|-|-|
| 44.1 | 131 | 486 | 84 |
| 96 | 259 | 1210 | 239 |p

## VST plugins

- https://sampleson.com/tronic.html

## Mini synths

- https://www.amazon.co.uk/IK-Multimedia-dual-filter-synthesizer-connections/dp/B0CB8P7CQN

## Tutorials

- [10 tricks](https://www.youtube.com/watch?v=T9i_N7IG_nk)
- [Working with external synths](https://www.youtube.com/watch?v=cQ-anN9uwzY)

## Shops

- [Studio Spares](https://www.studiospares.com/#6a9b/fullscreen/m=and&p=4&q=mic+stand+desk)

