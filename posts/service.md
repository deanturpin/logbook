# Watch servicing
Don't fiddle with the date wheel between 9pm and 4am. If it has a [phantom date](https://calibercorner.com/phantom-date/) then good luck!

## Procedure
### Strip down
- Time with Android app
- Remove caseback
- Estimate amplitude 
- Let down mainspring 
- Remove case clamps 
- Remove stem/crown
- Decase
- Reinsert stem/crown
- Remove
    - Balance wheel
    - Hands
    - Dial
    - Hour wheel and dial washer
    - Cannon pinion
- Put in movement holder 
- Cleaning 
    - Soak large components in Renata
    - Dip small components in Renata 

### Oiling
- Oil jewels with 9010
- Grease high friction components with 8300
- Clean dial with water
- Fit hands 
- Blow dust
- Put back in case
- Time with Android app
- Regulat
- Estimate amplitude using slow motion mode on phone
- Clean crystal with Polywatch
- Buff caseback

## Exercises
- Remove balance 
- Remove click 
- Remove and rewind a mainspring by hand
- Remove and refit hands
- Get a box of old movements that you can destroy 
- I've found it very useful to have two of the same movement so you can use one as a reference and/or donor
