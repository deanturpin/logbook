---
title: Maksikanagasana
subtitle: Dragonfly pose (Hummingbird, Grasshopper)
date: 2020-12-13
tags:
- yoga
- language
- sanskrit
---

# Maksikanagasana

Mastika means "head" or "top of the head" and naga means "snake" or "cobra". In
Hindi, Gujarati and Sanskrit two or three words are joined, which is called
"sandhi".

मक्सिकानागा -- मक्सिकानागासन

