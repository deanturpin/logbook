# Cheap tool comparison

## Hand levers
- Bergeon £40
- eBay £4

The Bergeon are, of course, finished very well; but they are just plastic sticks at the end of the day. Also you always use dial protector so the tool itself doesn't touch the dial.

## Crystal press
- Bergeon £££
- eBay £20

Haven't actually used a Bergeon but the cheap one has been fine so far. I actually built up a felt dome on the bottom half of the press with decreasing sizes of felt furniture pads.

## Barrel press
- Bergeon £££
- eBay £5

Very simple bit of plastic. Does a simple job well and difficult to justify the Bergeon.

## Movement holder
The cheapie is OK, but Bergeon's is actually pretty reasonable and much better made.

- Bergeon £20
- Economy (H&S Walsh) £5
