# UK house price data

## Independent

- [rightmove](https://www.rightmove.co.uk/news/house-price-index/)
- [UK property data](https://propertydata.co.uk/charts/house-prices) (14-day trial, 25/month minimum thereafter)

## UK government

### Land registry

- https://landregistry.data.gov.uk/app/ukhpi#:~:text=Current%20index,compared%20to%20the%20previous%20year.
- https://landregistry.data.gov.uk/api-config

## Collections

- https://www.gov.uk/government/collections/uk-house-price-index-reports
- https://www.gov.uk/government/collections/price-paid-data

## Use land property data

Lots of information on https://use-land-property-data.service.gov.uk/

Including:
- Geospatial Data Catalogue
- INSPIRE Index Polygons spatial data
- National Polygon Service
- Overseas companies that own property in England and Wales
- Price Paid Data
- Registered Leases
- Request for information (requisition) data
- Restrictive Covenants
- Transaction Data
- UK House Price Index: reports
- UK companies that own property in England and Wales
- 1862 Act Register

## HM land registry geospatial data

- Administrative Areas
- Approved Estate Plans
- Bankruptcy Information
- Bona Vacantia
- Cautions
- Chancel Repair
- Charities
- Copy Deeds
- Copyhold
- Easements
- Equitable Charges
- Escheat
- Exempt Information Documents
- HM Land Registry Property Gazetteer
- Index Map
- INSPIRE Index Polygons spatial data
- Legal Charges
- Mines and Minerals
- National Polygon Dataset
- Notices
- Overseas Companies that own property in England and Wales
- Personal Covenants
- Price Paid Register Entries
- Price Paid Data
- Private Individuals
- Profit à Prendre
- Provisions
- Registered Leases
- Rentcharges
- Restrictions
- Restrictive Covenants
- Survey Extents
- Survey Reports
- Title Descriptor Dataset
- Title number and UPRN Look Up dataset
- Title Plan
- Title Register
- UK Companies that own property in England and Wales

