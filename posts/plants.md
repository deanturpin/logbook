# Plants identified

## Back garden
- Mahaleb cherry
- Garden rose
- English ivy
- Potato vine
- Sweet mock-orange
- Mock privet
- European honeysuckle
- Apple mint
- Cleavers
- [Ovens wattle (Acacia pravissima)](https://en.wikipedia.org/wiki/Acacia_pravissima)
- Evergreen spindle
- Common columbine
- Wood avens
- Creeping cinquefoil
- Common holly
- Common hyacinth
- Blue leadwood
- Broadleaf enchanter's nightshade
- Purple toadflax
- European plum
- Common dandelion

# Front garden
#- [Seaside Daisy](https://en.wikipedia.org/wiki/Erigeron_glaucus)
- Serbian bellflower
- Common hawthorn
- [California poppy](https://en.wikipedia.org/wiki/Eschscholzia_californica)
- Red valerian
- Kenilworth ivy
- Majorcan hellebore
- Butter-and-eggs
- Annual mercury
- Butterfly bush
- Evergreen bugloss

## Other
- Horseshoe geranium
- Spreading pellitory
- Hedge mustard

