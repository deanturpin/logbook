---
title: Omega watches
---

# Omega watches

## Serial numbers by year (in millions)

| Decade | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 1890   |   |   |   |   |   | 1 |   |   |   |   |
| 1900   |   |   | 2 |   |   |   |   |   | 3 |   |
| 1910   |   |   | 4 |   |   |   | 5 |   |   |   |
| 1920   |   |   |   | 6 |   |   |   |   |   | 7 |
| 1930   |   |   |   |   |   | 8 |   |   |   | 9 |
| 1940   |   |   |   |   | 10|   |   | 11|   |   |
| 1950   |12 |   |13.5|  |14 |   | 15|   | 16|   |
| 1960   |17 | 18| 19| 20| 21| 22|23 | 25| 26|28 |
| 1970   |29,32 | 33| 34| 36| 38| 39|   | 40| 41| 42|
| 1980   |44 |   | 45|   | 46| 48| 49, 50|   |   |51 |
| 1990   |   | 53|   |   |   | 55|   |   | 56|   |

52 not used.

## Reference decode
| Number | 1st Digit | 2nd Digit | 3rd Digit |
| --- | --- | --- | --- |
| 1 | Gent’s Watch - leather strap| Manual winding without second | Non-water-resistant |
| 2 | Gent’s Jewelry Watch | Manual winding small second | Non-water-resistant calendar | 
| 3 | Gent’s Watch - bracelet | Manual winding center second 	| Non-water-resistant chronometer |
| 4 | Gent’s Jewelry Bracelet Watch | Manual winding chronograph | Non-water-resistant chronometer calendar |
| 5 | Lady’s Watch | Self-winding with second | Water-resistant |
| 6 | Lady’s Jewelry Watch | Self-winding center second | Water-resistant calendar |
| 7 | Lady’s Bracelet Watch | Self-winding chronograph | Water-resistant chronometer |
| 8 | Lady’s Jewelry Bracelet Watch | Electronic chronograph | Water-resistant chronometer calendar |
| 9 | | Electronic | |

See https://www.bobswatches.com/omega/serial-numbers

## Models
| Reference | Detail | Calibre |
| --- | --- | --- |
| 121.001  | 1964 Mechanical ||
| 125.007  | 1964 Seamaster 30 || 
| 131.6412 | GENEVE 9ct SOLID GOLD 17 JEWELS | 601 |
| 131.019  | Genève | 601 |
| 132.0019 | | 613 |
| 135.011  | c1969 | 601 |
| 135.041  | c1969 | 601 |
| 136.020  | Seamaster De Ville | |
| 136.041  | 1969 Genève orange seconds | 613 |
| 136.0049 | Genève | 613 |
| 136.0050 | | 613? |
| 136.070  | day-date G20 GC | |
| 145.029  | 1982 Seamaster manual chronograph | 861 |
| 145.012  | 1967 Speedmaster Pre Moon ||
| 162.009  | 1965 automatic dress watch | 565 |
| 162.009  | 1965 Solid 18k Gold Geneve Automatic | 562 |
| 165.003  | Seamaster | 552 |
| 165.024  | Seamaster 300 | 552 |
| 166.002  | 1960 Seamaster | 552 |
| 166.037  | | 565 |
| 166.041  | 1972 G20 | 561 |
| 166.070  | day-date | 565 |
| 166.098  | | 1481 |
| 166.0117 | Geneve Gold Men's Watch | |
| 166.0163 | 1972 | |
| 166.0169 | | 1022 | 
| | | |
| 161.009  | 1965 automatic dress watch | 522 |
| 131.019  | Genève | 601   |
| 136.0049 | 1972   | (613) |
| 165.070  |        | (552) |
| 131.009  | 1965   | 601   |

## Reference codes
```
BA Yellow gold
BC White gold
BG Pink gold
BT Platinum
DA Yellow gold combination
DD Gold combination
DG Pink gold combination
DL Gold combination
MD Gold plated
SP Strap
ST Stainless steel
TA Titanium and yellow gold combination
TI Titanium
TL Titanium and pink gold combination
```

## Appoximate eBay prices

| Manual | Sale price |
| --- | --- |
| 601 | £400+ |
| 613 | £376 |
| 132.0019 cal 613 | £376 |
| 136.070 613 - day-date G20 GC | £526 |
| 135.041 c1969 Cal 601 with Original Box/Papers - For Restoration | £277 |
| 131.6412 (?) cal 601 - VINTAGE OMEGA GENEVE 9ct SOLID GOLD 17 JEWELS | £398.89 |
| Gold cal 601 | £265 |
| VINTAGE OMEGA DE VILLE MANUAL WINDING 17 JEWELS CAL.625 | £436 |

| Automatic | Sale price |
| --- | --- |
| cal.552 - vintage mens omega seamaster automatic BC | £225 |
| 166.002 Cal. 552 - 1960’s Vintage Omega Seamaster  | £297 |
| 162.009 Cal.562 - 1965 Solid 18k Gold Vintage Omega Geneve Automatic | £840 |
| Seamaster Cal. 562 Ref. 14905 SC 62 | £390 |
| 166.041 cal 561 - 1972 G20 | £280 |
| 166.070 Cal. 565 - day-date | £362 |
| 166.037 Cal.565 | £485 |
| OMEGA Geneve Gold Men's Watch - 166.0117 | £205 |
| 166.098 Cal.1481 | £310 |
| 166.0163 cal ? - 1972 S&R | £175 |
| 166.0169 cal 1xxx Day/ Date Mens Wristwatch Circa 1972/3 Working Well | £229 |
| 166.0169 Cal 1022 - Geneve Automatic yellow dial | £389 |
| 166.0169 Cal 1022  | £215 | 
| cal 1481 - Omega Geneva Automatic S&R | £285.00 |
| OMEGA CONSTELLATION AUTOMATIC WRISTWATCH | £440 |
| 18ct Century 552 | £735 |
| Vintage Omega De Ville 9CT Gold Automatic Watch | £412 |
| 564 movement and dial only | £210 |
| Gold constellation | £565 |

## References
- [Vintage Omega watch parts](https://welwynwatchparts.co.uk/products/omega-calibre-600-601-movement-parts-used)
- [ Ref guide](http://www.chronomaddox.com/casereferenceguide.html)
- [Omega serial numbers by year](http://chronomaddox.com/omega_serial_numbers.html)
- https://www.chronomaddox.com/OmegaCaliberList.html
- [Speedmaster serial numbers](https://est1897.co.uk/pages/omega-serial-number-guide)
- http://download1076.mediafire.com/hssqzemjdd2g/f2wh10fa1rtxe85/Omega+Constellation+UltraThins+of+the+Sixties+and+Seventies_June2013.pdf
- [How to identify a vintage Omega](https://wahawatches.com/how-to-identify-a-vintage-omega-2019/)
- [Examples of vintage Omegas](https://watchesoflancashire.com/watches/vintage-omega-watches/)
- https://www.omegawatches.com/vintage-watches
- http://www.chronocentric.com/omega/choosing.shtml
- https://www.omegawatches.com/en-gb/watch-omega-geneve-omega-136-0049
- [Omega cal. 1480/1481, the forgotten movement?](https://www.watchuseek.com/threads/omega-cal-1480-1481-the-forgotten-movement.740850/)
- [Top 10 (new) Omega calibres](https://www.montredo.com/top-10-omega-calibres/)
- https://millenarywatches.com/omega-calibers/
- https://www.chronomaddox.com/OmegaCaliberList.html
- https://thewatchprofessional.co.uk/2020/10/09/watch-service-omega-geneve-cal-552/
