# Managing project dependencies

It's difficult to choose a dependency management tool without first-hand experience, so I'm using a few to manage my own projects.

> tl;dr: currently using a monorepo and CMake's `ExternalProject_Add`

---

## Evaluated

- [git submodules](https://turpin.one/posts/git-submodules)
- [Android Repo](https://turpin.one/posts/android-repo)
- [Subversion](https://turpin.one/posts/subversion-to-git-migration)
- git subtrees

## To be evaluated

- Just one big repo
- [Tundra](https://github.com/deplinenoise/tundra)
- [Apache Maven](https://en.wikipedia.org/wiki/Apache_Maven)
- [Meson](https://mesonbuild.com/Dependencies.html)
- [Conan](https://conan.io/) C/C++ package manager
- [Bazel](https://docs.bazel.build/versions/master/install-ubuntu.html) (used by TensorFlow)
- [Buck](https://buck.build/)
- [Pants](https://v1.pantsbuild.org/)
- [Chromium depot tools](http://dev.chromium.org/developers/how-tos/depottools)
- [repobuild](https://github.com/chrisvana/repobuild/wiki/Motivation)
- [Ninja](https://ninja-build.org/)
- Premake

## References
- [Dependency hell](https://en.wikipedia.org/wiki/Dependency_hell)
- [In quest of the ultimate build tool](http://blog.ltgt.net/in-quest-of-the-ultimate-build-tool/)

