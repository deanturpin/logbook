# Recipes

## Chicken curry

### Marinade

- Blend 6 cloves of garlic with equal quanitity of ginger
- Add 2 tbsp of the garlic and ginger mix to the chicken
- 1/2 tsp turmeric
- 1/2 tsp normal chilli powder
- 1/2 tsp Deggi Mirch
- 2 tbsp fish spice mix
- Mustard oil

### Sauce

- Onion
- Oil (mustard or rapeseed)
- Salt (brings out the moisture so it caramelises quicker)
- Brown for five minutes
- Add remainder of garlic and ginger paste
- Turmeric
- Meat spice mix
- Tomato puree
- Add chicken and seal until not translucent
- Add enough chicken stock to cover
- Add splash of soy sauce
- Cook on mendium heat for half an hour

## Spicy pie

### Dough

- 240g whole Spelt flour (I had some whole and some white so used both)
- 1/2 tsp fine salt
- 80ml coconut oil melted (I used a mix of Coconut, Olive and Rapeseed)

Sift the flour and salt into a bowl, pour in the oil and mix in with a fork. Add a little water at a time and mix together with hands, kneading until it comes together. Don't overwork! Cover with a damp tea towel and rest at room temp for an hour.

### Beef Filling

- 500g minced beef
- 1 onion finely chopped
- 2 garlic cloves crushed
- 1/2 scotch bonnet finely chopped or some chilli powder
- 1/2 tsp turmeric
- 1 tbsp Jamaican curry powder
- 1 tsp vinegar
- 1 sprig thyme
- Sprinkle of dark brown sugar
- 1 bay leaf
- 1 tsp cayenne powder
- 4 Allspice berries (ground)
- A squirt of tomato puree
- Beef stock
- Glug of dark Soy Sauce
- salt
- black pepper

Fry the onions in some oil until soft, then add garlic and chilli and cook down for a few minutes. Add in the mince and stir frequently so it's nicely browned. Add turmeric and curry powder then the tomato paste and other ingredients. Add some stock (to just cover the mixture) once the flavours have started to come together, and bubble away on a low flame until most of the liquid has gone. Taste and add what you think it needs, then cool.

Oven at 200 C, line a baking tray.

Divide the dough into 6 balls and roll out to a diameter of about 7 inches as evenly as poss. Put some filling on one side of the circle then fold the other side over and press the edges with a fork. Brush with a beaten egg.

20 - 25 minutes later, hello patties!

## Mackeral bun sice

- One fillet of smoked mackeral
- Grated beetroot
- Lemon
- Apple cider vinegar
- Avocado oil
- Spring onion
- Serve on toast
- Add hot sauce to taste

