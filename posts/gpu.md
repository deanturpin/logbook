---
title: Heterogenous programming
date: 2020-04-26
tags:
- revision
- gpu
- cpu
---

# Heterogenous programming

- [Sycl 1](https://github.com/KhronosGroup/SyclParallelSTL) - pronounced "sickle"
- [Sycl 2](https://www.khronos.org/sycl/)
- [C++ execution policies](https://github.com/KhronosGroup/SyclParallelSTL)
- Microsoft Visual C++ Parallel STL
- [CUDA](https://developer.nvidia.com/how-to-cuda-c-cpp)

## References
- [C++ parallel performance](https://www.bfilipek.com/2018/11/parallel-alg-perf.html)
- [Quora](https://www.quora.com/Why-dont-C-and-Java-unify-the-CPU-with-GPU-so-that-we-can-use-parallelism-without-using-3rd-party-libraries/answer/Christopher-Di-Bella?share=5c2cb3d3&srid=CbmP)

