# Security through absurdity

It was recently suggested that I consider "using unusual ports" at work as a
security measure. After I got my breath back I found these quotes.

> Security experts have rejected this view as far back as 1851.

> Rogues are very keen in their profession, and know already much more than we
> can teach them.

> System security should not depend on the secrecy of the implementation or its
> components.

## References
- [Obscurity can complement established security methods](https://danielmiessler.com/study/security-by-obscurity/)
- [Kerckhoffs's principle](https://en.wikipedia.org/wiki/Kerckhoffs%27s_principle)
- [Security through obscurity (Wikipedia)](https://en.wikipedia.org/wiki/Security_through_obscurity)

