# Fixed income trading

> Financial instruments are monetary contracts between parties. They can be
> created, traded, modified and settled. They can be cash (currency), evidence
> of an ownership interest in an entity or a contractual right to receive or
> deliver in the form of currency (forex); debt (bonds, loans); equity
> (shares); or derivatives (options, futures, forwards). 

Fixed income trading is investing in bonds.

### Types of bonds
- Government
- High yield
- Investment gradeA
- Developed market
- Emerging market
- Floating rate
- Bank loans

## Refs
- https://en.wikipedia.org/wiki/Financial_instrument
- https://en.wikipedia.org/wiki/Derivative_(finance)
- https://www.blackrock.com/us/individual/education/fixed-income
