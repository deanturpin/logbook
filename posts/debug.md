---
title: Debuggers
date: 2021-10-16
tags:
- debug
- compiler
---

# Debuggers
## Call frames
- Parameters passed to the function
- Local variables of the function
- A snapshot of registers to use and restore
- A return address
- A frame pointer (for debugging)

