# Presents

Inspiration for presents. Some of the shops are in Brighton, UK.

## Sneakers

- [Hoff](https://thehoffbrand.com/): very colourful, interesting designs
- [Diverge](https://www.diverge-sneakers.com/): custom, two week turnaround
- [Laced](https://www.laced.com/)
- [Sneakersnstuff](https://www.sneakersnstuff.com/en/): good for Nike
- [Solebox](https://www.solebox.com/en_GB/home)
- [Soleheaven](https://www.soleheaven.com/)
- [Sneaker District](https://www.sneakerdistrict.com/)
- [Sneaker Politics](https://sneakerpolitics.com/)
- [Sneaker Baas](https://www.sneakerbaas.com/)
- [Sneaker Junkies](https://www.sneakerjunkiesusa.com/)
- [Sneaker Studio](https://sneakerstudio.com/)
- [Sneaker Town](https://www.sneakertown.com/)
- [Sneaker Lab](https://www.sneakerlab.com/)
- [Sneaker Politics](https://sneakerpolitics.com/)
- [Allbirds](https://www.allbirds.co.uk/)
- [No Bull](https://www.nobullproject.com/)
- [Farfetch](https://www.farfetch.com/)

## Shoes

- [Basq](https://basqcompany.com/pages/women-men): trainers made with recycled materials
- [Vivo barefoot](https://www.vivobarefoot.com/uk/)
- [Nike Dunk](https://www.nike.com/gb/w/dunk-shoes-90aohzy7ok)
- [GH Bass](https://www.ghbass-eu.com/)

## Clothes

- [Chris Simpson](https://picturesthatigoneanddone.com/collections/clothing)
- [Colorful Standard](https://colorfulstandard.com)
- [Wasted Paris](https://wasted.fr/)
- [The Couture Club](https://www.thecoutureclub.com/)
- [Wasted Heroes](https://www.wastedheroes-shop.com/collections/side-smiley)
- [Sophie Darling](https://www.sophiedarling.com/): cool printed jackets
- Stussy
- [WESC](https://www.wesc.uk/products/o414983t-leon-pop-smiley-parisian-night)
- [Worn On TV](https://wornontv.net/loot/nicholas/)
- [COS](https://www.cosstores.com/)
- Oklahomamam clouds scarf
- [YMC](https://www.youmustcreate.com/)
- [The Ragged Priest](https://www.theraggedpriest.com/)
- [The Kooples](https://www.thekooples.com/uk_en/)
- [All Saints](https://www.allsaints.com/)
- [ssense](https://www.ssense.com/en-gb)
- [Hayley Menzies](https://www.hayleymenzies.com/)
- [Wolf &amp; Badger](https://www.wolfandbadger.com/uk/)
- [Flannels](https://www.flannels.com/)
- [Never Fully Dressed](https://www.neverfullydressed.com/)

## Makeup

- [Culture Hustle](https://www.culturehustle.com/)
- [Cult Beauty](https://www.cultbeauty.co.uk/)
- [Charlotte Tilbury](https://www.charlottetilbury.com/uk/): good for lipsticks
- [Glossier](https://www.glossier.com/uk/en_gb): good for skincare
- [Nars](https://www.narscosmetics.co.uk/en_GB/home): good for blushes
- [MAC](https://www.maccosmetics.co.uk/): good for lipsticks
- [Bobbi Brown](https://www.bobbibrown.co.uk/): good for lipsticks
- [Fenty Beauty](https://www.fentybeauty.com/): good for lipsticks
- [Huda Beauty](https://www.hudabeauty.com/): good for lipsticks
- [Pat McGrath Labs](https://www.patmcgrath.com/): good for lipsticks
- [Anastasia Beverly Hills](https://www.anastasiabeverlyhills.com/): good for lipsticks
- [Urban Decay](https://www.urbandecay.co.uk/en_GB/home): good for lipsticks
- [Drops of Youth](https://www.thebodyshop.com/en-gb/face/serums-treatments/drops-of-youth-youth-concentrate/p/p000136): good for skincare

## Hair

- [Simon Webster Hair](https://www.simonwebsterhair.com/get-in-touch)

## Skin care

- [Absolute Collagen](https://www.absolutecollagen.com/)

## Spa

- [South Lodge](https://www.exclusive.co.uk/the-spa-at-south-lodge/)

## Jewelery

- [7879](https://7879.co/) -- pure gold, most expensive: solid
- [Jeremy Hoye](https://www.jeremy-hoye.co.uk/): long-establish Brighton-based jeweler (near the station)
- [Missoma gold](https://www.missoma.com/products/zenyu-fan-necklace-18ct-gold-plated): tops 14ct
- The Octopus Ring
- [The Great Frog](https://www.thegreatfroglondon.com/) -- slightly macabre, silver only
- [Sarah Layton](https://sarahlayton.co.uk/) -- gold plated
- [Stargaze](https://stargazejewelry.com/collections/drop-02), least expensive

## Home

- [Peugeot Saveurs](https://uk.peugeot-saveurs.com/en/bali-palais-des-epices-bali-black-cast-iron-pepper-mill-and-its-salt-cellar-as-a-gift-box.html)
- [The White Company](https://www.thewhitecompany.com/uk/): candles, diffusers, etc.
- [Elephant candlestick](https://www.hi-werns.com/uk/Candle-holder-parrot-on-elephant-pink-blue-white/54381)
- [Stackable cat glasses](https://doiydesign.com/products/kitty-ginger/)


## Art

- [Thicket Design](https://thicketdesign.com/shop)
- [Prescription](https://www.prescriptionart.com/products/pattern-up-soak-up-those-lies)

## Sunglasses

- [Ace & Tate](https://www.aceandtate.com/gb/)
- [Cubitts](https://www.cubitts.com/)
- [Kirk Originals](https://www.kirkoriginals.com/)
- [Taylor Morris](https://www.taylormorriseyewear.com/)
- [Oscar Deen](https://www.oscardeen.com/products/fraser-m-series-treacle?variant=40400330817618)
- [Monc](https://www.monclondon.com/)

## Alcohol

- [Friary](https://www.friarydrinks.co.uk/)
- [El Rayo Tequila](https://elrayotequila.com/): can get it in Sainsbury's

## Final touch

- Flowers
- [Prestige Flowers](https://www.prestigeflowers.co.uk/): flowers
- [Prestat](https://www.prestat.co.uk/): chocolates
- [Hotel Chocolat](https://www.hotelchocolat.com/uk/): chocolates
- Card
