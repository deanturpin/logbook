---
title: No comment
subtitle: Leaving commented code
date: 2020-12-13
tags:
- programming
---

# No comment
What are the alternatives to leaving commented old code in a codebase as a
reference? Many times I've been saved by something that had been commented, but
does the overhead of littering the code outweigh the occasional convenience?

Commented code is also destined to bit rot.

- Add an extended comment to remind your future self what was removed
- Gist?
- Move to legacy code examples -- would be cool if it turns up in IDE searches
- Document in a third party tool
- Get used to using `git log -p`
- GitLab blame
- Some other change visusalisation tool?
- Raise an ticket...?
