# Growing mangos

Mangos are notoriously difficult to grow in non-tropical climates -- Kew
Gardens has managed one fruit in twenty years -- but nevertheless I took five
scraped pips and planted them above and slightly below the surface of a
compost/manure mix. They were then covered with cling film and left in the
warm. After approximately four weeks *one* of the pips planted beneath the soil
sprouted.

And it grew to a good 30cm over the next six months, but ultimately withered
and died as the temperature dropped.

