---
title: Colour chords
subtitle: What does orange sound like?
date: 2020-01-22
tags:
- photography
- photo
- music
- colour
---

# Colour chords

Have you ever wondered what the equivalent of a chord is in light: i.e., can we see them and do they look nice? Like 300+400+500Hz makes a sweet sound for the ears. 

- What's the equivalent sound to orange?
- Why don't we see octaves?

## Visible spectrum
430–770 THz

- Red 405–480 THz
- Green 530–600 THz
- Blue 620–680 THz

## Examples
Orange is sRGB(255, 127, 0)

## References
- https://www.colorhexa.com
- https://en.wikipedia.org/wiki/Visible_spectrum
- https://en.wikipedia.org/wiki/Chromaticity
- https://en.wikipedia.org/wiki/Impossible_color

