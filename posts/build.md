# The cost of heavily refurbishing a flat in Brighton in 2020

Build was converting an open plan shop into residential (two bedroom flat).

Add 10% waste for all skirting, flooring and tiles.

Estimate the cost of the electrics as you go along if they're not defined by
the architect or you're diverging from what the building contractor has
specified. Anything to do with getting a new supply of electrics or water costs
at least a grand.

Patching the roof up was an unexpected expense.

Do all your fixtures and fittings the same colour? If you go for brass taps,
say, does everything else match: sockets, window handles, ceiling roses, plug
holes, light switches? You will be surprised how many things are steel...

Put the washing machine right next to the dryer. Think about which side your
toilet roll holder will be on. An outside tap is really useful if you have a
patio/garden.

## Building controls - signing off

If building control have asked for a sound test you might need to get it done
_before_ the floor goes down. If the pavement has been damaged during the build
you may be asked to repair it. However, your normal builder might not be able
to do it.

Double the sound insulation between floors. In fact, triple it! The acceptable
minimum dictated by the sound test isn't acceptable for people, especially if
there are kids running around upstairs.

## Finances

Be cautious about taking on extra debt before applying for a mortgage. A
credit card might get you through the build but a maxed credit card won't look
good on your credit record. If possible, borrow money from friends/family and
zero those unsightly credit cards.

Development loans: expect to pay 1% a month.

## Itemised costs

| Item | Unit | Cost |
| --- | --- | --- |
| Howdens kitchen | small | 3500 |
| Howdens kitchen | medium | 8500 |
| Bathroom tiles | m<sup>2</sup> | 35 |
| Engineered oak flooring | m<sup>2</sup> | 35 |
| Scaffold tower | | 500 |
| New water supply | | 1000 |
| New gas supply | | 2000 |
| Sound tests | 2 vertical airborne, 2 vertical impact | 500 |
| New electricity supply | | 2000 |
| Skirting board 168mm (primed MDF) | m | 5.59 |
| Architrave (primed MDF) | door (both sides) | 42 |
| Painter | room | 500 |
| Paint | 10L per room | 36 |
| Bathroom suite | small (en suite) | 800 |
| Flush plate (black) | | 68.98 |
| Concealed cistern | | 66.3 |
| Freestanding bath | | 429.97 |
| Towel rail (black) | | 110 |
| Towel roll holder | | 14 |
| Toilet pan and seat | | 124.97 |
| Flexible pan connector | | 22.03 |
| Shower waste | | 17.03 |
| Basin waste | | 17.03 |
| Waterfall tap | | 42.01 |
| Quadrant shower enclosure | | 199.97 |
| Vanity unit with sink | | 74.99 |
| Shower unit | | 168.76 |
| Internal fire door | | 100 |
| Front door | | 300 |
| Front door frame | | 120 |
| Letterbox | | 100 |
| Front door locks | cylinder + deadlock | 100 |
| Refurbished cast iron radiator with valves/stays | | 200 |
| Nest thermostat | | 169 |
| Large LED bulb | | 24 |
| Decorative flex, bulb holder and ceiling rose | | 18 |
| Internal door handles | pair | 15 |
| Door hinges | 3 per door | 15 |
| Full cost of single spot (1st, 2nd fix labour and materials) | | 50 |
| Single dimmer (brushed aluminium) | | 11.77 |
| Double socket (brushed aluminium) | | 6 |
| External wall light | | 35 |
| Washing machine | | 350 |
| Tumble dryer (condenser) | | 350 |
| Wooden floor (engineered, click fit) | m<sup>2</sup> | 35 |
| uPVC Windows | | TBC |

## Kitchen tiles
- [Attingham Earth™ Sun Decor Tile](https://www.toppstiles.co.uk/attingham/attingham-earth-sun-decor-tile)
- [Attingham Earth™ Sun Tile](https://www.toppstiles.co.uk/attingham/attingham-earth-sun-decor-tile)

## Windows
We went with white uPVC and changed the handles to silver. I'm fond of
[Velfac](https://velfac.co.uk/) but we just went along with what the builder
specified.

## Skirting
The kitchen and lounge ceiling are almost 3m high so I went for large skirting
board. Also the standard ogee and torus profiles seem very fussy,
[bullnose](https://www.skirtech.co.uk/Bullnose_MDF_655_Skirting_Board--product--43.html)
is much simpler. Similarly for the
[architrave](https://www.skirtech.co.uk/Bullnose_MDF_Architrave_Set_25mm--product--511.html).
Make sure the plumber knows how high your skirting boards are.

## Working with architects
We initially used a traditional local architect but it was expensive and we
weren't that happy with the results and service. So we ended up submitting
revised plans from [Archiplan](https://www.arkiplan.co.uk/). These were much
better value. However, the architect we chose didn't come to site so there were
some ommisions and features that didn't work which may have been mitigated by
coming in person. In hindsight I wish I'd had more involvement at this stage.
Fortunately our builder was really savvy and challenged anything that didn't
make sense.

## Doors

See these
[handles](https://www.ironmongerydirect.co.uk/product/morello-chichester-lever-door-handles-on-rose-satin-nickel-648373).

## Build quotes

The most expensive quote was double the cheapest. We went for somewhere
in-between.

## Painting

We received wildly varying quotes for painting, the most expensive was thrice
the cheapest.

## Utility room

Options were a little limited with this room. It was originally part of an
original extension with a low flat roof, consequently the ceiling height after
putting down the floor and insulation is only 2.1m. The online architect had
had combined this room with the kitchen space but having not visited the site
he didn't realise the original external room was between.

## Bathroom

Something not immediately apparent from the plans is that the back of the
property is 600mm higher than the front. In the original plan the bathroom was
on the lower level but the soil pipe was higher up at the back. The builder
offered some solutions with pumps but ultimately the decision was made to
relocate the bathrooms to the back and move the kitchen.

- [Laminate worktop](https://www.diy.com/departments/38mm-kala-matt-grey-concrete-effect-laminate-particle-board-square-edge-kitchen-worktop-l-3000mm/3663602634430_BQ.prd) £98

## Radiators

A mixture of rads from various sources. Refurbished cast iron rads seemed cheap
at £50 each but with sandblasting, lacquer and valves/stays they weren't so
cheap.

## Patio

Went for stone tiles but remember you may need have to treat/seal them before
they are laid.

## References

- [EICR](https://www.propertyinvestmentproject.co.uk/blog/landlords-requirements-for-electrical-safety/)
- [Light bulbs](https://www.edwardsandhope.co.uk/xxl-filament-led-lightbulb)
- [GB Construction](https://www.brighton-construction.co.uk/)
- [Howdens kitchens](https://www.howdens.com/kitchens)
