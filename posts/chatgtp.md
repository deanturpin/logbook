# What is ChatGPT?

According to OpenAI:
> ChatGPT is a natural language processing (NLP) model developed by OpenAI. It is an open-source, end-to-end dialogue model based on the GPT-3 platform. It can generate contextually relevant responses to user questions, as well as produce creative conversations. It is being used for a variety of applications, including customer service, online teaching and learning, and conversational AI.

