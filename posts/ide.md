# IDEs

## Visual Studio
The go-to for Windows development and is pretty slick. Has the awesome "Edit and Continue".

## Visual Studio Code
The free version of Studio and has many of the features of its elder sibling.

## PyCharm
Obviously specialising in Python, essential for any slight serious Python dev.

## Qt Developer
Actually pretty cool despite my aversion to becoming dependent on 3rd-party frameworks.

## Code::Blocks
Linux option which feels pretty bad!

