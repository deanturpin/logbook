# Money guys

> Discipline > Margin of saving money > time

- Deferred gratification
- Avoid debt

1. 60% of Americans cannot come up with $1000 for an emergency
1. 47% of Americans carry a credit card balance
1. New cars lose 50% of their value in the first four years
1. Save 20-25% of gross monthly income (10% is not enough now)
1. Only 25% or Americans work in their field of study

## References

- <https://www.moneyguy.com/2020/12/financial-order-of-operations-course/>
