# Banks

Challenger banks are small, recently created retail banks that compete directly
with the longer-established banks in the country, sometimes by specialising in
areas underserved by the "big four" banks (Barclays, HSBC, Lloyds Banking
Group, and NatWest Group).

See [challenger bank](https://en.wikipedia.org/wiki/Challenger_bank) on Wikipedia.

## Challenger banks
- Aldermore
- Atom Bank
- Metro Bank
- Monzo
- N26
- OakNorth
- Shawbrook Bank
- Starling Bank
- Tandem Bank

## Investment banks
- barclayscapital.com
- db.com
- ml.com
- gs.com
- rbs.com
- morganstanley.com
- citigroup.com
- lloydsbanking.com
- maninvestments.com
- btm.com
- mwam.com
- bluecrestcapital.com
- glgpartners.com

