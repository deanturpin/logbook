# Sources of big data

## Big data communities and platforms
1. http://archive.ics.uci.edu/ml/index.php
1. [Kaggle](https://www.kaggle.com/)
1. [Statista](https://www.statista.com/)
1. [Data World](https://data.world/datasets)
1. [Data Hub](https://datahub.io/collections)
1. [AWS open data](https://registry.opendata.aws/)
1. [Google public data](https://www.google.com/publicdata/directory)

## Crime
1. https://www.fbi.gov/services/cjis/ucr
1. https://www.pcr.uu.se/research/UCDP/
1. https://www.drugabuse.gov/drug-topics/trends-statistics

## Internet
1. https://wiki.dbpedia.org/
1. https://trends.google.com/trends/explore
1. [Reddit datasets](https://www.reddit.com/r/datasets/)

## Government
1. https://www.ukdataservice.ac.uk/
1. https://data.gov.uk/
1. https://data.london.gov.uk/
1. https://www.data.gov/
1. https://opengovernmentdata.org/data/
1. https://www.cia.gov/library/publications/the-world-factbook/
1. https://data.gov.au/
1. https://opendata.cityofnewyork.us/
1. https://open.canada.ca/en/open-data

## House prices
1. [UK house prices](https://www.gov.uk/government/statistical-data-sets/price-paid-data-downloads)

## Mathematics
- I got 717 problems... [Project Euler problems](https://projecteuler.net/archives)

## Health
1. https://healthdata.gov/
1. https://digital.nhs.uk/data-and-information/data-collections-and-data-sets
1. https://www.who.int/data/collections
1. https://www.who.int/gho/maternal_health/reproductive_health/en/
1. http://portals.broadinstitute.org/cgi-bin/cancer/datasets.cgi
1. https://www.cdc.gov/datastatistics/
1. https://www.fda.gov/drugs/drug-approvals-and-databases/drugsfda-data-files
1. https://github.com/publichealthengland/coronavirus-dashboard
1. [1000 Genomes project](https://www.internationalgenome.org/)
1. [USDA food composition](https://data.nal.usda.gov/dataset/composition-foods-raw-processed-prepared-usda-national-nutrient-database-standard-reference-release-27)

## Business
1. https://www.glassdoor.com/research/type/data-sets/
1. https://opencorporates.com/

## Transport
1. https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page

## Climate
1. [Africa climate](http://africaclimate.opendataforafrica.org/)
1. https://openaq.org/
1. [Historic weather](https://www.weather.gov/help-past-weather)
1. [NOAA tides and currents data](https://oceanservice.noaa.gov/facts/find-tides-currents.html)
1. [Global temperatures](https://datahub.io/core/global-temp)
1. [Nasa Earth data](https://search.earthdata.nasa.gov/search?fst0=Atmosphere&fsm0=Atmospheric%20Electricity)
1. [Reeep](http://data.reeep.org/)

## Nature
1. [NYC squirrel census](https://github.com/rfordatascience/tidytuesday/tree/master/data/2019/2019-10-29) - see also [squirrel attacks](https://cybersquirrel1.com/)

## Travel
1. https://www.ustravel.org/research

## Current affairs
1. [Five Thirty Eight](https://data.fivethirtyeight.com/)

## Entertainment
1. [BFI industry data insights](https://www.bfi.org.uk/industry-data-insights)
1. [Spotify developer API](https://developer.spotify.com/documentation/web-api/)

## Sport and gambling
1. [Historical Football Results and Betting Odds Data](http://www.football-data.co.uk/data.php)

## Social media
1. [Facebook API](https://developers.facebook.com/docs/graph-api)
1. [Instagram API](https://developers.facebook.com/docs/instagram-api)
1. [YouTube API](https://developers.google.com/youtube/v3)

## Finance
1. [Google Finance](https://www.google.com/finance)
1. [Cryptocompare API](ttps://min-api.cryptocompare.com/)
1. [XE API](https://www.xe.com/xecurrencydata/) (free trial)
1. https://datahub.io/collections/stock-market-data
1. https://data.imf.org/?sk=388dfa60-1d26-4ade-b505-a05a558d9a42
1. https://atlas.cid.harvard.edu/
1. [World Bank Open Data](https://data.worldbank.org/)
1. [Financial Times markets data](https://markets.ft.com/data/)

## To sort
1. https://www.gapminder.org/data/
1. http://storage.googleapis.com/books/ngrams/books/datasetsv2.html
1. https://github.com/rfordatascience/tidytuesday

## Tools
- https://oxylabs.io
- https://scrapinghub.com
- https://import.io
- https://webscraper.io

## References
- https://www.kdnuggets.com/2017/12/big-data-free-sources.html
- https://www.springboard.com/blog/free-public-data-sets-data-science-project/
- https://www.dataquest.io/blog/free-datasets-for-projects/
- https://www.forbes.com/sites/bernardmarr/2016/02/12/big-data-35-brilliant-and-free-data-sources-for-2016/#4166564fb54d
- https://www.tableau.com/en-gb/learn/articles/free-public-data-sets
- https://piktochart.com/blog/100-data-sets/
- https://github.com/awesomedata/awesome-public-datasets
- https://mode.com/analytics-dispatch/interesting-data-sets/
- https://www.dataquest.io/blog/free-datasets-for-projects/
- https://github.com/BuzzFeedNews/nics-firearm-background-checks
- https://www.springboard.com/blog/free-public-data-sets-data-science-project/
- https://www.kdnuggets.com/2017/12/big-data-free-sources.html
- https://github.com/awesomedata/awesome-public-datasets
- https://github.com/sindresorhus/awesome
