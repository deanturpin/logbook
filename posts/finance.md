# Finance

## Terminology
> The bid–ask spread (also bid–offer or bid/ask and buy/sell in the case of a
> market maker) is the difference between the prices quoted (either by a single
> market maker or in a limit order book) for an immediate sale (ask) and an
> immediate purchase (bid) for stocks, futures contracts, options, or currency
> pairs in some auction scenario. The size of the bid–ask spread in a security
> is one measure of the liquidity of the market and of the size of the
> transaction cost.[1] If the spread is 0 then it is a frictionless asset. 

See [Bid-ask spread](https://en.wikipedia.org/wiki/Bid%E2%80%93ask_spread).

### Me
- BUY
- SELL

### Them
- OFFER
- BID

## HFT
A type of algorithmic trading characterized by high speeds, high turnover
rates, and high order-to-trade ratios. HFT firms make up the low margins with
incredibly high volumes of trades, frequently numbering in the millions.

- Sophisticated algorithms
- Co-location
- Short-term investment horizons
- Sophisticated technological tools and computer algorithms to rapidly trade securities
- Proprietary trading strategies carried out by computers to move in and out of positions in seconds or fractions of a second

## Finance
- DeFi: [Decentralised Finance](https://en.wikipedia.org/wiki/Decentralized_finance)
- Value at risk ([VaR](https://en.wikipedia.org/wiki/Value_at_risk))
- A derivative is a price guarantee
- Buyer/seller (counterparties), underlier, future price, future date,
- Forward, futures, swap, option
- Liquid: more traded on a single day
- Fungible: one is as good as another
- A financial instrument is a standard agreement (contract) that bestows certain financial rights and/or responsibilities to its parties
- [Zero sum games](https://en.wikipedia.org/wiki/Zero-sum_game)
- Hedging, manage uncertainty
- Speculation, wager on uncertainty
- Taking a view: gambling
- Financial leverage
- Market makers, merchants of derivatives arbitrageurs, mispriced security search
- Delta one products are financial derivatives that have no optionality and as such have a delta of (or very close to) one -- meaning that for a given instantaneous move in the price of the underlying asset there is expected to be an identical move in the price of the derivative.
- Deal flow is a term used by finance professionals such as venture capitalists, angel investors, private equity investors and investment bankers to refer to the rate at which they receive business proposals/investment offers

## Securities
See [Wikipedia](https://en.wikipedia.org/wiki/Security_(finance) ).

## There are 3 major blocks in an Algo Trading System
See [details](https://www.quora.com/How-does-an-algorithmic-trading-system-architecture-look-like).

1. Market Data Handler (e.g., FAST handler)
1. Strategy Module (e.g., crossOver strategy)
1. Order Router (e.g., FIX router)

## Order management system
An order management system (OMS) is an electronic system developed to execute
securities orders in an efficient and cost-effective manner. Brokers and
dealers use order management systems when filling orders for various types of
securities and are able to track the progress of each order throughout the
system.

## Fibre versus microwave
- [Microwave networks](https://arstechnica.com/information-technology/2016/11/private-microwave-networks-financial-hft/)
- [Information Transmission Between Chicago and New York](https://arxiv.org/pdf/1302.5966v1.pdf)

## Terminology
### Bloomberg system
- IM between clients
- Most people in investment banking use Bloomberg
- Bloomberg Terminal
- All about derivatives Michael Durbin
- FX (currencies)
- Bloomberg lets you add trendiness etc.
- Banks: sale side (selling product, research) / market makers
- Two way price
- Middle men (like eBay)
- Proprietary trading (ceased because of regulations, own account)
- Many clients on the buy side: invest manager, hedge fund, pension
- Ask banks for prices on each thing

### Sell side
- Economists
- Strategists
- Providing research
- Standard chartered: retail presence (taking deposits from consumers)
- Old school: give a loan against deposit
- IPO
- Issue bond to bunch of investors
- Small investment managers

### Buy side
- Algorithmic trading
- Options pricing model

## References
- https://en.wikipedia.org/wiki/Bid%E2%80%93ask_spread
- https://en.wikipedia.org/wiki/Market_maker
- https://en.wikipedia.org/wiki/Order_book

