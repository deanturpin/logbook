# Bandwidth

See the [complete
list](https://gist.github.com/deanturpin/4197baff46c894d5f12f9ada69e09183#file-bandwidth-csv),
[What Every Programmer Should Know About
Memory](https://akkadia.org/drepper/cpumemory.pdf) and [Grace Hopper explaining
the nanosecond](https://www.youtube.com/embed/9eyFDBPk4Yw).

| Technology | Gigabits/s | Category |
| --- | --- | --- |
| USB 1.1			| 0.012 | Peripheral |
| ADSL2+ 			| 0.024 | Broadband |
| HSPA+				| 0.042 | Mobile |
| Bluetooth 5.0			| 0.05 | Wireless |
| LTE				| 0.173 | Mobile |
| DOCSIS 3.0 (Virgin fibre) 	| 0.216 | Broadband |
| VME64 32-64bit 		| 0.4 | Bus |
| USB 2.0			| 0.48 | Peripheral |
| 1 Gb Ethernet			| 1 | LAN |
| SATA revision 1.0		| 1.5 | Storage |
| FireWire 1600			| 1.573 | Peripheral |
| DOCSIS 3.1			| 2 | Broadband |
| SATA revision 2.0 		| 3 | Storage |
| FireWire 3200			| 3.1457 | Peripheral |
| PCI Express 2.0		| 5 | Bus |
| USB 3.0			| 5 | Peripheral |
| 6G-SDI (SMPTE 2081)		| 5.94 | Peripheral |
| SATA revision 3.0		| 6 | Storage |
| SDHC/SDXC/SDUC (SD Express)	| 7.9 | Storage |
| PCI Express 3.0		| 8 | Bus |
| HDMI 1.3 			| 8.16 | Video |
| Thunderbolt 			| 10 | Peripheral |
| 10 Gb Ethernet		| 10 | LAN |
| USB 3.1			| 10 | Peripheral |
| HDBaseT			| 10.2 | Peripheral |
| DisplayPort 1.2	 	| 21.6 | Video |
| DisplayPort 1.4	 	| 32.4 | Video |
| HDMI 2.1 			| 48 | Video |
| DisplayPort 2.0	 	| 77.36 | Video |

