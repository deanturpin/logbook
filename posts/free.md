---
title: Website hosting
subtitle: On the cheap...
date: 2021-04-20
tags:
- website
- hosting
---

# Website hosting
> tl;dr -- currently using GitLab for hosting and Cloudflare for DNS

## Hugo
So this whole site is generated from markdown using the Hugo blog engine. And
it's hosted on GitLab completely free. See how [I did it](/post/hugo). This
does really need settiing up from a softie, but once it's in place you an edit
and add articles within in the browser.

## 000webhost / Hostinger
Free hosting is available from 000webhost -- albeit with an advert. But you can
go pro for as little as $2 a month.

It did amuse me that I hit two verification issues on the registration page!
> Please use an email address from a reputable email provider (like GMail or Outlook).

> The password may not be greater than 48 characters.

## References
- [Network Chuck](https://www.youtube.com/watch?v=gwUz3E9AW0w)
