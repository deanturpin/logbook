# Electric car resources

Observations and tips, with a focus on my own experiences owning a [Tesla Model Y](https://en.wikipedia.org/wiki/Tesla_Model_Y). There is also a [Tesla performance](https://accelerationtimes.com/models/tesla-model-y-dual-motor-long-range) with a slightly shorter range (but goes like stink).

## Cars I have an opinion on

Because I've either test driven, researched or just like the look of. Ordered in rough order of cost.

| Make/Model | Notes |
|-|-|
| Porsche/Taycan | Big money, looks great |
| Lotus/Electre | Slighty less money, also looks very fancy |
| Jaguar/iPace | Feels more solid than a Tesla, tech a little lacking; feels like a grown up car |
| Tesla/Model X | The biggest of the bunch but they do look quite similar to the uninitiated |
| Tesla/Model Y | Slightly engorged Model 3, more roomy with a proper hatchback |
| Tesla/Model 3 | Handles better than the Y and has a boot |
| VW/iD3 | First EV I drove and almost bought it on the spot, but the tech is not a patch on the Model Y |
| BMW/i3 | Inexplicably no longer in production, popular but I thought the steering was heavy |
| Abarth | Big heritage, that's all I know |
| Peugeot/308 | Nice-looking small car, petrol version is very pleasant to drive |
| Renault/Zoe | Very popular, certainly not a looker |
| Skoda/TBC | Tiny EV |
| Citroen/Ami | Impossibly quite, absolutely filthy to drive, probably alright if you lived on the beach |

## Comparison of charger powers

> DC power = voltage x current

Note the supercharger is DC and uses those two large pins at the bottom of the connector.

| Description  | Power | Current | mph  | Voltage |
|---           |---    |---      |---   |---      |
| Wall plug    | 2.3kW |         | 8    | 240V    |
| Lamppost     | 3.7kW | 16A     | 14   | 240V    |
| Home         | 7.4kW | 32A     | 28   | 240V    |
| 3-phase      | 11kW  |         | 42   |         |
| 3-phase      | 32kW  |         | 42   |         |
| Supercharger | 250kW | 520A    | 648  | 480V    |

See [Tesla Supercharger](https://en.wikipedia.org/wiki/Tesla_Supercharger) and [evbox.com](https://evbox.com/uk-en/electric-cars/tesla/tesla-model-y).

## Connectors

- [CCS2 "Combo 2"](https://en.wikipedia.org/wiki/Combined_Charging_System) (Combined Charging Connector) -- what a design this is!
- IEC Type II -- the normal one you'll see everywhere
- [CHAdeMO](https://en.wikipedia.org/wiki/CHAdeMO) -- weird one that you need an adapter for

## Home chargers

- [Anderson EV: nicely designed](https://andersen-ev.com/)

## Tesla Model Y

- Battery capacity 82 kWh
- Driving range 336 miles (this has been downgraded in 2024)
- Efficiency 271 Wh/miles

| Model | Range | Efficiency | Battery capacity | Power per mile |
|---    |---    |---         | ---              | ---            |
| Model Y RWD | 303 miles | 250 Wh/miles | 75 kWh | 250 W/mile |
| Model Y AWD LR | 336 miles | 271 Wh/miles | 82 kWh | 271 W/mile |
| Model Y AWD Performance | 303 miles | 271 Wh/miles | 82 kWh | 271 W/mile |

82000 / 271 = 302.5 miles

The Y is a big car. With an equally impressively large turning circle too! (I don't know if this is a general torquey EV thing: i.e., not having sloppy steering lest you end up in a hedge.) You have to be patient parking them too; have a close look at the alloy wheels when you pass one next. (I have only scuffed one in a year but that was when I was under duress.)

## Third party

- [Tessories](https://tessories.uk/)
- [Tesery](https://www.tesery.com/)
- [Tesla shop](https://shop.tesla.com/)
- [EV Accessories](https://evaccessories.co.uk/)

## Camping in the Tesla

- [Camping Privacy Curtains Especially Designed for Tesla Model Y](https://www.amazon.co.uk/TESBEAUTY-Curtains-Sunshade-Upgraded-Distinctive/dp/B0B1WZJWR8/ref=dp_fod_sccl_3/257-3695261-1079115?pd_rd_w=Sd8zS&content-id=amzn1.sym.fd0b82fb-e9ae-42a5-94a3-41ec499cc326&pf_rd_p=fd0b82fb-e9ae-42a5-94a3-41ec499cc326&pf_rd_r=7T983VGM031Z0895DENM&pd_rd_wg=92lAv&pd_rd_r=d5faa8d3-0415-43f4-8271-1c244ee65fba&pd_rd_i=B0B1WZJWR8&th=1)
- [LOSTHORIZON 4.5”Thick Self Inflating Sleeping Pad for Tesla Model Y](https://www.amazon.co.uk/dp/B0BL7VMHKY?ref_=cm_sw_r_apan_dp_4TWBNWEJ5E2K3JEA9BM8)

## Tariffs

- https://lovemyev.com/explore/ev-tariffs/charge-anytime-vs-octopus-intelligent

## Software developers

- [Unoffical Tesla API](https://www.teslaapi.io/vehicles/commands)

## Solar panels

- https://www.tesla.com/referral/jeremiah22519

## Repairs

- https://revive-uk.com/territory/brighton/alloy-wheel-repair/

## Audio

- [Focal](https://www.focal.com/en/t3y-new-tesla-compatible-kits)

## Protection

- PPF
- Ceramic polish
- <https://www.amazon.co.uk/AlloyGator-Replacement-Profile-Protectors-replacement/>

## Return to an ICE

Observations after driving an electric car for a while. This is comparing a 2023 with a 2022 Mercedes petrol.

### Cars

- Tesla Model Y long range 2023
- Mercedes C 200 SPORT MHEV 2022 (petrol)
- Mercedes CLC 220 2008 (diesel)

| Feature | Tesla | Mercedes |
|---      |---    |---       |
| Acceleration | Instance torque in an EV | Unresponsive but sounds nice |
| Braking | Regen is great | Brakes are still really sharp after two years |
| "don't pull out now!" warning | On screen in peripheral vision | A little primative, but very effective! Especially when you reversing into a road |
| Turning | Quite large | Tighter |
| Steering | Medium | Very light |
| Reversing camera | Good | Awful super wide angle |
| Navigation | Good | Very cluttered |

