# Boost
You have to be wary of using third-party libraries, especially when there's so
much good stuff in the core library! But if you must, consider constraining it
to a subsystem rather than letting it creep throughout the codebase.

- Boost.Asio
- Boost.Signals2
- Boost.Thread
- Boost.Test

- [boost.org](https://www.boost.org/).
- [Should I become proficient in STL before learning
  Boost?](https://stackoverflow.com/questions/548751/should-i-become-proficient-with-stl-libraries-before-learning-boost-alternatives)
- [Comparing C++ and
  Boost](https://insights.dice.com/2013/03/15/comparing-the-c-standard-and-boost-2/)

