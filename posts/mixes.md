# Publishing mixes

| Platform | Free |
|-|-|
| Vimeo | |
| Mixcloud  | 10 uploads, no HQ |
| YouTube | Copyrighted material not allowed |

## Publishing

- 10 mins to bounce a 90 minute mix
- 5 mins to upload (1GB file) to MixCloud, 5 mins to "process" before it can be published
- See [upload rules](https://help.mixcloud.com/hc/en-us/articles/360004031080-What-are-the-Featured-Artist-Rules-and-why-is-my-upload-unavailable-for-copyright-reasons)

## Resources

- [The Reel World Scoring for Pictures, Television, and Video Games, Third Edition Jeff Rona - Foreword by Peter Golub](https://rowman.com/ISBN/9781538137758/The-Reel-World-Scoring-for-Pictures-Television-and-Video-Games-Third-Edition)
- [Soundcloud mixes](https://checkout.soundcloud.com/artist?ref=t100)


