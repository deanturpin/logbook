---
title: Rolex
---

# Rolex

## Precision
- ref 6426
- cal 1210
- Date of issue: c.1969
- Serial number example: 2615***
- Inside case back: MONTRES ROLEX S.A. 6426
- Dial: ROLEX / OYSTER / Precision / - T SWISS T - 
- https://www.vintagegoldwatches.com/product/rolex-precision-18ct-1950/

### Examples
- https://www.blackbough.co.uk/product/rolex-oyster-royal-precision-ref-6426-steel-vintage-wristwatch-dated-1962-wwropsg1/
- https://www.ebay.co.uk/itm/265628592342

## Daytona
> The Rolex Cosmograph Daytona is a mechanical chronograph wristwatch designed to meet the needs of racing drivers by measuring elapsed time and calculating average speed. Its name refers to Daytona, Florida, where racing flourished in the early 20th century. It has been manufactured by Rolex since 1963 in three distinct generations (or series); the second series was introduced in 1988, and the third in 2000. While cosmetically similar, the second series introduced a self-winding movement (the first series is hand-wound), and the third series brought manufacture of the movement in-house to Rolex. 

https://en.wikipedia.org/wiki/Rolex_Daytona

## Sky Dweller
## Day-Date
## Datejust
## GMT Master II
> The Rolex Oyster Perpetual Date GMT Master is part of the Rolex Professional Watch Collection. Designed in collaboration with Pan American Airways for use by their pilots and navigators, it was launched in 1954.

https://en.wikipedia.org/wiki/Rolex_GMT_Master_II

## Milgauss
> The Rolex Oyster Perpetual Milgauss is a wristwatch model introduced by Rolex in 1956 with model number 6541. The Milgauss was advertised as "designed to meet the demands of the scientific community working around electromagnetic fields". 

https://en.wikipedia.org/wiki/Rolex_Milgauss

## Submariner
> The Rolex Oyster Perpetual Submariner is a line of sports watches designed for diving and manufactured by Rolex, and are known for their resistance to water and corrosion.[1] The first Submariner was introduced to the public in 1954 at the Basel Watch Fair.[2] It was the first watch to be waterproof up to 100m.[3] The Rolex Submariner is considered "a classic among wristwatches",[1] manufactured by one of the most widely recognized luxury brands in the world.[4][5][6][7] Due to its huge popularity, there are many homage watches by well-established watchmakers, as well as illegal counterfeits. The Rolex Submariner is part of Rolex's Oyster Perpetual line.

> The Rolex Sea-Dweller, developed in 1967 but introduced to the general public in 1971, is a heavier-duty steel version of the Submariner, with a thicker case and crystal, as well as a date feature, sans cyclops magnifier.

https://en.wikipedia.org/wiki/Rolex_Submariner

## Sea Dweller
> The Rolex Oyster Perpetual Date Sea-Dweller is a line of diver's watches manufactured by Rolex, with an underwater diving depth rating of 1,220 meters (4 000 ft) and up to 3,900 metres (12,800 ft) for the Sea-Dweller Deepsea model. Launched in 1967 with a diving depth of 610 metres (2 000 ft), the Sea-Dweller features a gas escape valve, developed by the brand specifically for watches, which allows the helium trapped in the watch while decompressing to be released at a given pressure during decompression, while preserving the watch case's water resistance. Today's Sea-Dweller models are available in steel or steel and yellow gold, and have a 43 mm case.

https://en.wikipedia.org/wiki/Rolex_Sea_Dweller

## Yacht-Master
> The Rolex Yacht-Master is a luxury sports watch manufactured by Rolex and first introduced in 1992 as Reference 16628 in 18-karat yellow gold.

> In 2007, the brand released the Rolex Yacht-Master II regatta chronograph watch, the world's first watch equipped with a programmable countdown from 1 to 10 minutes using a mechanical memory.

https://en.wikipedia.org/wiki/Rolex_Yacht-Master

## References
- https://www.blowers-jewellers.co.uk/cheapest-rolex/
- https://expertswatches.com/pages/rolex-marconi-watches

