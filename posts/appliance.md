# Home appliance energy usage

Charges for Brighton on 12/01/22. See [Octopus energy rates](https://octopus.energy/go/rates/) for your postcode.

- Unit rate (04:30 - 00:30): 43.40p / kWh
- Unit rate (00:30 - 04:30): 12.00p / kWh
- Standing Charge: 41.13p / day

It's usual to hear "every penny counts"... but it doesn't!

If you make sure to turn off your phone charger when you're not using it, you simply won't notice any different in your bills if you're also running a kW electric fire. So we can take some advice from software development: focus your attention on the big problems first.

E.g., if your oven is the biggest consumer in your household, how can you avoid using it? Is an air fryer cheaper to run?

| Appliance | Power (kWh) | Duration (s) | Cost |
| --- | --- | --- | --- |
| Kettle, 0.5 litre of room temp water (nominally 3 kW) | 2.858 | 73 | 2.52p |
| Tefal Actifry 2 in 1 air fryer (nominally 1.4 kW) | 1.37 | 600 | 9.91p |
