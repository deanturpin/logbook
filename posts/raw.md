---
title: Shooting raw images on the Huawei P30 Pro
subtitle: Observations and workflow
date: 2020-01-22
tags:
- photography
- raw
- mobile
- huawei
- p30pro
---

# Shooting raw images on the Huawei P30 Pro

# Prerequisites
- Google Photos for Android
- PhotoShop for Android
- Lightroom (free) for Android
- Launch the camera in Pro mode and update your settings to 40MP and raw. Now when you take a shot you'll get the normal JPEG but also the raw file (DNG) that the camera used to generate the image.

# Workflow
- Take some photos in Pro camera mode.
- Overexpose by taking manual control of the shutter and slowing it down
- Open Google Photos and you'll see the phone has also generated some JPEGs alongside. Multi-select and delete them so we don't get distracted.

# Sensors
## 39.7 MP
- Technology CMOS BSI
- 7296x5472
- IMX650 Wide (f/1.6, 1/1.7", 27mm, OIS) []
- 7.3 mm x 5.47 mm, Diagonal: 0.46 inch
- Pixel size 1 μm
- "SuperSpectrum" image sensor
- RYYB

## 19.5 MP Ultrawide
- 5104x3824
- f/2.2, 1/2.7", 16mm

## 7.9 MP Telephoto
- 3248×2432
- f/3.4, 1/4", 125mm
- folded optics
- OIS

# Sensor equivalency
- What does 27mm mean?

# Screen resolution
- 19.5:9
- 1080x2340

# Processing
- cta-core (2x 2.6 GHz Cortex A76, 2x 1.92 GHz Cortex A76, 4x 1.8 GHz Cortex A55)
- GPU Mali-G76 MP10

# Generating HDR images
- You can use the HDR mode but you can't access the raw data
- Export as 16-bit TIF in Lightroom

## References
- [Wikipedia](https://en.wikipedia.org/wiki/Huawei_P30)
- https://www.deviceranks.com/en/camera-sensor/686/sony-imx650-exmor-rs
- https://en.wikipedia.org/wiki/Exmor
- https://frankdoorhof.com/web/2019/04/solving-the-p30-pro-color-problems/

