# Sight reading

Resources for learning to read music properly.


## Clefs

Whilst at first glance the bass and treble clef are the same, why would they be? The bass clef is shifted down by a third.

And where is middle-C? Not in the middle of either!

![](https://2.bp.blogspot.com/-CpSYO-y9WKA/UPdrMBhjMEI/AAAAAAAAACI/CIJDJ2jYoHY/s1600/Screen+Shot+2013-01-16+at+9.08.17+PM.png)

## Apps

TBC

## Chord wheel

TBC