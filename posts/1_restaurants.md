# Restaurants

| Key | Price per head with refreshments |
| --- | --- |
| £ | £15 |
| ££ | £30 |
| £££ | £60 |
| ££££ | £120 |
| £££££ | £240 |

## Would like to visit

- pearly cow
- el bullilo
- voya

## 2025

- The Coalshed (££): lunch at the new location on North Street; they were overrun so service was a little slow but a had a decent steak (even on the set menu)
- The Barley Mow, Polegate (£££): first restaurant of the year! A decent burger and the lambiest roast lamb I've eaten for a while

## 2024

### December

- The Set (££££): NYE brunch
- The Windmill (££): Really solid sirloin steak sharing dish for Sunday roast. Slightly contentious pigs in blankets for some but I loved them!
- The Set (££££): super consistent fine dining; they've been around a long time... best set menu in Brighton?!

### October

- Dilsk @ Drakes (££££): fine dining on the prom... but is it as good as Furna?! (Not this time but still very nice.)
- Solera (££): went here years ago and pleased that it's still there! Solid tapas but you have to go somewhere else for dessert
- [Naninella](https://www.nanninellapizzeria.co.uk/) (££): first time back since they reopened! Best pizza in Brighton?

### August

- Sol i LLuna, Gandia, Spain (££): very chilled paella on the beach
- L'Estibador, Valenica, Spain (£££): finer dining on yet another lovely beach

### July

- La Taska Sidrería, Valencia, Spain: very nice resto in town, with fancy cider pump on the table if you like that sort of thing
- Begin: Jungla, Valenica, Spain (£££): stylish and remarkably good value

### June

- No No Please, Brighton (££): second time here and they hadn't run out of beef stew, delicious!

### May

- Wagamama (£££): super-consistent chain with good cocktails
- Burger bros (£): still the best in Brighton

### April

- Rustico, Brighton (££): my favourite pizza restaurant at the moment! Great Neapolitan food
- Permit Room, Brighton (££): we finally get our own one! Pretty much the same as London but right in the middle of town
- No No Please, Brighton (££): the hotest new restaurant on Preston Street; bring your spicy trousers, this is going to be _busy_ in summer

### March

- Ola, Bilbao (££££): Michelin-starred food in a really curious building (never got to the bottom of what it was)
- Mina, Bilbao (££££): outstanding Michelin-starred Basque cuisine by the river
- Adador Sagarra, Bilbao (£££): cracking steak and wine in the old town
- Restaurante Aura, Bilbao (££):
- Zaharra Plaza nueva, Bilbao (££): very good pintxos
- El Callejon de Cervantes, Bilbao (£): three course lunch on an industrial estate, if you've ended up in this end of town you are far from the tourist trail!
- Côte, Brighton (££): solid French chain

### February

- Bonsai Plant Kitchen, Brighton (£££): very stylish Japanese skewers, and you would not know it was vegan
- Etci, Brighton (£££): pronounced: "et-chi", best kebab in Brighton?! Posh sit-down affair, very tasty and you won't go home hungry

### January

- SIX, Hove (££): slick brunch and dinner in a cool bit of Hove, pooch friendly too
- Carne, Hove (£££): solid meat specialist in Hove

## 2023

### December

- Burnt Orange, Brighton (£££): excellent sharing plates with a Middle Eastern twist
- The New Club, Brighton (££): top notch Sunday roasts by the seaside

### November

- Plateau (£££), Brighton: back to form for this French sharing plates restaurant in Brighton
- Zoilo, London (£££): perfectly good steakhouse near Selfridges
- Maison Bab, London (££): gyros style kebabs, quick and tasty.
- The Clove Club, London (£££££): two-star in Michelin in Shoreditch... I had to add another pound to my rating system for this one!
- Dishoom (King's Cross), London (£££): very slick Indian chain; quick and tasty

### October

- Etch, Brighton (££££): tough call between Etch and Furna, but Etch just pips the new boy
- [Furna](https://furnarestaurant.co.uk/), Brighton (££££): the newest fine dining in Brighton
- Wild Flor, Hove (££)
- Bincho, Brighton (££): it's always hard to get in here, defo book
- [64 Degrees](https://64degrees.co.uk/), Brighton (££££): fine dining by acclaimed chef Michael Bremner

### September

No dining? Wait... what?!

### August

- Rocco, Lisbon (£££): a very slick Italian, styled much like The Ivy in the UK
- Bar da Praia, Odeceixe (££): great spot overlooking the river (was getting slightly nippy after sunset), very prompt service and some cheeky pinchos: namely the Gilda, of which I could have eaten many

## 2021

- Goemon (ramen)
- Aji Sushi - 27/09/21
- [Halisco](https://www.facebook.com/HaliscoRestaurant/) (Mexican) - 21/09/21
- atTEN (small plates) - 22/09/21
- [Terre à Terre](https://terreaterre.co.uk/) (long-running vegetarian restaurant) - 23/09/21
- Burnt Orange (small plates) - 27/08/21
- Coalshed (steaks) - 02/09/21
- Saltroom - 25/12/21
- The Set - 18/01/22
- [Naninella](https://www.nanninellapizzeria.co.uk/) (Neapolitan Pizzeria)
- [Etch](https://www.etchfood.co.uk/) (fine dining by Masterchef winner)
- Bincho (Japanese Yakitori skewers)
- The Market
- The Ivy
- Little Fish Market
- [Block](https://blockbar.co.uk)
- Murmur (seafront fine dining, 64 Degrees' sister restaurant)
- Lucky Khao (Thai)
- Shelter Hall
- Cyan (main restaurant in The Grand)
