# What's the best programming language?

- [Tiobe Index](https://www.tiobe.com/tiobe-index/)
- [9 tech skills that will land you a salary of $125,000 or more](https://www.businessinsider.com/highest-paying-programming-languages-stack-overflow-developer-survey-2020-5?r=US&IR=T)
- [The 10 most dreaded programming languages](https://www.businessinsider.com/stack-overflow-developer-survey-10-most-dreaded-programming-languages-2020-5)
- [The 14 most loved programming languages](https://www.businessinsider.com/stack-overflow-developer-survey-14-most-loved-programming-languages-2020-5)
