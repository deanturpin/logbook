# Soundtracks

Editing soundtracks in Logic, all examples with a two hour movie.

## File formats

So Logic is a bit picky about the video format it will import.

- Original mkv rip: 1.18 GB
- DaVinci Resolve transcode: 5.62GB
- Logic project with original audio muted and stems split: 11.86GB

## Vocal isolation

Stem splitter takes a couple of minutes.

