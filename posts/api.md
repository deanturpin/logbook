# APIs

## Property

- https://developer.zoopla.co.uk/

## Misc

- https://aws.amazon.com/data-exchange/why-aws-data-exchange/apis/
- https://www.gov.uk/government/statistical-data-sets/price-paid-data-downloads

## Finance

- [ProfitView](https://profitview.net/)
- [Interactive Brokers](https://www.interactivebrokers.com/)
- [Alpaca](https://alpaca.markets/)
- [Onanda](https://www.oanda.com/)
- [Quandl](https://www.quandl.com/)
- [Tiingo](https://www.tiingo.com/)
- [Xignite](https://www.xignite.com/)
- [Alpha Vantage](https://www.alphavantage.co/)
- [Barchart](https://www.barchart.com/)
- [EOD Historical Data](https://eodhistoricaldata.com/)
- [Finnhub](https://finnhub.io/)
- [IEX Cloud](https://iexcloud.io/)
- [Polygon](https://polygon.io/)
- [Tradier](https://tradier.com/)
- [Yahoo Finance](https://finance.yahoo.com/)
