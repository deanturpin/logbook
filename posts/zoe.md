# Zoe

Diary of my Zoe tests and experiences. Started 10 Feb 2024.

## The night before

I forgot to get started until 10pm and then had to apply the glucose sensor. But it didn't seem to matter: just just need 8 hours of fasting before you eat the cookies. There's a slight disjunct between the two apps: Zoe and Libre; and you seem to be presented with the same kind of instructions but in a different format. Why does Zoe not integrate it?! Anyway...

So, after fannying around with the sensor and the app, I attached it straight on my non-dominant bingo wing; it then counts down an hour to "calibrate" and then you can take your first reading. And so to bed.

## First day

The breakfast cookies were alright! Don't know what the fuss is about. Get them down in nine minutes, no problem. Also did the poop test. And there was no shortage of "sample data" in the supplied scat hammock. And now to fast for four hours until my tasty blue lunch cookies. Black coffee is OK.

The blue cookies were also fine! A different consistency, perhaps, but I suppose it could be testing some other form of glucose.

I thought I would be logging food in the app straight away but the little plus sign is not available until the third day. Slightly annoying that the sensor info grumbles about visiting the pool and sauna, as I have just rediscovered both.

On final thing for UK testers: if you start on a Friday you won't actually be able to post it until Monday morning. The documentation says there might be a problem after 72-hours so I've just got to remember to post it.

## Second day

The blue (cookie) turtle reared it's head at 11:30am. Well, it wasn't exactly blue but I'm taking that as a sign. You have to start logging your food from today, but the app refused to let me! Turned out you had to go back and complete all the induction exercises first.

Had a couple of beers and a Twix and saw the glucose shoot up to almost the top of the green range. Hopefully that was the Twix.