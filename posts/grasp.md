---
title: GRASP
subtitle: General Responsibility Assignment Software Principles
date: 2021-09-15
tags:
- design
---

# GRASP

1. Information expert: put the responsibility of handling inputs on the class that owns the inputs (coupling, encapsulation)
1. Creator
1. Low coupling
1. Protected variations
1. Indirection: compare with Adapter pattern
1. Polymorphism: compare with Strategy pattern
1. High cohesion: focus classes around one responsibility; compare with *S*OLID
1. Pure fabrication: compare with Facade pattern
1. Controller: an example of previous

## References
- [SOLID](/post/solid)
- https://www.fluentcpp.com/2021/06/23/grasp-9-must-know-design-principles-for-code/

