# Training and outside interests

- __Training__: Presentation skills (2024), Introduction to Management (2022), cpponsea conference (2024)
- __Interests__: Music production, mechanical watch restoration
- __Language__: English (native), French (provenance), Gujarati (extended family)

