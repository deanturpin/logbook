# Restaurant reviews

## Rocco, Lisbon, Portugal (August 2023)

A very slick Italian, styled much like The Ivy in the UK. Of course there was the standard confusion with the starters (which I have come to accept is me) but overall it was pretty solid. We did, however, have have massive fomo after seeing the seemingly outrageously expensive "mushroom pasta" be served behind us. The pasta was finished by swished around _in_ a hollowed out Parmesan round, and topped with a very generous portion of shaved dark truffle.

Back to our food: the arancini were excellent, the bife Rossini steak was pretty good (and rare as requested). But that mushroom pasta will haunt me forever. Or until I return and try it for myself.

Almost worth going for the epic bathroom (much like The Ivy), I just can't decide if it's bad form to have our final Portuguese dinner at an Italian.

## TBC, Evora, Portugal (August 2023)

A great looking place and the chef was hyped up enough to expect good things were coming. But I was quickly told off for putting my plate on the menu (I'm 47) and it only went downhill from there.

So, the amuse bouche (or equivalent in Portuguese) was a tasty cube of flan served on what appeared to be a slightly abused bit of rocket. My antagonist came over to ask if everything was OK, and we were told that's what the dressing does to rocket. Alrighty, let's move on.

We ordered three starters: the prawns were pretty good if a little standard; the "sausage" was apparently _finely-chopped_ sausage, hidden in a round of egg-fried rice with a slice of toast on the side... but what I really wanted was a lovely sausage; and the mushroom dish consisted of five very salty field mushrooms (I ate three but gave up to save my palate.)

We continued with pork cheeks and rack of lamb for mains. The pork was an absolutely enormous burger-shaped puck of what appeared to be shredded cheeks, and I think they had starting cooking the chops the day before as they had absolutely destroyed them: lambageddon. I left one as a protest (can you imagine leaving a lamb chop?) and was asked if it wasn't well done enough. Well done enough?! It was served with the ribs clawing at more rice with a scoop of mint ice cream on the top. Funnily enough it was the best ice cream I'd had in Portugal, but did it belong with overcooked meat? Well I couldn't get into it and I am an advocate of chaud-froix combos.

So we got out of there after that: you can keep your desserts. However, the gin and tonic was by far the most interesting part of the dinner. It was served without a garnish, the flavouring being added by smoking the glass itself. Novel, striking and tasted great.

We rounded off the evening with a disappointing slice of chocolate cake in the street and went to a bar instead.

## TBC2, Evora, Portugal (August 2023)

Booked a fairly upmarket dinner in a lovely courtyard next to the ceramics museum (which is itself worth a visit.)

We had a nice selection of glassy charcuterie to start (including one regional specialty which I must find again.) And some of their own bread and locally-sourced olive oil, which I don't think I'm sophisticated enough to properly appreciate.

They were a bit unlucky as a week previously an electric fault had enfeebled their electricity supply, periodically knocking the power out, turning off the music and apparently halting the supply to the kitchen too.

Equally unlucky for us was that one couple had brought their child with them: OK, children need to eat too... but also his bike... a bike! To a fine-dining restaurant. Whilst he was merrily lapping the courtyard -- occasionally pit-stopping at each table to holla "laa laa la laa" at each group of diners -- the parents remained unphased. "At least he's not bothering us for a change," they were probably thinking. But thankfully they left before everybody else did.

Additionally, the triple-cooked chips were really bigged up by the waiter but arrived a little soggy and disappointing.

I would like to revisit this place as it ticks all the boxes for provenance and passion, but this time was marred by electrical faults and an over-tired amateur cyclist.

## TBC3, Evora, Portugal (August 2023)

We had lunch after wandering around a little upon arrival. Very honest food: sardines cooked to order and really wholesome pork cheeks with chipped potatoes.

## Bar da Praia, Odeceixe, Portugal (August 2023)

Great spot overlooking the river (was getting slightly nippy after sunset), very prompt service and some cheeky pinchos: namely the Gilda, of which I could have eaten many.

OMG, and the potatoes. All future spuds will be held up these. They were beautiful waxy numbers, cooked to perfection and served with a red pepper dip (there was actually a second green dip but I can't remember what it was).

We also had tuna tartar and chicken and rice. Great food and friendly host.

## Enigma hotel, Odeceixe, Portugal (August 2023)

We discussed the negative press after the fire with our previous host, Thomas; and he said we should think of it as an opportunity to see what has happened after the fire, and also to support the locals who were clearly on their arse after such a widespread and devastating natural disaster.

Driving around Alentejo really is a pleasure. But as we approached, it was really quite shocking: the fire burnt all the way up to the hotel and pretty much as far as you could see around it. After sunset there was an eery silence, I'm guess because most of the animals and insects had perished.

## TBC, Vila Nova de Milfontes, Portugal (August 2023)

This was a few doors down from the first and it took over an hour before a friendly Mexican guy on the next table queried our order. And it had been forgotten. But no matter, it would be prepared asap. My pork was pretty good but the octopus was hard like a rock and was returned and refunded as it was so late. Fortunately the salad and one of my pork steaks made an OK dinner.

## TBC2, Vila Nova de Milfontes, Portugal (August 2023)

Found a really lovely guest house, notable by the hosts' impeccable taste in decor and breakfasts: a brother-sister duo who converted it to a five-bed with great courtyard for breakfasts. And also an honesty bar with beers and a curated wine selection.

So, to the evening: we tried to walk in at 8:30pm but was offered a table at 9:15pm, so we decided to walk around a little and get a drink. Impossible to have a drink without a meal! Finally got a beer out of a plastic cup out of what appeared to be a festival and returned to the restaurant. There was some confusion with starters and we received five (for the two of us)! With trepidation we awaited the steaks for main, wondering where we would stuff them after all the starters. And we waited. And the neighbours got theirs. And also their desserts.

So we're not entirely sure what happened but by fluke the mains weren't put through the till and we skipped out of there pronto.

## TBC, Comporta, Portugal (August 2023)

I had the pork secret: a Portuguese staple. Slick but a little underwhelming.

## TBC, Ericeira, Portugal (August 2023)

TBC

## TBC2, Ericeira, Portugal (August 2023)

TBC
