---
title: Southern Vietnamese
subtitle: Resources and vocabulary
date: 2020-02-22
tags:
- language
- vietnam
- learn
- translate
---

# Southern Vietnamese

## Slang
- chời ơi - Oh man
- biết chết liền - know die immediately
- hên xui - bad luck (said when dealing with uncertainty: you don't know if I did well in that exam)
- bó tay - give up (I/you give up, lost cause)
- cũng thường thôi - it's OK

## Magic words
### Buồn
- Sad
- Ticklish
- Feel like [doing] something

## Feel like...
### cười - to laugh
- buồn cười - funny

### ngủ - to sleep
- buồn ngủ - sleepy

### nôn - to be sick
- buồn nôn - nauseous

### tè - to pee
- buồn tè - bursting (for a pee)

### ị - to poop
- buồn ị - bursting (for a poop)

## Single letter words
See [TVO](https://www.youtube.com/watch?v=UXBnJE4L0YE)

- à - right? (question tag)
- ạ - sir (and a sentence with ạ to show respect)
- é - left on the shelf
- ị - to poop
- ở - at (preposition), to stay
- ô - unbrella
- ố - stained
- ổ - nest
- ợ - to burp
- ừ - yes (to younger people)
- ủ - to ferment
- u - tumour/lump
- ý - Italy/opinion

## Food and drink
- rượu - wine
- rượu trắng - white wine
- bia - beer
- hai cốc bia - two glasses of beer
- hai chai bia - two bottles of beer

## Phrases
- Hẹn gặp lại (nhé) - see you later
- xin lỗi, cho tôi xin một bát phở - excuse me, one bowl of pho please

## Questions
- nào - which (goes after noun)
- phòng nào - which room

### Tag questions
- Bạn là một giáo viên, phải không?
- Bạn là một giáo viên, đúng không?
- Bạn là một giáo viên, à?

### Answer
- đúng, phải - correct
- vâng - yes (to older people)
- ừ - yes (to same age or younger)

## Directions
- Ở đâu – at where
- Đâu – where
- bia của tôi đâu – where is my beer?
- trái/phải - left/right

## Classifiers
### Animals - con
- con chó - dog
- con mèo – cat
- con bò – cow
- con gà – chicken
- con lợn - pig
- exceptions: knife, river

### Fruits - quả
- quả táo - apple
- trái xoài – mango
- trái chuối – banana
- quả chanh – lime
- trái bóng – ball (also used for some round objects)

### Objects
- cái bàn – table
- cái xe máy – motorbike
- cái áo – shirt
- cái điện thoại
- cái vé - ticket
- exceptions: tree

Use when there’s a specific number or object.

- tôi có một con chó – I have a dog
- Tôi bị dị ứng với mèo – I am allergic to cats (no classifier)
- cái: used for most inanimate objects
- chiếc: almost similar to cái, usually more connotative (e.g. when referring to a cute object, chiếc might be more suitable than cái)
- con: usually for animals and children, but can be used to describe some non-living objects that are associated with motion
- bài: used for compositions like songs, drawings, poems, essays, etc.
- câu: sentential constructs (verses, lyrics, statements, quotes, etc.)
- cây: used for stick-like objects (plants, guns, canes, etc.)
- chuyện: a general topic, matter, or business
- lá: smaller sheets of paper (letters, playing cards)
- tòa: buildings of authority: courts, halls, "ivory towers".
- quả/trái: used for globular objects (the Earth, fruits)
- quyển/cuốn: used for book-like objects (books, journals, etc.)
- tờ: sheets and other thin objects made of paper (newspapers, papers, calendars, etc.)
- việc: an event or an ongoing process

https://en.wikipedia.org/wiki/Vietnamese_grammar

## Peanut
- dầu phộng (south)
- củ lạc (north)
- Tôi không có đậu phộng – I not have peanuts

## Numbers
1. một
1. hai
1. ba
1. bốn
1. năm
1. sáu
1. bảy
1. tám
1. chín
1. mười
1. mười một
1. mười hai
1. mười ba
1. mười bốn
1. mười lăm
1. mười sáu
1. mười bảy
1. mười tám
1. mười chín
1. mười hai

## Duration
- bao lâu – how long
- trong bao lâu – in how long
- bao lâu rồi – how long already

## Colours màu
https://www.omniglot.com/language/colours/vietnamese.php

## Number shortcuts
- hai nghìn rưỡi – two and a half thousand

## How much/many
- bao nhiêu tiền - how much money
- mấy - (how) few
- đáng tiền – worth the money
- quá versus rất – too verus very
- cho versus để - for versus to

## Things
- Đồ thing
- đồ ăn – food
- đồ uống – drink
- đồ bơi – swimsuit
- đồ chơi – toy

## Pronouns
https://en.wikipedia.org/wiki/Vietnamese_pronouns

- anh / chị - older brother/sister
- em – younger brother/sister
- cháu – niece/nephew

## Greetings
https://www.youtube.com/watch?v=mkuR1-UMTxQ

- tạm biệt – goodbye
- Đi nhé! – let’s go!
- Chào – hi
- Xin Chào
- mình tên là... - my name is...
- còn bạn thì sao - how about you
- rất vui được gặp bạn - nice to meet you
- chào bạn - hello friend (common amongst young people)

## Vocab
- trời ấm – it is warm
- trời lạnh – it is cold
- nó nóng – it is hot
- Trời đang có gió – it is windy
- tôi đói – I am hungry
- tôi khát – I am thirsty
- tên tôi là – my name is
- chào các bạn – hello friends
- táo – apple
- xe máy – motorbike
- ngày – day
- Ở đâu – where
- Bia – beer
- nhiều – many
- hoa – flower
- khỏe – healthy
- với – with
- Nước – water
- Đũa – chopsticks
- Sữa – milk
- vui - happy
- túi – bag
- Muối – salt
- người – person
- luôn luôn – always

## Questions
- bao nhiêu – how much?

## Verbs
- nói – speak
- muốn – want
- uống – drink
- mua – buy
- hỏi – ask
- biết – to know
- hiểu – to understand


## Vietnamese language resources - IPA
Just the tricky consonants from Wikipedia:IPA for Vietnamese.

### Implosive
- ɓ Voiced bilabial implosive bạn you
- ɗ Voiced alveolar implosive đuôi tail

### Nasal
- ɲ Palatal nasal nhà house
- ŋ Velar nasal ngà ivory "... the sound of ng in English sing."

### Retroflex
- ʂ Voiceless retroflex sibilant (southern dialects) sữa milk
- ʈ͡ʂ Voiceless retroflex affricate

### Fricative
- ɣ Voiced velar fricative ghế chair
- x Voiceless velar fricative không not

### Sibilant
- ʐ Voiced retroflex sibilant rô diamond

## References
- [Vietnamese typography](https://vietnamesetypography.com/diacritical-details/)
- [Vietnamese phonology](https://en.wikipedia.org/wiki/Vietnamese_phonology) - see [comparison of initials](https://en.wikipedia.org/wiki/Vietnamese_phonology#Comparison_of_initials)
