# The lighthouse keeper's son

> John James Turpin's memories of growing up in occupied Jersey at [La Corbière
> lighthouse](https://en.wikipedia.org/wiki/La_Corbi%C3%A8re). Born in 1930.

---

# Chapter 1

My father was a full time naval man. Having served before the First World War
up until the late twenties, he joined the lighthouse service. He was then in a
position to bring his family to the lighthouse cottage which consisted of my
mother, my two brothers, my sister and of course myself. Our furniture was
transported in the usual way by open lorry. My parents, I am told, were praying
for fine weather, so as to keep the family possessions from the rain that was
forecast for that October day in 1930. We duly arrived, I am told, all in one
piece at the cottage that was to play such an important part in the years to
follow my mother pushed my pram past the first cottage. The lady we become to
know as Mrs. Lolly looked into the pram and said “will it live?” I must have
looked a sorry site, my legs were in bad shape and my feet were both facing
inwards. That was the way they were until the engineer in charge of installing
the engine room and diaphone house saw me, and said he knew an osteopath that
could do something for me. He, being a man of his word, duly brought his
osteopath friend along to the cottage and performed what I now know was nothing
short of a miracle. After he straightened my legs, he would not hear of any
payment. Mainly, I suspect, he knew my parents were struggling at that time.
However, I was to be kept off my feet for the next eighteen months, which must
have been very difficult for my mother. But with the aid of my sister, who kept
me amused for some of the time by riding me around in a wheelbarrow but of
course we had the odd accident, she being only two years older than myself had
a job at times to keep me amused she would lose concentration and the end
result would be for me to be tipped out and hitting the floor with a bump and
then she had the task of picking me up which was no small job for one so small
she done her best I will be grateful to her forever.

# Chapter 2

Having explained how we became to take up residence at the cottage I will
endeavour to fill in a few more years, The next few years were quite
un-eventful and I suppose you might say we enjoyed an ideal way of life, my
mother ran her small tea room during the thirteen weeks which was the
recognised summer holiday period, with making beach trays and a few tables
under an awning outside to protect people from the sun, she also had three
tables inside in case the weather took a turn for the worst. She must have had
a hard time running the tea room and looking after four growing kids plus my
dad during those summer months, Artists used to come regular to draw and paint
the lighthouse with the sun setting behind in the west, [which was to become an
artists dream and drew them from far and wide,]

My sister and I have had our portraits painted and drawn many times over the
years, And have spent hours sitting for the artists, one of the most famous
ones was whistler - who was also commissioned to paint a portrait of Winston
Churchill, my sisters portrait was in black but mine was in pastel colours
which I still have and hope to be able to scan to enter in my book.

My father worked on the lighthouse 92 hours on 92 off and his time off was
spent setting fishing lines, also fishing for crab and lobster, conger and
anything that was edible or could exchange with the local farmers for farm
produce. We kids spent our time swimming fishing, and generally having a good
time but alas it was not to last, but came to an abrupt end in 1939.

However as we now know these idyllic days would have to be put on hold for a
further 5 years, I was in town [ST. HELIER] that is, with my mother walking
through the square, when I looked up I noticed white flags were being flown
from the town hall building. A man was stood on the town hall steps putting up
a bulletin declaring Jersey an open town, which of course was the beginning of
the Channel Islands about to be occupied by the German forces. The Germans
dropped a few small bombs around the harbour area before landing even ao an
open town had been declared, I can now only believe it was a show of strength.
Soon slowly and surely the Germans showed their might by landing their troops
by sea and air, black uniforms for the tank corps green for the ordinary
soldiers, a darker green for the marines then of course the usual blue for the
navy. The navy of course were the ones who took over the lighthouse and
installed themselves till war was over. My dad and the first lighthouse keeper
were suddenly whisked away by the Germans, They were then accused of signalling
to the enemy [British that is],and we were all amazed when they both came
walking down the hill unharmed, having been released without charge, I suspect
it was to put the wind up them, nobody explained or gave any reason for
releasing them, but we were all delighted they were unharmed. Things gradually
changed I used to sit on the steps just watching. And as a young boy of nine
and a half was both surprised and fascinated by this huge Zundapp motorcycle
and sidecar complete with mounted machine gun and three armed soldiers about to
take the lighthouse by storm, they continued to travel to and fro and then I
noticed another crew complete with cine camera which I now assume to be their
version of Pathe news taking a propaganda film capturing the lighthouse. I
assume this film was made for the benefit of the German cinema audiences to
give the impression that there were scores of motor cycle and sidecars taking
the lighthouse by storm. The German navy occupied the lighthouse and the area
where we lived become a military zone, they then duly barbed wired our area
off, fitted a rolling barbed wire gate at the top of the hill and marked the
roads with a red line indicating a military zone. That meant we had to have
identity cards complete with photos.
